<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<style type="text/css">
body{
	margin : 0;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	
	$(function(){
		setEl();
		
		<% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%>
		//focus TEXT 맞추기
		var chk_short = true;
	
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
	//시작 이벤트 집어넣기
	function ready(){
		/* (function poll(){ 
			$.ajax({
				url: "server"
				,success: function(data){
					//Update your dashboard gauge
			//		salesGauge.setValue(data.value);
					cnsole.log("==t==")
				}
				,dataType: "json"
				,complete: poll,
				timeout: 30000
			});
		})(); */

		
 		if(popId!=null){
			getWorkerList();
		}
		
	}

	// 실시간 데이터 받아오기 위해
	function poll(){
		setTimeout(function() {
			// 작업,장비 명단 가지고 오기
			getWorkerList();
		}, 10000);
	}
	//enter key 처리
	function enterEvt(event) {
		if(event.keyCode == 13){
			getTable()
		}
	}
	
	//사원 바코드를 입력해주세요
	function alarmMsg(){		
		clearTimeout(evtMsg)
		
		return evtMsg = setTimeout(function() {$("#aside").html("<span></span>")}, 10000)
//		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span>")}, 10000)
	}
	
	function getWorkerList(){
		var url = "${ctxPath}/pop/getWorkerList.do";
		
		var param = "popId=" + popId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			complete : poll,
			error : function(e){
				console.log("error")
			},
			timeout : 3000,
			success : function(data){
				var json = data.dataList;
				// 12개 이상으로 보여줄경우 화면에 다표시가 안되어
				// 자동으로 5초마다 바뀌게 하였음
				// 1번 테이블
				var table;
				// 2번 테이블
				var tableTwo;
				table = "<table id='workTable' style='width:100%; height:70%; text-align:center;'><tr><th></th><th>작업자</th><th>장비</th></tr>";
				tableTwo = "<table id='workTable' style='width:100%; height:70%; text-align:center;'><tr><th></th><th>작업자</th><th>장비</th></tr>";
				console.log(json.length)
				$(json).each(function(idx,data){
					if(idx<=12){
						table +="<tr><td width='5%;' style='background:" + data.color +";'> </td>"
						table +="<td width='30%;'>" + decode(data.nm) + "</td>"
						table +="<td width='65%;' style=''>" + decode(data.name) + "</td></tr>"
						//					}else if(idx==11){
//						table +="<tr><td></td><td> 왼쪾 </td> <td> 오른쪽 </td></tr>"
					}else{
						tableTwo +="<tr><td width='5%;' style='background:" + data.color +";'> </td>"
						tableTwo +="<td width='30%;'>" + decode(data.nm) + "</td>"
						tableTwo +="<td width='65%;' style=''>" + decode(data.name) + "</td></tr>"
					}
				})
				table +="</table>";
				tableTwo +="</table>";
				
				//javascript 테이블 그린 후 css 수정하기
				tableCss(table);
				
				//뒤에 페이지가 있을경우 5초후에 페이지 변경하기 위해
				if(json.length>13){
					setTimeout(function() {
						// 작업,장비 명단 가지고 오기
						tableCss(tableTwo);
					}, 5000);
				}
			}
		})
		
		
	}
	
	function tableCss(table){
		$("#workerList").empty();
		$("#workerList").append(table);

		//작업자 리스트 CSS
		$("#workerList").css({
			"top" : $("#noticeMsg").height() + $("#header").height(),
			"display" : "",
			"opacity" : 1,
			"width" : originWidth * 0.32,
			"height" : originHeight * 0.75,
			"transition" : "1s"
		})
		
		$("#workTable").css({
//		    "border-top": "1px solid #444444",
		    "border-collapse": "collapse",
		})
		
		$("#workTable tr").css({
			"height": $("#logo2").height() *  1.38,
			"font-size" : $("#logo2").height() *  0.45,
		});
		
		$("#workTable th").css({
		    "border-bottom": "1px solid #444444",
		    "padding": "10px",
		});
		
		$("#workTable td").css({
		    "border-bottom": "1px solid #444444",
//		    "padding": "10px",
			"margin" : 0,
		    "overflow":"hidden",
			"white-space" : "nowrap",
			"text-overflow": "ellipsis",
		})
	}
	//로그인하기 
	function getTable(){
		if($("#empCd").val()==0){
			return;
		}
		
		//popId 체크하기
		if(popId == null || popId == undefined){
			$("#aside").html("<span style='color:red;'>POP 장비 식별 불가능 입니다.</span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		$("#empCd").focus();
		$("#empCd").select();		
		empCd=$("#empCd").val();
	
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(empCd.substring(0,4)!="1111"){
			$("#aside").html("<span style='color:red;'>바코드를 다시 확인해 주세요</span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		empCd = empCd.substring(4,empCd.length);
		
		var url = "${ctxPath}/pop/popLoginChk.do";
		var param = "empCd=" + empCd ;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
/* 				alert("su")
				console.log(data); */
				

				var json = data.dataList;
				location.href="${ctxPath}/pop/popMainMenu.do?empCd=" + empCd
				
			},error : function(request,status,error){
				console.log(request.responseText);
				if(request.responseText=="no"){
					
					$("#aside").html("<span style='color:red;'>등록된 사원번호가 아닙니다.</span>")
					alarmMsg();
					
				}else{
	
					$("#aside").html("<span style='color:red;'>관리자에게 문의해주세요.</span>")
					alarmMsg();
				}
				$.hideLoading(); 

			}
		})
	}
	
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})
		
		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 $(".unos_input").css({
	        "background-color" : "rgba(0,0,0,0)",
	        "border" : "0",
	        "color" : "black",
	        "inline" : "0",
	        "outline" : "0",
	        "border-bottom" : getElSize(10) + "px solid lightgray",
	        "padding" : getElSize(15) + "px",
	        "font-size": $("#logo2").width() * 0.31 + "px",
	        "transition" : "1s"
        	
	    }).focus(()=>{
	        $(".unos_input").css({
	            "border-bottom" : getElSize(10) + "px solid red"
	        })
	    }).blur(()=>{
	        $(".unos_input").css({
	            "border-bottom" : getElSize(10) + "px solid lightgray"
	        })
	    })
	    
		$(".unos_input").css({
			"margin-left" : ((originWidth + originHeight * 0.5)/ 2) - ($(".unos_input").width() / 2),
			"margin-top" : (originHeight / 2) - ($(".unos_input").height() / 2) - $("#header").height(),
			"display" : "inline"
		}).focus()
		
		$("#aside").css({
//			"margin-left" : (originWidth / 2) - ($(".unos_input").width() / 2),
//			"margin-top" : (originHeight / 2) - ($(".unos_input").height() / 2) - $("#header").height(),
			"margin-left" : originHeight * 0.5,	
			"margin-top" : getElSize(50),
			"font-size" : $("#logo2").width() * 0.31 + "px",
			"text-align" : "center"
//			"display" : "inline"
		})
		
	}
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="https://www.digitaltwincloud.com/assets/imgs/default/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
		<div id="workerList" style="position: absolute; float: left; display: none; opacity: 0;">
<!-- 			<table> -->
<!-- 				<tr> -->
<!-- 					<td width=""> -->
<!-- 					</td> -->
<!-- 				</tr> -->
<!-- 			</table> -->
		</div>
		
        <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>        
	</div>
	

</body>
</html>