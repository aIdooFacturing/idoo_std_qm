BEGIN

 	DECLARE STD_HH_ST_N char(2);
	DECLARE STD_MM_ST_N char(2);
	
	DECLARE STD_HH_ST_D char(2);
	DECLARE STD_MM_ST_D char(2);
	
	DECLARE PARAM_DATE char(10);
	DECLARE PARAM_DATE_ADD1 char(10);

	DECLARE STD_DAY_ST_DT datetime;
	DECLARE STD_DAY_ED_DT datetime;

	DECLARE STD_NGT_ST_DT datetime;
	DECLARE STD_NGT_ED_DT datetime;
	
	DECLARE WORKIDX char(2);
	DECLARE COMDATE char(20); -- 날짜비교위해
--	SET PARAM_DATE = INPUT_DATE;

	
--	SET PARAM_DATE = CURDATE();
	IF ( time(now()) > "08:30:00" and time(now()) < "20:30:00" ) THEN		-- 주간 근무(오늘날짜)
		SET WORKIDX=2;
		SET PARAM_DATE = CURDATE();
	ELSEIF( time(now()) >= "00:00:00" and time(now()) < "08:30:00") THEN 	-- 야간 근무(현재날짜가 넘어가서 -1해줌)
		SET WORKIDX=1;
		SET PARAM_DATE = DATE_FORMAT(DATE_ADD( CURDATE(), INTERVAL -1 DAY),'%Y-%m-%d' );	-- 야간 근무(당일 야간날짜와 같아서 idx만 1)
	ELSE
		SET WORKIDX=1;
		SET PARAM_DATE = CURDATE();
	END IF;
	
	SET PARAM_DATE_ADD1 = DATE_FORMAT( DATE_ADD( PARAM_DATE, INTERVAL 1 DAY ),'%Y-%m-%d');
	
	
	SET STD_HH_ST_N = (select SUBSTRING_INDEX(start_time,'-',1) from tb_shop_setting where shop_id = 1 and start_time is not null);
	SET STD_MM_ST_N = (select SUBSTRING_INDEX(start_time,'-',-1) from tb_shop_setting where shop_id = 1 and start_time is not null);
	
	SET STD_HH_ST_D = (select SUBSTRING_INDEX(end_time,'-',1) from tb_shop_setting where shop_id = 1 and end_time is not null);
	SET STD_MM_ST_D = (select SUBSTRING_INDEX(end_time,'-',-1) from tb_shop_setting where shop_id = 1 and end_time is not null);
	
	SET STD_DAY_ST_DT = DATE_FORMAT(concat(PARAM_DATE,' ' ,STD_HH_ST_D,':',STD_MM_ST_D,'0:00'),'%Y-%m-%d %T');
	SET STD_DAY_ED_DT = DATE_FORMAT(concat(PARAM_DATE,' ' ,STD_HH_ST_N,':',STD_MM_ST_N,'0:00'),'%Y-%m-%d %T');
	
	SET STD_NGT_ST_DT = DATE_FORMAT(concat(PARAM_DATE,' ' ,STD_HH_ST_N,':',STD_MM_ST_N,'0:00'),'%Y-%m-%d %T');
	SET STD_NGT_ED_DT = DATE_FORMAT(concat(PARAM_DATE_ADD1 ,' ',STD_HH_ST_D,':',STD_MM_ST_D,'0:00'),'%Y-%m-%d %T');
	
--	set STD_HH_ST_N = 'no';

/* 	IF(ISNULL(STD_HH_ST_N))then
	 select 'why null?', STD_HH_ST_N;
	END IF; */
	

-- SELECT PARAM_DATE, PARAM_DATE_ADD1, STD_DAY_ST_DT, STD_DAY_ED_DT, STD_NGT_ST_DT, STD_NGT_ED_DT, STD_HH_ST_N,STD_MM_ST_N;


DROP TEMPORARY TABLE IF EXISTS TEMP_CNT_D;
CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_CNT_D AS (

    select
		TAS.dvc_id
		,max(part_count) as part
		,max(part_count) - if(STC.ttl_cnt=0,part_count,STC.ttl_cnt) as count
		,2 as idx
	from
		tb_adapter_status TAS
	inner join
		tb_std_ttl_cnt STC
	on
		TAS.dvc_id = STC.dvc_id
	WHERE
		STC.`std_ty`='d'
		and STC.date = PARAM_DATE
		and TAS.date in (PARAM_DATE) 
		and TAS.start_date_time > STD_DAY_ST_DT
		and TAS.start_date_time <= STD_DAY_ED_DT
	group by dvc_id
);

-- select * from TEMP_CNT_D;

DROP TEMPORARY TABLE IF EXISTS TEMP_CNT_N;
CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_CNT_N AS (
	
	select
		TAS.dvc_id
		,max(part_count) as part
		,max(part_count) - if(STC.ttl_cnt=0,part_count,STC.ttl_cnt) as count
		,1 as idx
	from
		tb_adapter_status TAS
	inner join
		tb_std_ttl_cnt STC
	on
		TAS.dvc_id = STC.dvc_id
	WHERE
		STC.`std_ty`='n'
		and STC.date = PARAM_DATE
		and TAS.date in (PARAM_DATE, PARAM_DATE_ADD1) 
		and TAS.start_date_time > STD_NGT_ST_DT
		and TAS.start_date_time <= STD_NGT_ED_DT
	group by dvc_id
);

-- 기존 계산 로직. (before ttl)
/* 
	SELECT
    			A.dvc_id
    			,IFNULL(B.COUNT,0) AS COUNT
    			,B.PART
    			,1 as idx
			FROM (
			        SELECT
						TD.dvc_id
					FROM
						tb_device TD
			       ) A			
			LEFT OUTER JOIN (
			       SELECT A.dvc_id , COUNT(A.dvc_id) as COUNT, MAX(PART) as PART
			       FROM (select
							TAS.dvc_id
							,TAS.MAIN_PRGM_NAME
						--	,TAS.MDL_M1
							,TAS.MAIN_PRGM_START_TIME
							,TAS.START_DATE_TIME
							,TAS.END_DATE_TIME
							,IFNULL(TAS.part_count,0) AS PART
						FROM
							tb_adapter_status TAS
						WHERE TAS.DATE = PARAM_DATE
							and TAS.END_DATE_TIME is not null
							and TIMESTAMPDIFF(second,STD_DT, TAS.END_DATE_TIME) < -43200
						group by TAS.dvc_id, TAS.main_prgm_start_time
					)A
				group by A.dvc_id
			) B
			ON A.dvc_id = B.dvc_id */
			


IF ((SELECT count(*) FROM tb_dvc_cnt_nd WHERE work_date = PARAM_DATE and work_idx = 1) > 0 ) THEN
		-- select "over 0", PARAM_DATE , std_dt;
		DELETE from tb_dvc_cnt_nd WHERE work_date =PARAM_DATE and work_idx = 1 ;
END IF;
--  select 'CLEAR', STD_DT, PARAM_DATE, DATE20, NOW_DATE, inputDate ;


INSERT INTO tb_dvc_cnt_nd
			 (dvc_id
			 ,work_date
			 ,tg_cyl
			 
			 ,part_cyl
			 ,cnt_cyl
			 ,work_idx
			 ,cpc
			 ,regdt
			 ) 
SELECT
    		TD.DVC_ID AS DVC_ID
    		,PARAM_DATE
    		,TG.tg_cyl
			,N.PART AS PART
			,N.COUNT * TG.cnt_per_cyl AS COUNT
			,N.idx
			,TG.cnt_per_cyl
    		,now()
    	FROM
    		tb_device TD
		inner join
    		TEMP_CNT_N N
    	ON
    		N.dvc_id = TD.dvc_id
		left join
			(select dvc_id, tg_run_time, tg_cyl, cnt_per_cyl from tb_dvc_tg_nd where work_idx = 1 and tg_date=(select max(tg_date) from tb_dvc_tg_nd)) TG
		ON
			TG.dvc_id = TD.dvc_id;

IF ((SELECT count(*) FROM tb_dvc_cnt_nd WHERE work_date = PARAM_DATE and work_idx = 2) > 0 ) THEN
		-- select "over 0", PARAM_DATE , std_dt;
		DELETE from tb_dvc_cnt_nd WHERE work_date =PARAM_DATE and work_idx = 2 ;
END IF;


INSERT INTO tb_dvc_cnt_nd
			 (dvc_id
			 ,work_date
			 ,tg_cyl
			 ,part_cyl
			 ,cnt_cyl
			 ,work_idx
			 ,cpc
			 ,regdt
			 )
SELECT
    		TD.DVC_ID AS DVC_ID
    		,PARAM_DATE
    		,TG.tg_cyl
			,N.PART AS PART
			,N.COUNT * TG.cnt_per_cyl AS COUNT
			,N.idx
			,TG.cnt_per_cyl
    		,now()
    	FROM
    		tb_device TD
		inner join
    		TEMP_CNT_D N
    	ON
    		N.dvc_id = TD.dvc_id
		left join
			(select dvc_id, tg_run_time, tg_cyl, cnt_per_cyl from tb_dvc_tg_nd where work_idx = 2 and tg_date=(select max(tg_date) from tb_dvc_tg_nd)) TG
		ON
			TG.dvc_id = TD.dvc_id;

-- select * , PARAM_DATE, STD_DT,DATE20 from tb_dvc_cnt_nd WHERE work_date = PARAM_DATE order by work_idx desc;



-- 스타트타임이 같은경우 startCnt 넣기
IF((select count(*) from tb_dvc_tg_nd_add tgadd where tgadd.tg_date=PARAM_DATE and start_work_time=substr(CURTIME(),1,5))>=1) THEN
	UPDATE
		tb_dvc_tg_nd_add tgadd
	SET
		startCnt=(select cnt_cyl from tb_dvc_cnt_nd where work_date=PARAM_DATE and dvc_id=tgadd.dvc_id),
		endCnt=(select cnt_cyl from tb_dvc_cnt_nd where work_date=PARAM_DATE and dvc_id=tgadd.dvc_id)
	WHERE
		tgadd.tg_date=PARAM_DATE
	AND
		start_work_time=substr(CURTIME(),1,5);

	UPDATE
		tb_dvc_tg_nd tgadd
	SET
		startCnt=(select cnt_cyl from tb_dvc_cnt_nd where work_date=PARAM_DATE and dvc_id=tgadd.dvc_id)
	WHERE
		tgadd.tg_date=PARAM_DATE
	AND
		start_work_time=substr(CURTIME(),1,5);	
END IF;

-- 스타트타임~앤드타임 사이인지 구하기위해 날짜를 추가
IF(WORKIDX=2) THEN
	SET COMDATE=PARAM_DATE;
ELSEIF(WORKIDX=1) THEN
	SET COMDATE= DATE_FORMAT(DATE_ADD( PARAM_DATE, INTERVAL 1 DAY),'%Y-%m-%d' );
END IF;	

	
-- 추가된 작업자 수량 변경
UPDATE
	tb_dvc_tg_nd_add tgadd
SET
	endCnt=(select cnt_cyl from tb_dvc_cnt_nd where work_date=PARAM_DATE and dvc_id=tgadd.dvc_id and work_idx=WORKIDX)
	,input_date_time=now()
WHERE
	tgadd.tg_date=PARAM_DATE
AND
	substr(now(),1,16)>(concat((if(tgadd.start_work_time>"08:30" and tgadd.start_work_time<="24:00",PARAM_DATE,COMDATE))
	," ",tgadd.start_work_time))
AND
	substr(now(),1,16)<=(concat((if(tgadd.end_work_time>"08:30" and tgadd.end_work_time<="24:00",PARAM_DATE,COMDATE))
	," ",tgadd.end_work_time));

-- 기존 작업자 수량 변경
UPDATE
	tb_dvc_tg_nd tgadd
SET
	endCnt=(select cnt_cyl from tb_dvc_cnt_nd where work_date=PARAM_DATE and dvc_id=tgadd.dvc_id and work_idx=WORKIDX)
	,input_date_time=now()
WHERE
	tgadd.tg_date=PARAM_DATE
AND
	substr(now(),1,16)>(concat((if(tgadd.start_work_time>"08:30" and tgadd.start_work_time<="24:00",PARAM_DATE,COMDATE))
	," ",tgadd.start_work_time))
AND
	substr(now(),1,16)<=(concat((if(tgadd.end_work_time>"08:30" and tgadd.end_work_time<="24:00",PARAM_DATE,COMDATE))
	," ",tgadd.end_work_time));
	
	
	
-- tb_worker_cnt_nd 테이블에 작업자별 수량 저장
	-- 처음에 주,야 작업자 모두 집어넣기 오늘날짜의 작업자가 0일때
	IF ((select count(*) from tb_worker_cnt_nd where tg_date=PARAM_DATE)=0) THEN
		INSERT INTO tb_worker_cnt_nd
			 (emp_cd
			 ,tg_date
			 ,work_idx
			 ,tg_cyl
			 ,tg_run_time
			 ,cnt
			 ,regdt
			 )
		SELECT 
			emp_cd,tg_date,work_idx,sum(tg_cyl) as tg_cyl,tg_run_time,sum(cnt) as cnt,now()
		FROM (
			SELECT 
				emp_cd
				,tg_date
				,work_idx
				,tg_cyl
				,tg_run_time
				,ifnull(endCnt,0)-ifnull(startCnt,0) as cnt
			
			FROM
				tb_dvc_tg_nd_add tgadd
			WHERE
				tgadd.tg_date=PARAM_DATE
			
			UNION ALL
			
			SELECT
				emp_cd
				,tg_date
				,work_idx
				,tg_cyl
				,tg_run_time
				,ifnull(endCnt,0)-ifnull(startCnt,0) as cnt
			FROM
				tb_dvc_tg_nd tgnd
			WHERE
				tgnd.tg_date=PARAM_DATE
			) A 
			group by emp_cd,work_idx;
	END IF;
	
	
	-- 현재 주,야 확인하여 맞는 시간의 데이터 삭제후 재 등록 (수량 업데이트)
	IF (WORKIDX=2) THEN
		delete from tb_worker_cnt_nd where tg_date=PARAM_DATE and work_idx=2;	
	ELSEIF (WORKIDX=1) THEN
		delete from tb_worker_cnt_nd where tg_date=PARAM_DATE and work_idx=1; 
	END IF;
	
	INSERT INTO tb_worker_cnt_nd
		 (emp_cd
		 ,tg_date
		 ,work_idx
		 ,tg_cyl
		 ,tg_run_time
		 ,cnt
		 ,regdt
		 ,countDvc
		 )
	SELECT 
		emp_cd,tg_date,work_idx,sum(tg_cyl) as tg_cyl,tg_run_time,sum(cnt) as cnt,now(),count(*) as countDvc
	FROM (
		SELECT 
			dvc_id
			,emp_cd
			,tg_date
			,work_idx
			,tg_cyl
			,tg_run_time
			,ifnull(endCnt,0)-ifnull(startCnt,0) as cnt
		
		FROM
			tb_dvc_tg_nd_add tgadd
		WHERE
			tgadd.tg_date=PARAM_DATE
		
		UNION ALL
		
		SELECT
			dvc_id
			,emp_cd
			,tg_date
			,work_idx
			,tg_cyl
			,tg_run_time
			,ifnull(endCnt,0)-ifnull(startCnt,0) as cnt
		FROM
			tb_dvc_tg_nd tgnd
		WHERE
			tgnd.tg_date=PARAM_DATE
		) A 
		WHERE
			work_idx=WORKIDX
		AND
			dvc_id IN (
					SELECT dvc_id FROM tb_dvc_cnt_nd nd WHERE nd.work_date=PARAM_DATE
					)
		group by emp_cd,work_idx;
		
		
	-- 새로만들어보자	
	INSERT INTO
	tb_dvc_cnt_change
	(
		dvcId
		,start_count
		,part_count
		,date
		,regdate
	)
	SELECT
		dvc_id as dvcId
		,part_count as start_count
		,part_count as part_count
		,max(end_date_time) as date
		,now() as regdate
	FROM
		tb_adapter_status
	WHERE
		part_count is not null
	AND
		end_date_time is not null
	AND
		end_date_time > date_add(now(), interval -1 hour)
	GROUP BY
		dvc_id,part_count
	ON DUPLICATE KEY UPDATE
		regdate=now();
		
	insert into tb_worker_cnt_nd_copy(emp_cd,nm) values (9999,WORKIDX);
		
	IF (WORKIDX=2) THEN
		delete from tb_worker_cnt_nd_copy where tg_date=PARAM_DATE and work_idx=2;	
	ELSEIF (WORKIDX=1) THEN
		delete from tb_worker_cnt_nd_copy where tg_date=PARAM_DATE and work_idx=1; 
	END IF;
	
	DROP TEMPORARY TABLE IF EXISTS tgnd_table;
	CREATE TEMPORARY TABLE IF NOT EXISTS tgnd_table AS (
		select * from (select * from tb_dvc_tg_nd where tg_date=PARAM_DATE
		UNION
		select * from tb_dvc_tg_nd_add where tg_date=PARAM_DATE) A
	);
	
	
	-- 새로 만들어보자 copy에 추가하기
	insert into tb_worker_cnt_nd_copy(emp_cd,nm) values (9999,WORKIDX);
	
	insert into tb_worker_cnt_nd_copy(emp_cd,nm) values (9999,13232);
	
	INSERT INTO tb_worker_cnt_nd_copy
	 (emp_cd
	 ,tg_date
	 ,work_idx
	 ,tg_cyl
	 ,tg_run_time
	 ,regdt
	 ,countDvc		
	 ,cnt
	 )
	SELECT 
		emp_cd
		 ,tg_date
		 ,work_idx
		 ,tg_cyl
		 ,tg_run_time
		 ,regdt
		 ,countDvc
		 ,cnt
	FROM(
		SELECT
			emp.EMP_CD as emp_cd
			,tgnd.tg_date as tg_date
			,tgnd.work_idx as work_idx
			,sum(tgnd.tg_cyl) as tg_cyl
			,tgnd.tg_run_time as tg_run_time
			,now() as regdt
			,count(*) as countDvc
			,emp.NM as nm
			,ifnull(sum( ( (SELECT max(part_count) FROM tb_dvc_cnt_change 
				WHERE date > IF(
					tgnd.work_idx=1 && tgnd.start_work_time < "20:30" 
					,concat(date_format(date_add(tgnd.tg_date, interval 1 day),"%Y-%m-%d")," ",tgnd.start_work_time)
					,concat(tgnd.tg_date," ",tgnd.start_work_time)
					)
				AND date < IF(
					tgnd.work_idx=1 && tgnd.end_work_time < "20:30" 
					,concat(date_format(date_add(tgnd.tg_date, interval 1 day),"%Y-%m-%d")," ",tgnd.end_work_time)
					,concat(tgnd.tg_date," ",tgnd.end_work_time)
					)
				AND	dvc_id=dvcId
			) - (SELECT min(part_count) FROM tb_dvc_cnt_change 
				WHERE date > IF(
					tgnd.work_idx=1 && tgnd.start_work_time < "20:30" 
					,concat(date_format(date_add(tgnd.tg_date, interval 1 day),"%Y-%m-%d")," ",tgnd.start_work_time)
					,concat(tgnd.tg_date," ",tgnd.start_work_time)
					)
				AND date < IF(
					tgnd.work_idx=1 && tgnd.end_work_time < "20:30" 
					,concat(date_format(date_add(tgnd.tg_date, interval 1 day),"%Y-%m-%d")," ",tgnd.end_work_time)
					,concat(tgnd.tg_date," ",tgnd.end_work_time)
					)
				AND	dvc_id=dvcId
			) )*cnt_per_cyl),0) as cnt
			
		FROM
			tgnd_table tgnd
		LEFT JOIN
			EMP_MST emp
		ON
			emp.EMP_CD=tgnd.emp_cd
		WHERE
			tgnd.dvc_id<="104"
		AND
			work_idx=WORKIDX
		GROUP BY
			emp.EMP_CD,tgnd.work_idx
		HAVING
			(cnt is not null
			OR
			nm is not null
			)
	) A;
END