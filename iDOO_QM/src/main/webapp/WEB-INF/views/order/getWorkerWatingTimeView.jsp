<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#print_table table{
	table-layout: fixed;
}
#datePicker{
    background : rgb(121, 218, 76);
    border-radius: 5px;
    font-weight: bolder;
}
</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var now = moment().subtract(1,'days').format("YYYY-MM-DD")
		$("#sDate").val(now);
		$("#eDate").val(now);
		getWorkerWatingTime();
	};
	
	var handle = 0;
	
	$(function(){
		createNav("kpi_nav", 4);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("input").css({
			"font-size" : getElSize(50),
			"height" : getElSize(100),
			"margin" : "0px",
		    "margin-right" : "0px",
		    "margin-left" : "0px",
		    "width" : getElSize(700)
		})
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(250),
			"font-size" : getElSize(50)
		});
		
		$("#content_table td").css({
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#tableDiv").css({
			"width" : $(".menu_right").width(),
			"margin-top" : getElSize(10),
			"position" : "absolute"	,
			"background-color" : "black"
		});
		
		$("#sDate, #eDate").css({
			"width" : getElSize(300)
		})
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
</script>

<script>

var workerList=[];

function getWorkerWatingTime(){
	$.showLoading()
	
	$("#print_table").empty();
		
	var table ="<table border='1' class='top_table' style='table-layout: fixed;' width='95%' align='center'><tr>"+
							"<th width='60%' rowspan='3' colspan='2'>비가동 시간통계 <br>"+ $("#sDate").val() +' ~ '+$("#eDate").val() +
							"</th><th style='background : lightgray' colspan='3'>결제</th></tr>" +
							"<tr style='background : lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
							"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
							"</table>"+
							"<table class='p_table' border='1' width='95%' align='center'>"+
							"<tr style='background : lightgray'><th>장비</th> <th>비가동시작시간</th> <th>비가동종료시간</th> <th>비가동 시간</th></tr>";
	
	var url = "${ctxPath}/chart/getWorkerWatingTime.do";
	var param = "sDate="+$("#sDate").val()+"&eDate="+$("#eDate").val();
	var workerArray=[];
	$.ajax({
		url : url,
		dataType : "json",
		data : param,
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			$(json).each(function(idx, data){
				
				var object = {
						"name" : decode(data.name),
						"oprNo" : decode(data.oprNo),
						"dvcId" : data.dvcId,
						"sDate" : data.sDate,
						"eDate" : data.eDate,
						"seq" : data.seq,
						"workIdx" : data.workIdx,
						"chartStatus" : data.chartStatus,
						"time" : data.waitTime,
						"checkNum" : data.checkNum,
						"totalWaitTime" : data.totalWaitTime,
						"empCd" : data.empCd
				} 
				
				workerArray.push(object)
			});
			var waitList = [];
			if(workerArray[0].checkNum==1){
				waitList=workerArray;	
			}else{
				
				
				var launchStart = moment($("#sDate").val(), "YYYY-MM-DD HH:mm:ss")
				launchStart.add(12,"hours").add(30,"m")
				var launchEnd = moment($("#sDate").val(), "YYYY-MM-DD HH:mm:ss")
				launchEnd.add(13,"hours").add(30, "m")
				var temp=[];
				var temp2="";
				
				for(var i=1; i<workerArray.length;i++){
					var workersDate = moment(workerArray[i].sDate)
					if(temp2==""){
						temp2=workerArray[i]
					}else{
						if(temp2.dvcId==workerArray[i].dvcId && 
								temp2.chartStatus == workerArray[i].chartStatus && (moment(workerArray[i].eDate).diff(temp2.eDate)/1000/60)==2){
							temp2.eDate=workerArray[i].eDate
						}else{
							temp2.chartStatus=workerArray[i-1].chartStatus
							temp.push(temp2);
							temp2="";
						}
					}
				}
				for(var i=0; i<temp.length; i++){
					var sDate = new Date(temp[i].sDate);
					var eDate = new Date(temp[i].eDate);
					if(eDate - sDate > 300000){
						temp[i].time = (eDate-sDate)/1000/60
						if(temp[i].empCd==null){
							temp[i].empCd=0;
						}
						waitList.push(temp[i])
					}
				}
				var obj = new Object();
				obj.val = waitList;
	      		
				var url = "${ctxPath}/chart/insertWaitTime.do";
				var param = "val="+JSON.stringify(obj) +"&date="+$("#sDate").val();
				$.ajax({
					url : url,
					dataType : "json",
					data : param,
					type : "post",
					success : function(data){
					}
				})
			}
			
			waitList.sort(function (a, b) {
				if(a.name < b.name) return -1
				else return 1
			});
			console.log(waitList)
			var length = waitList.length;
			var NoWorker=[]
			
			var NewwaitList=[]
			for(var i=0; i<length; i++){
				if(waitList[i].name=="작업자 미배치"){
					console.log(waitList[i].name)
					NoWorker.push(waitList[i])
				}else{
					NewwaitList.push(waitList[i])
				}
			}
			
			console.log(NewwaitList)
			console.log(NoWorker)
			
			var NoWorkLength=NoWorker.length
			for(var i=0;i<NoWorkLength;i++){
				NewwaitList.push(NoWorker[i])
			}
			
			console.log(NewwaitList)
			
			length = waitList.length;
			var tempList=null;
			var totalWaitTime=0;
			
			for(var i=0;i<length;i++){
				if(tempList == null){
					tempList = NewwaitList[i];
					totalWaitTime +=NewwaitList[i].time 
				}else if(i==length-1){
					totalWaitTime +=NewwaitList[i].time 
					for(var j=0;j<length;j++){
						if(NewwaitList[j].name==tempList.name){
							NewwaitList[j].totalWaitTime=totalWaitTime	
						}
					}
				}else{
					if(tempList.name==NewwaitList[i].name){
						totalWaitTime+=NewwaitList[i].time
					}else{
						for(var j=0;j<length;j++){
							if(NewwaitList[j].name==tempList.name){
								NewwaitList[j].totalWaitTime=totalWaitTime	
							}
						}
						tempList=NewwaitList[i];
						totalWaitTime=NewwaitList[i].time;
						NewwaitList[i].totalWaitTime = totalWaitTime;
					}
				}
			}
			
			var chk=1;
			
			//프린트 그리는 총 테이블 갯수 구하기 chk
			$(NewwaitList).each(function(idx,data){
				chk++;
				if(idx!=0){
					if(NewwaitList[idx].name!=NewwaitList[idx-1].name){
						chk++;
					}
				}
			});
			//테이블 몇번째 열 까지 뽑을지 (A4사이즈 맞추기 위해서)
			var divideNum=40;		
			var page=1;		//page size표시
			var totalpage=Math.ceil(chk/divideNum);		//총 페이지수
			var index=0;	//행 갯수 세기
			var countDvc=[]; //장비 갯수 세기
			$(NewwaitList).each(function(idx,data){
				
					//한개의 tr
					if(idx==0){
						//작업자별 장비 갯수 구하기
						for(i=0;i<NewwaitList.length;i++){
							if(data.name==NewwaitList[i].name){
								if(countDvc.indexOf(NewwaitList[i].dvcId)==-1){
									countDvc.push(NewwaitList[i].dvcId)
								}
							}
						}
						index++;
						table +="<tr style='background-color:lightgray' class='totalwork'><td> 작업자 : "+ data.name +"</td><td colspan='3'> 총 장비 : "+ countDvc.length +" 대 <font style='float:right'>총 비가동 시간 : " + data.totalWaitTime +" 분</font></td>";
					}else{
						index++;
						if(NewwaitList[idx].name!=NewwaitList[idx-1].name){
							countDvc=[]
							for(i=0;i<NewwaitList.length;i++){
								if(data.name==NewwaitList[i].name){
									if(countDvc.indexOf(NewwaitList[i].dvcId)==-1){
										countDvc.push(NewwaitList[i].dvcId)
									}
								}
							}
							
							
							index++;
							table +="<tr style='background-color:lightgray' class='totalwork'><td> 작업자 : "+ data.name +"</td><td colspan='3'> 총 장비 : " + countDvc.length + " 대 <font style='float:right'>총 비가동 시간 : " + data.totalWaitTime +" 분</font></td>";
						}else{
							
						}
					}
					
					//tr
					table +="<tr><td>" + data.oprNo + "</td><td>" + data.sDate + "</td><td>" + data.eDate + "</td><td><center>" + data.time + "분</center></td></tr>" ;
					
					//a4 size page 넘기기
					if(idx!=0 && index>divideNum ){	
						page++;
						index=1;
						table +="</table>" +
								"<div style='page-break-before:always'></div>";
								
						table +="<br><br><br><br> <table class='p_table' border='5' width='95%' align='center'><tr><th colspan='3' rowspan='2'>생산완료입력 조회"+
								"<br>"+ $("#sDate").val() +' ~ '+$("#eDate").val() +
								"</th>	<th colspan='2' style='background : lightgray'>페이지</th></tr>" +
								"<tr class='sign'><th colspan='2'>" + page +" / " + totalpage +"</th></tr>" +
								"<tr style='background : lightgray'><th>장비</th> <th>비가동시작시간</th> <th>비가동종료시간</th> <th>비가동 시간</th></tr>";
					}
			})
			
			table +="</table>"
			$("#print_table").append(table);
			
			$(".sign").css({
				"height" : "50px"
			})
			
			$(".top_table").css({
				"border" : "1px ridge black",
				"font-size" : "10px"
			})
			
			$(".sign_title").css({
				"height" : "30px"
			})
			
			$(".p_table tr th").css("font-size","10px")
			$(".p_table tr td").css("font-size","10px")
			
			var dataSource = new kendo.data.DataSource({
	                data: waitList,
	                group: [{ field: "name", title:"${worker}" , dir: "desc",  aggregates:[ {field: "time", aggregate:"sum"} ]},{ field: "oprNo", title:"${machine_Name}",  dir: "asc" , aggregates:[ {field: "time", aggregate:"sum"} ]}],
	        	});
			dataSource.fetch().then(function(){
				  var data = dataSource.data();
			});
			grid.setDataSource(dataSource);
			
			
			$.hideLoading()
		}
	});
}






$(function(){
	
	$("#tableDiv").kendoGrid({
		  
		selectable: "row",
	    height:  getElSize(1650),
	    groupable: false,
	    sortable: true,
	    editable: false,
	    scrollable: true,
	    filterable: {
	        messages: {
	          search: "${search_Placeholder}"
	        }
	      },
	    change: function() {
	  		
	  	},
	    columns: [
	    			{
	        			field: "name",
	        			title: "${worker}",
	        			template : "#= name #",
	        			attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(40)
            			},
            			filterable: {
            		          multi: true,
            		          search: true
            		        }
	    			},
	    			{
	        			field: "oprNo",
	    				title: "${device}",
	    				template : "#= oprNo #",
	    				attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(40)
            			},
            			filterable: {
          		          multi: true,
          		          search: true
          		        }
	    			},
	    			{
	        			field: "chartStatus",
	    				title: "${chartStatus}",
	    				template : "#= chartStatus #",
	    				attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(40)
            			},
            			filterable: {
          		          multi: true,
          		          search: true
          		        }
	    			},
	    			{
	        			field: "sDate",
	        			title: "${noon_Operationtimestart}",
	        			template : "#= sDate #",
	        			attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(40)
            			},filterable: false
	    			},
	    			{
	        			field: "eDate",
	        			title: "${noon_Operationtimeend}",
	        			template: "#= eDate #",
	        			attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(40)
            			},filterable: false
	    			},
	    			{
	        			field: "time",
	        			title: "${noon_Operation_time}",
	        			attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(40)
            			},
	        			groupFooterTemplate: " ${total}: #= sum # ${minute}",filterable: false
	    			}
	    		]
		});
	grid = $("#tableDiv").data("kendoGrid");
})

var printChkWin=false;
function pop_print(){
	printChkWin=true;
	win = window.open("","","height=" + screen.height + ",width=" + screen.width + "fullscreen=yes");
       self.focus();
       win.document.open();
       win.document.write('<'+'html'+'><'+'head'+'><'+'/'+'head'+'><'+'body'+'>');
       win.document.write(document.getElementById('print_table').innerHTML);
       win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
       win.document.close();
       
       if(printChkWin==true){
			$(window).click(function() {
				//Hide the menus if visible
				alert('프린터 창이 열려 있습니다')
			});
		}

       win.print();
       
       printChkWin=false;
	   $(window).off("click");
       
       win.close();
       
}
	
	
$(function(){
	$("input#sDate").datepicker({
		"minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
		onSelect:function(){
		}
	})
	$("input#eDate").datepicker({
		"minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
		onSelect:function(){
		}
	})
	$(".day").css({
		"background" : "green",
		"color" : "white"
	})
})

var startDate,
endDate,
selectCurrentWeek = function () {
    window.setTimeout(function () {
        $('#sDate,#eDate').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
    }, 1);
};

function daily(){
	
	$( "input#sDate" ).datepicker( "destroy" );
	$( "input#eDate" ).datepicker( "destroy" );
	
	$('.ui-weekpicker').on('mousemove', 'tr', function () {
       $(this).find('td a').removeClass('ui-state-hover');
   });
	
	$("input#sDate").datepicker({
	    "minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
		onSelect:function(){
		}
	})
	$("input#eDate").datepicker({
	    "minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
		onSelect:function(){
		}
	}) 
	
	$("#dailyRange").css({
		"display" : "inline"
	})
	$("#weeklyRange").css({
		"display" : "none"
	})
	$("#monthlyRange").css({
		"display" : "none"
	})
	
	$(".day").css({
		"background" : "green",
		"color" : "white"
	})
	$(".week").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	}) 
	$(".month").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$("#actionBtn").css({
			"display":"inline"
	})
}

function weekly(){
	
	$( "input#sDate" ).datepicker( "destroy" );
	$( "input#eDate" ).datepicker( "destroy" );
	
	$('input#sDate').datepicker({
	 "minDate": new Date(2017, 12-1, 1),
        "maxDate": 0,
       "showOtherMonths": false,
       "selectOtherMonths": false,
       "onSelect": function (dateText, inst) {
           var date = $(this).datepicker('getDate'),
               dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
           startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
           endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
           $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
           $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
           selectCurrentWeek();
       },
       "beforeShow": function () {
           selectCurrentWeek();
       },
       "beforeShowDay": function (date) {
           var cssClass = '';
           if (date >= startDate && date <= endDate) {
               cssClass = 'ui-datepicker-current-day';
           }
           return [true, cssClass];
       },
       "onChangeMonthYear": function (year, month, inst) {
           selectCurrentWeek();
       }
   }).datepicker('widget').addClass('ui-weekpicker');
   
   $('input#eDate').datepicker({
	   "minDate": new Date(2017, 12-1, 1),
       "maxDate": 0,
       "showOtherMonths": false,
       "selectOtherMonths": false,
       "onSelect": function (dateText, inst) {
           var date = $(this).datepicker('getDate'),
               dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
           startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
           endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
           $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
           $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
           selectCurrentWeek();
       },
       "beforeShow": function () {
           selectCurrentWeek();
       },
       "beforeShowDay": function (date) {
           var cssClass = '';
           if (date >= startDate && date <= endDate) {
               cssClass = 'ui-datepicker-current-day';
           }
           return [true, cssClass];
       },
       "onChangeMonthYear": function (year, month, inst) {
           selectCurrentWeek();
       }
   }).datepicker('widget').addClass('ui-weekpicker');
   
   
   $('.ui-weekpicker').on('mousemove', 'tr', function () {
       $(this).find('td a').addClass('ui-state-hover');
   });
   $('.ui-weekpicker').on('mouseleave', 'tr', function () {
   	$(this).find('td a').removeClass('ui-state-hover');
   });
	
	$("#dailyRange").css({
		"display" : "inline"
	})
	$("#monthlyRange").css({
		"display" : "none"
	})
	$(".day").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$(".week").css({
		"background" : "green",
		"color" : "white"
	})
	$(".month").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$("#actionBtn").css({
		"display":"inline"
	})
}

function monthly(){
	$( "input#sDate" ).datepicker( "destroy" );
	$( "input#eDate" ).datepicker( "destroy" );
	
	$('input#sDate').datepicker( {
		 "minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
       changeMonth: true,
       changeYear: true,
       showButtonPanel: true,
       dateFormat: 'yy mm',
       onClose: function(dateText, inst) { 
           var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
           if(month<10){
           	month = '0'+month.toString()
           }
           
           $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
           $('input#eDate').val(year+'-'+month.toString()+'-'+31);
       }
   });
	
	$('input#eDate').datepicker( {
		 "minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
       changeMonth: true,
       changeYear: true,
       showButtonPanel: true,
       dateFormat: 'yy mm',
       onClose: function(dateText, inst) { 
           var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
           var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
           if(month<10){
           	month = '0'+month.toString()
           }
           
           $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
           $('input#eDate').val(year+'-'+month.toString()+'-'+31);
       }
   });
	
	$("#dailyRange").css({
		"display" : "inline"
	})
	
	$(".day").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$(".week").css({
		"background" : "rgb(121, 218, 76)",
		"color" : "black"
	})
	$(".month").css({
		"background" : "green",
		"color" : "white"
	})
	$("#actionBtn").css({
		"display":"inline"
	})
}

</script>

</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/kpi_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td>
								<div class="rangeSelector" style="display: -webkit-inline-box;">
									<button id="datePicker" class="day" onclick="daily()"><spring:message code="By_date"></spring:message></button>					
									<button id="datePicker" class="week" onclick="weekly()"><spring:message code="Weekly"></spring:message></button>					
									<button id="datePicker" class="month" onclick="monthly()"><spring:message code="monthly"></spring:message></button>
								</div> 
								<div id="dailyRange" style="display: inline">
									<spring:message code="date_"></spring:message>: <input type="text" id="sDate" readonly > ~ <input type="text" id="eDate" readonly>
								</div>
								<div id="actionBtn" style="display: inline; float: right; margin:auto;">
									<button id="search" onclick="getWorkerWatingTime()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
									<button id="print" onclick="pop_print()"><i class="fa fa-print" aria-hidden="true"></i> <spring:message code="print"></spring:message></button>
								</div>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="tableDiv"></div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	 <div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	