<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("maintenance", "NonOperation")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => getTable()
		
		//getMene();
		getMenu();

		setEl();
	})
	
	//비가동 사유 리스트 담기 위한 변수
	var nonOpTy = [];

	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
				var diff = new Date($("#eDate").val()).getTime()-new Date(e).getTime();
				diff = diff/1000/60/60/24;
				console.log(e)
				if(diff>186){
					alert("6개월 이상 조회하실수 없습니다._s")
// 					$("#sDate").val(moment($("#eDate").val()).subtract('days',90).format("YYYY-MM-DD"))
					$("#sDate").val(moment($("#eDate").val()).subtract('months',6).format("YYYY-MM-DD"))
					getTable();
				}else{
			    	//e == 날짜 
			    	getTable();
			    	$("#sDate").val(e);
				}
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	var diff = new Date(e).getTime()-new Date($("#sDate").val()).getTime();
				diff = diff/1000/60/60/24;
				
				if(diff>186){
					alert("6개월 이상 조회하실수 없습니다._d")
// 					$("#eDate").val(moment($("#sDate").val()).add('days',90).format("YYYY-MM-DD"))
					$("#eDate").val(moment($("#sDate").val()).add('months',6).format("YYYY-MM-DD"))
					getTable();
				}else{
			    	//e == 날짜 
			    	getTable();
			    	$("#eDate").val(e);
				}
				
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
		
		$("#worker").css({
			"width" : getElSize(270)
		})
		
		$("#historyPage").css({
			"background" : "green"
			,"padding-top" : getElSize(40)
			,"padding-bottom" : getElSize(40)
			,"padding-left" : getElSize(75)
			,"padding-right" : getElSize(75)
			,"border-radius" : getElSize(15)
			,"cursor" : "pointer"
		})
	}
	
	function getMenu(){
		nonOpTyDataSource = new kendo.data.DataSource({});
		
		//이유 선택박스 가지고오기
		var url = ctxPath + "/chart/getNonOpTy.do"
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var options = ""
				$(json).each(function(idx, data){
					
					var obj = {
							"nonOpTy" : data.id,
							"nonOpName" : decode(data.nonOpTy)
					}
					
					nonOpTyDataSource.add(obj)
					nonOpTy.push(obj)
				});
				
				getGrid();
			}	
		})
	}
	
	//비가동 전체 선택기능
	function checkOpAll(e){
		console.log($("#checkallOp").is(":checked"))
		if($("#checkallOp").is(":checked")){
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		kendotableOp.dataSource.fetch();
	}
	
	//체크박스 tbody 선택시 (비가동)
	function checkRowOp(e){
		console.log("event click")
		var gridList=kendotableOp.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		
		console.log(dataItem)
		
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = gridOp.dataItem(row)
		 console.log(initData)
		 
		 if(initData.checkSelect){
			 initData.checkSelect = false;
		 }else{
			 initData.checkSelect = true;
		 }
		 kendotableOp.dataSource.fetch();
	}
	
// 	var collapseAllGroups = function (grid) {
// 	    grid.table.find(".k-grouping-row").each(function () {
// 	        grid.collapseGroup(this);
// 	    });
// 	}
	
	function getGrid(){
		
		kendotableOp = $("#grid").kendoGrid({
			editable : true
			,height : getElSize(1650)
			,dataBound : function(e) {
				gridOp = this;
				gridCss();
				
// 				collapseAllGroups(this);
				
			}
			,toolbar: [{
				name:"excel"
			}]
			,excel:{
	            fileName:"비가동_" + moment().format("YYYY-MM-DD") + ".xlsx"
	        }
			,columns : [/* {
				field:"checkerOp",
				title:"<input type='checkbox' id='checkallOp' onclick='checkOpAll(this)'>"
				,width:originWidth * 0.055,
				template: "<input type='checkbox' onclick='checkRowOp(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" 
			}, */{
				title : "장비"
// 				,groupHeaderTemplate : "장비 : #=value# #=aggregates.time.sum#   #=aggregates.time.max#  #=aggregates.time.min#  #=aggregates.time.count#"
				,groupHeaderTemplate : "<div class='headerDiv'>장비 : #=value#</div> <div style='color:red;' class='headerDiv'>총 비가동 : #=aggregates.time.sum# (분)</div>"
				,field : "name"
			},{
				title : "비가동 유형"
				,field : "chartStatus"
			},{
				title : "시작"
				,field : "sDate"
			},{
				title : "종료"
				,field : "eDate"
			},{
				title : "비가동<br>시간(분)"
				,field : "time"
			},{
    			field: "reason",
				title: "비가동사유",
// 				groupHeaderTemplate : "<div class='headerDiv'>사유 : #=value# <span style='color:red;'> [ #=aggregates.time.sum# (분) ] </span> </div>",
				template : "#=changeNonOpty(reason)# ",
				editor: categoryDropDownEditorByNonOpty,
// 				editable : false,
    			attributes: {
        			style: "text-align: center; font-size: " + getElSize(35) + "px"
      			}
			}]
		}).data("kendoGrid")
		
		//아래 group header 제외 하고 excel 다운로드 하는 로직
		var exportFlag = false;
		$("#grid").data("kendoGrid").bind("excelExport", function (e) {
			
			console.log(e);
			console.log(e.data);
			console.log(e.sender.columns);
			console.log(e.workbook.sheets[0]);
			
			var data = e.data;
			var gridColumns = e.sender.columns;
			var sheet = e.workbook.sheets[0];
			var visibleGridColumns = [];
			var columnTemplates = [];
			var dataItem;
			// Create element to generate templates in.
			var elem = document.createElement('div');
			
			// Get a list of visible columns
			for (var i = 0; i < gridColumns.length; i++) {
				if (!gridColumns[i].hidden) {
					visibleGridColumns.push(gridColumns[i]);
				}
			}
			// Create a collection of the column templates, together with the current column index
			for (var i = 0; i < visibleGridColumns.length; i++) {
				if (visibleGridColumns[i].template) {
					columnTemplates.push({ cellIndex: i, template: kendo.template(visibleGridColumns[i].template) });
				}
			}
			
			/* // Traverse all exported rows.
			for (var i = 1; i < sheet.rows.length; i++) {
//	 			console.log("3333333")
				var row = sheet.rows[i];
				// Traverse the column templates and apply them for each row at the stored column position.
				// Get the data item corresponding to the current row.
				var dataItem = data[i - 1];
				if(dataItem!=undefined){
					for (var j = 0; j < columnTemplates.length; j++) {
						var columnTemplate = columnTemplates[j];
						// Generate the template content for the current cell.
						elem.innerHTML = columnTemplate.template(dataItem);
						if (row.cells[columnTemplate.cellIndex] != undefined)
						  // Output the text content of the templated cell into the exported cell.
							row.cells[columnTemplate.cellIndex].value = elem.textContent || elem.innerText || "";
					}
				}
			} */
			
			console.log(e.workbook.sheets[0].rows)
			$(e.workbook.sheets[0].rows).each(function(idx,data){
// 				console.log(data.cells)
				$(data.cells).each(function(id,item){
					if(id==5){
						console.log(item)
						item.value = changeNonOpty(item.value)
					}
				})
			})
// 			e.preventDefault();
			
			e.workbook.sheets.forEach(function (sheet) {
				sheet.rows = sheet.rows.filter(r => r.type != "group-header");
			});
			if (!exportFlag) {
				e.sender.hideColumn(1);
				e.preventDefault();
				exportFlag = true;
				setTimeout(function () {
					e.sender.saveAsExcel();
				
				});
			} else {
				e.sender.showColumn(1);
				exportFlag = false;
			}
		});
				
		getTable();
	}
	
	function categoryDropDownEditorByNonOpty(container, options) {
		$('<input name="' + options.field + '"/>')
           .appendTo(container)
           .kendoDropDownList({
           	valuePrimitive: true,
               dataTextField: "nonOpName",
               dataValueField: "nonOpTy",
//                dataValueField: "nonOpTy",
               dataSource: nonOpTyDataSource,
               change:function(e){
            	   gridCss();
               }
         });
    }
	
	
	function gridCss(){
		
		$(".kendo thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$(".kendo tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px"
		})
	
		$("#nonOpList thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$("#nonOpList tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px",
			"text-align" : "center"
		})
		// check box 사이즈
		$("#checkallOp").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})

		// check box 사이즈
		$("#checkall").css({
			"width" : originWidth * 0.035,
			"height" : originWidth * 0.035
		})
		
		// check box 사이즈
		$(".checkbox").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})
		
		$(".k-grouping-row td").css({
			"color" : "white",
			"text-align" : "left"
		})
		
		$(".headerDiv").css({
			"display" : "-webkit-inline-box"
			,"width" : getElSize(740)
		})
		
// 		$(".k-i-collapse").remove()
		
	}
	
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getTable();
				
			}
		});
	}
	
	function changeNonOpty(num){
		var length = nonOpTy.length;
		for(var i=0;i<length;i++){
			if(nonOpTy[i].nonOpTy==num){
				return nonOpTy[i].nonOpName
			}else if(nonOpTy[i].nonOpTy=="비가동사유"){
				return "비가동사유"
			}
		}
		return "사유를 선택해주세요"
	}
	
	function getTable(){
		
// 		var param = "sDate=" + $("#sDate").val() +
// 					"&eDate=" + $("#eDate").val();
		
		var url = "${ctxPath}/chart/getNonOpHist.do";
		
		var diff = new Date($("#eDate").val()).getTime()-new Date($("#sDate").val()).getTime();
			diff = diff/1000/60/60/24;
		
		if(diff>186){
			alert("6개월 이상 조회하실수 없습니다.")
			return;
		}
		
// 			alert("diff : " + diff);
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val();
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading();

				var json = data.dataList;
				console.log(json)
				
				$(json).each(function(idx,data){
					data.name = decode(data.name)
					data.oprNo = decode(data.oprNo);
					data.reason = data.reason;
					data.time = Number(data.time);
					data.beforereason = data.reason;
					data.checkSelect = false;
				})
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
// 	                autoSync: true,
	                group: [{
		                	field:"name"
		                	,aggregates : [{
		                		field : "time"
		                		,aggregate : "sum"
		                	}]
		                	,dir:"asc"
	                	}/* ,{
	                		field:"reason"
	                		,dir:"asc"
                			,aggregates : [{
		                		field : "time"
		                		,aggregate : "sum"
		                	}]
                		} */],
	                sort: [{
                    	field: "name" , dir:"asc" 
                    },{
                    	field: "sDate" , dir:"asc"
                    }],
// 					aggregate: [
// 						{ field: "time", aggregate: "sum" },
// 					],	
	                schema: {
	                    model: {
							id: "a",	 
							fields: {
								name :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
								checkerOp :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	oprNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	chartStatus: { editable: false,validation: { required: false },nullable:true },
		                    	sDate: { editable: false,validation: { required: false },nullable:true },
		                    	eDate: { editable: false,validation: { required: false },nullable:true },
		                    	time: { editable: false,validation: { required: false },nullable:true },
		                    	reason: { editable: true,validation: { required: false },nullable:true },
		                    }
	                    }
	                }
	            });
				
				kendotableOp.setDataSource(dataSource)
			}
		})
	}
	
	function saveRow(){
		var savelist = [];
		var json = kendotableOp.dataSource.data();
		// 선택되어 있는 리스트 가져오기
		$(json).each(function(idx,data){
			//변경된 사항만 저장하기
			console.log(data.beforereason + " , " + data.reason);
			if(data.beforereason!=data.reason){
				savelist.push(data)
			}
/* 			if(data.checkSelect==true){
			} */
		})
		
		if(savelist.length==0){
			alert("변경된 사항이 없습니다.");
			return ;
		}
		
		for(i=0 , len = savelist.length; i < len; i++) {
			if(savelist[i].reason==0){
				alert("비가동 사유를 선택해 주세요.");
				return;
			}
		}

		
		var obj = new Object();
		obj.val = savelist;

// 		var url = "${ctxPath}/chart/addNonOpHst_One.do";
		var url = "${ctxPath}/chart/nonOpHistoryUpdate.do";
		var param = "val=" + JSON.stringify(obj);

		//비가동 사유 입력 
		console.log("--저장리스트--")
		console.log(param)
// 		console.log("wow")
// 		return;
		
		$.ajax({
				url : url,
				data : param,
				type : "post",
				success : function(data){
					if(data=="success"){
						getTable();
						alert("저장되었습니다;");
						return;
					}else if(data=="fail"){
						alert("저장에 실패하셨습니다. 관리자에게 문의해 주세요_0930");
						return;
					}
					console.log(data);
				}
		})
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				작업자 : <select id="worker"></select>
				　날짜 <input type="text" id="sDate" class="date" readonly="readonly">
				 ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
				<button onclick="saveRow()">
					저장
				</button>
			</div>

			<div style="display: table-cell; vertical-align: middle;">
				<span id="historyPage" onclick="location.href='${ctxPath}/chart/NonOperation.do'">비가동 보고</span>
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
			
			</div>
		</div>
	</div>	
</body>
</html>	