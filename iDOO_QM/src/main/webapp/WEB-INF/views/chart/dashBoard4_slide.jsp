<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	/* window.addEventListener("keydown", function (event) {
		if (event.keyCode == 39) {
			location.href = ctxPath + "/chart/main.do";
		}else if (event.keyCode == 37) {
			location.href = ctxPath + "/chart/main3.do";
		};
	}, true); */
	
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : -getElSize(100)
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			url = "${ctxPath}/chart/main.do";
			location.href = url;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else{
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		};
	};
	
	$(function(){
		$(".menu").click(goReport);
		getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
		
		$("#title_right").click(function(){
			location.href = "${ctxPath}/chart/main.do";
		});
		
		$("#title_left").click(function(){
			location.href = "${ctxPath}/chart/main3.do";
		});
		
		setElement();
		
		setInterval(time, 1000);
		setInterval(getList, 1000 * 10);
		
		/* $("#title_left").click(function(){
			location.href = ctxPath + "/chart/main3.do";
		});
		
		$("#title_right").click(function(){
			location.href = ctxPath + "/chart/multiVision.do";
		});
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
		
		//switch page
		setTimeout(function(){
			location.href = ctxPath + "/chart/multiVision.do";
		},1000*60); */
		
		getList();
	});
	
	var cnt = 15;
	var cList = 0;
	var totalList = 0;
	
	function getList(){
		var url = ctxPath + "/mes/mesTest.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				var json = $.parseJSON(data);
				
				var tr = "";
				var tHead = "<tr style='color: black; background-color: #fff8dc; font-weight: bold; font-size: " + getElSize(60) + "px' class='tr'>"+
								"<td   align='center' class='td'>"+
									"오더번호"+
								"</td>"+
								"<td   align='center'>"+
									"품번"+
								"</td>"+
								"<td   align='center'>"+
									"품명"+
								"</td>"+
								"<td   align='center'>"+
									"소요일"+
								"</td>"+
								"<td   align='center'>"+
									"소요기종호기"+
								"</td>"+
								"<td   align='center'>"+
									"생산월"+
								"</td>"+
							"</tr>";
							
				$(json).each(function(i, data){
					totalList = i;
					if(i>=(cnt-15) && i<cnt){
						cList++;
						tr += "<tr style='font-size: " + getElSize(50) + "px; font-weight: bold;'>" + 
						"<td style='padding: " + getElSize(10) + "px;'>" + data.aufnr + "</td>" + 
						"<td style='padding: " + getElSize(10) + "px;'>" + data.matnr + "</td>" + 
						"<td style='padding: " + getElSize(10) + "px;'>" + data.maktx + "</td>" + 
						"<td style='padding: " + getElSize(10) + "px;'>" + data.bdter + "</td>" + 
						"<td style='padding: " + getElSize(10) + "px;'>" + data.equnr + "</td>" + 
						"<td style='padding: " + getElSize(10) + "px;'>" + data.emonu + "</td>" + 
						"</tr>";
					}
				});
				
				cnt += 15
				$("#listCnt").html((cList) + "/" + (totalList+1));
				$("#mainTable").html(tHead + tr);
				
				if(cnt>=totalList){
					cnt = 15;
					cList = 0;
				};
			}
		});
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$("#mainTable").css({
			"left" : width/2 - $("#mainTable").width()/2
		});
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2)
		});
		
		$("#title_main").css({
			"width" : getElSize(1200),
			"top" : $("#container").offset().top + (getElSize(50)),
		});
		
		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2),
			"font-size" : getElSize(130)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"left" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"width" : getElSize(300),
			"right" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("hr").css({
			"width" : contentWidth * 0.9,
			"top" : $("#container").offset().top + getElSize(250),
		});
		
		$("hr").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("hr").width()/2,
		});
		
		$("#listCnt").css({
			"font-size": getElSize(50),
			"right": $("#container").offset().left + getElSize(200),
			"top" : $("#container").offset().top + getElSize(300),
		});
		
		$("#mainTable").css({
			"width" : contentWidth * 0.9,
			"top" : $("#container").offset().top + getElSize(360)
		});
		
		$("#mainTable").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("#mainTable").width()/2,
		});
		
		$(".tr").css({
			"font-size" : getElSize(55)
		});
		
		$(".td").css({
			"padding" : getElSize(20)
		});
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : -getElSize(100),
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
</script>
<style>
#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	background-color: black;
  	font-family:'Helvetica';
	background-size : 100% 100%;
	overflow: hidden;
}

.title{
	top : 50px;
	position: absolute;
}

#title_main{
	width: 1000px;
	z-index: 999;
	font-weight: bolder;
	color : white;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#time{
	position: absolute;
	top: 170px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 

hr{
	position:absolute;
	z-index: 99;
	border: 2px solid white;
}

#mainTable{
	z-index: 99;
  	position: absolute;
	background: linear-gradient(white, gray); /* Standard syntax */
 	opacity : 0.7;
}

#mainTable tr:last-child td:first-child { 
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

.tr{
	font-size: 30px; font-weight: bold; padding: 50px;
}
#title{
	font-size: 130px;
	font-weight: bolder;
	color : white;
}
.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style>

<script type="text/javascript">
	
</script>

</head>


<body>
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<center><font id="title_main" class="title">가공 결품 예상 현황</font></center>
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title">	
	
	<hr>

	<font id="date"></font>
	<font id="time"></font>
	<font id="listCnt" style="color: white;font-weight: bolder;font-size: 50; position: absolute; right: 200px;"></font>
	<br>
	<br>


	<center>
		<table id="mainTable" border="2px" > 
	
		</table>
	</center>
</body>
</html>