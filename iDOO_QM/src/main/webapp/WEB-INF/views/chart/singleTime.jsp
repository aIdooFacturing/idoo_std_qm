<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

.groupCss td{
	background: #363636 !important;
}

#grid thead tr th{
	text-align: center;
	vertical-align: middle;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("maintenance", "singleTime")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => getTable()
		
		//getMene();
		getDeviceList();
// 		getGrid();

		setEl();
	})
	
	//비가동 사유 리스트 담기 위한 변수
	var nonOpTy = [];

	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
// 				var diff = new Date($("#eDate").val()).getTime()-new Date(e).getTime();
// 				diff = diff/1000/60/60/24;
// 				console.log(e)
// 				if(diff>90){
// 					alert("90일 이상 조회하실수 없습니다._s")
// 					$("#sDate").val(moment($("#eDate").val()).subtract('days',90).format("YYYY-MM-DD"))
// 					getTable();
// 				}else{
// 			    	//e == 날짜 
// 			    	getTable();
// 			    	$("#sDate").val(e);
// 				}
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
// 		    	var diff = new Date(e).getTime()-new Date($("#sDate").val()).getTime();
// 				diff = diff/1000/60/60/24;
				
// 				if(diff>90){
// 					alert("90일 이상 조회하실수 없습니다._d")
// 					$("#eDate").val(moment($("#sDate").val()).add('days',90).format("YYYY-MM-DD"))
// 					getTable();
// 				}else{
// 			    	//e == 날짜 
// 			    	getTable();
// 			    	$("#eDate").val(e);
// 				}
				
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
		
		$("#device").css({
			"width" : getElSize(550)
		})
		
		$("#historyPage").css({
			"background" : "green"
			,"padding-top" : getElSize(40)
			,"padding-bottom" : getElSize(40)
			,"padding-left" : getElSize(75)
			,"padding-right" : getElSize(75)
			,"border-radius" : getElSize(15)
			,"cursor" : "pointer"
		})
	}
	
	function getMenu(){
		nonOpTyDataSource = new kendo.data.DataSource({});
		
		//이유 선택박스 가지고오기
		var url = ctxPath + "/chart/getNonOpTy.do"
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var options = ""
				$(json).each(function(idx, data){
					
					var obj = {
							"nonOpTy" : data.id,
							"nonOpName" : decode(data.nonOpTy)
					}
					
					nonOpTyDataSource.add(obj)
					nonOpTy.push(obj)
				});
				
				getGrid();
			}	
		})
	}
	
	//비가동 전체 선택기능
	function checkOpAll(e){
		console.log($("#checkallOp").is(":checked"))
		if($("#checkallOp").is(":checked")){
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		kendotableOp.dataSource.fetch();
	}
	
	//체크박스 tbody 선택시 (비가동)
	function checkRowOp(e){
		console.log("event click")
		var gridList=kendotableOp.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		
		console.log(dataItem)
		
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = gridOp.dataItem(row)
		 console.log(initData)
		 
		 if(initData.checkSelect){
			 initData.checkSelect = false;
		 }else{
			 initData.checkSelect = true;
		 }
		 kendotableOp.dataSource.fetch();
	}
	var aaa
	function getGrid(){
		
		$("#grid").empty();
		
		kendotableOp = $("#grid").kendoGrid({
			editable : false
			,height : getElSize(1600)
			,dataBound : function(e) {
				gridOp = this;
				gridCss()
				
				$(e.sender.element).find('[data-role="autocomplete"]').each(function(){
					$(this).data("kendoAutoComplete").setOptions({autoWidth:true})
					test = $(this)
					console.log($(this))
				})
			}
			,toolbar: [{
				name:"excel"
			}]
			,excel:{
	            fileName:"가동분석" +  ".xlsx"
	        }
			,filterable: {
			      mode: "row"
			}
			,columns : [/* {
				field:"checkerOp",
				title:"<input type='checkbox' id='checkallOp' onclick='checkOpAll(this)'>"
				,width:originWidth * 0.055,
				template: "<input type='checkbox' onclick='checkRowOp(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" 
			}, */{
				title : "장비"
				,field : "name"
				,width : getElSize(440)
			},{
				title : "part<br>count"
				,field : "cnt"
				,width : getElSize(190)
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
			},{
				title : "프로그램"
				,field : "mainPrgmName"
				,width : getElSize(380)
			},{
				title : "프로그램 명"
				,field : "prgmHead"
				,width : getElSize(390)
// 				,groupFooterTemplate : function(e){
// 					if(e.prgmStart!=undefined){
// 						return "소계 시간 : " + kendo.toString(e.prgmStart.min,"yyyy-MM-dd HH:mm:ss") + " ~ " + e.prgmEnd.max
	
// 					}else{
// 						return "nodisplay"
// 					}
// 				}
			},{
				title : "시작시간"
				,field : "prgmStart"
				,format:"{0: yyyy-MM-dd HH:mm:ss}"
				,width : getElSize(500)
 				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
			},{
				title : "종료시간"
				,field : "prgmEnd"
				,width : getElSize(500)
				,format:"{0: yyyy-MM-dd HH:mm:ss}"
				,filterable: {
                    cell: {
                        enabled: false
                    }
	             }
			},{
				title : "Time ( 분 ) "
				,columns :[{
	    			field: "INCYCLE",
					title: "IN-CYCLE",
					width : getElSize(220),
					/* groupFooterTemplate: function(e){
						if(e.INCYCLE!=undefined){
							return "합계 : " + e.INCYCLE.sum.toFixed(1)
						}
					}, */headerAttributes: {
				    	style: "background: " + incycleColor
				    }
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
	    			field: "CUT",
					title: "CUT",
					width : getElSize(220),
					/* groupFooterTemplate: function(e){
						if(e.CUT!=undefined){
							return e.CUT.sum.toFixed(1)
						}
					}, */headerAttributes: {
				    	style: "background: " + cutColor
				    }
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
	    			field: "WAIT",
					title: "WAIT",
					width : getElSize(220), 
					/* groupFooterTemplate: function(e){
						if(e.WAIT!=undefined){
							return e.WAIT.sum.toFixed(1)
						}
					}, */headerAttributes: {
				    	style: "background: " + waitColor
				    }
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				},{
	    			field: "ALARM",
					title: "ALARM",
					width : getElSize(220),
					/* groupFooterTemplate: function(e){
						if(e.ALARM!=undefined){
							return e.ALARM.sum.toFixed(1)
						}
					}, */headerAttributes: {
				    	style: "background: " + alarmColor
				    }
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
				}]
			}]
		}).data("kendoGrid")
		
		//엑셀 다운시 설정
		excelSetting();
		
	}
	
	//엑셀 다운시 설정
	function excelSetting(){
		//아래 group header 제외 하고 excel 다운로드 하는 로직
		var exportFlag = false;
		$("#grid").data("kendoGrid").bind("excelExport", function (e) {
			
			console.log(e);
			console.log(e.data);
			console.log(e.sender.columns);
			console.log(e.workbook.sheets[0]);
			console.log(e.workbook.fileName);
			e.workbook.fileName = $("#sDate").val() + "~" + $("#eDate").val() + "_가동분석"
			var data = e.data;
			var gridColumns = e.sender.columns;
			var sheet = e.workbook.sheets[0];
			var visibleGridColumns = [];
			var columnTemplates = [];
			var dataItem;
			// Create element to generate templates in.
			var elem = document.createElement('div');
			
			// Get a list of visible columns
			for (var i = 0; i < gridColumns.length; i++) {
				if (!gridColumns[i].hidden) {
					visibleGridColumns.push(gridColumns[i]);
				}
			}
			// Create a collection of the column templates, together with the current column index
			for (var i = 0; i < visibleGridColumns.length; i++) {
				if (visibleGridColumns[i].template) {
					columnTemplates.push({ cellIndex: i, template: kendo.template(visibleGridColumns[i].template) });
				}
			}
			
			// Traverse all exported rows.
			for (var i = 1; i < sheet.rows.length; i++) {
//	 			console.log("3333333")
				var row = sheet.rows[i];
				// Traverse the column templates and apply them for each row at the stored column position.
				// Get the data item corresponding to the current row.
				var dataItem = data[i - 1];
				if(dataItem!=undefined){
					for (var j = 0; j < columnTemplates.length; j++) {
						var columnTemplate = columnTemplates[j];
						// Generate the template content for the current cell.
						elem.innerHTML = columnTemplate.template(dataItem);
						if (row.cells[columnTemplate.cellIndex] != undefined)
						  // Output the text content of the templated cell into the exported cell.
							row.cells[columnTemplate.cellIndex].value = elem.textContent || elem.innerText || "";
					}
				}
			}
			
			e.workbook.sheets.forEach(function (sheet) {
				sheet.rows = sheet.rows.filter(r => r.type != "group-header");
			});
			if (!exportFlag) {
				e.sender.hideColumn(1);
				e.preventDefault();
				exportFlag = true;
				setTimeout(function () {
					e.sender.saveAsExcel();
				
				});
			} else {
				e.sender.showColumn(1);
				exportFlag = false;
			}
			
			var grid = $("#grid").data("kendoGrid");
			grid.hideColumn("partCount");
			
		});
	}
	
	function gridCss(){

		//총 footer 없애기 위해서
		$('#grid tbody tr').each(function(){
	    	 if($(this).text().indexOf("nodisplay")==-1){
	    		 $(this).addClass('fontSize')
	    	 }else{
	    		 $(this).addClass('nodisplay')
	    	 }
	    	 
		})
		
		//소계 색상 바꾸기
		$(".k-group-footer td").css({
			"background":"#D8D8D8"
			,"color":"black"
			,"white-space": "nowrap"
		    ,"overflow": "visible"
		    ,"font-family": "monospace"
// 		    ,"font-size": getElSize(42)
		})
		
		$(".fontSize").css({
			"font-size" : getElSize(40)
		})
		//총 footer 안보이게 하기위해
		$(".nodisplay").css({"font-size":0})
		
		//2번째 그룹한거 없애기 위해서
		$(".k-group-cell").closest("tr.k-grouping-row").removeClass().addClass("groupCss")
		
		$(".groupCss").css({
			"line-height" : 0
			,"font-size" : getElSize(4)
		})
		
		$(".kendo thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$(".kendo tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px"
		})
	
		$("#nonOpList thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$("#nonOpList tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px",
			"text-align" : "center"
		})
		// check box 사이즈
		$("#checkallOp").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})

		// check box 사이즈
		$("#checkall").css({
			"width" : originWidth * 0.035,
			"height" : originWidth * 0.035
		})
		
		// check box 사이즈
		$(".checkbox").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})
		
		$(".k-grouping-row td").css({
			"color" : "white",
			"text-align" : "left"
		})
		
// 		$(".groupCss .k-i-collapse").remove()
		$(".k-i-collapse").remove()
		
	}
	
	//getJigList4Report
	function getDeviceList(){
		var url = ctxPath + "/chart/getJigList4Report.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		console.log(param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dvcList;
				console.log(json)
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#device").html(option);
				getGrid();
				
				getSimpleTable();
			}
		});
	}
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getTable();
				
			}
		});
	}

	function getTable(){
		
// 		var param = "sDate=" + $("#sDate").val() +
// 					"&eDate=" + $("#eDate").val();
		
		var url = "${ctxPath}/chart/getsingleTime.do";
		
		var diff = new Date($("#eDate").val()).getTime()-new Date($("#sDate").val()).getTime();
			diff = diff/1000/60/60/24;
		
		if(diff>90){
			alert("90일 이상 조회하실수 없습니다.")
			$("#sDate").val(moment($("#eDate").val()).subtract('days',90).format("YYYY-MM-DD"))
			return;
		}
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&dvcId="+ $("#device").val();
		
		console.log(param);
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				$("#grid").empty();
				getGrid();
				
				$.hideLoading();
				
				$("#detailBtn").css({
					"background" : "rgb(58 134 218)"
				});
				
				$("#simpleBtn").css({
					"background" : "rgb(155, 155, 155)"
				});

				var json = data.dataList;
				console.log(json)
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.cnt = data.partCount;
					data.INCYCLE = Number((data.INCYCLE/60).toFixed(1));
					data.CUT = Number((data.CUT/60).toFixed(1));
					data.WAIT = Number((data.WAIT/60).toFixed(1));
					data.ALARM = Number((data.ALARM/60).toFixed(1));
				})
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
	                autoSync: true,
	                group: [{
	                	field:"name",dir:"asc"
	                },{
	                	field:"partCount",dir:"asc"
                		,aggregates : [{
	                		field : "prgmStart" , aggregate: "min"
	                	},{
	                		field : "prgmEnd" , aggregate: "max"
	                	},{
	                		field : "INCYCLE" , aggregate: "sum"
	                	},{
	                		field : "CUT" , aggregate: "sum"
	                	},{
	                		field : "WAIT" , aggregate: "sum"
	                	},{
	                		field : "ALARM" , aggregate: "sum"
	                	}]
	                }],
	                sort: [{
                    	field: "name" , dir:"asc" 
                    },{
                    	field: "partCount" , dir:""
                    },{
                    	field: "prgmStart" , dir:"asc"
                    }],
// 					aggregate: [
// 						{ field: "prgmStart", aggregate: "min" },
// 						{ field: "prgmEnd", aggregate: "max" },
// 					],	
	                schema: {
	                    model: {
							id: "a",	 
							fields: {
								checkerOp :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	oprNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	INCYCLE: { type:"number" },
		                    	CUT: { type:"number" },
		                    	WAIT: { type:"number" },
		                    	ALARM: { type:"number" },
		                    	prgmStart: { type:"date", format:"{0: yyyy-MM-dd HH:mm:ss}" },
// 		                    	prgmEnd: { type:"date", format:"{0: yyyy-MM-dd HH:mm:ss}" },
// 		                    	 workIdx: { type: "number" },
		                    }
	                    }
	                }
	            });
				
				kendotableOp.setDataSource(dataSource)
			}
		})
	}
	
	
	function getSimpleTable(){
		
		
		var diff = new Date($("#eDate").val()).getTime() - new Date($("#sDate").val()).getTime();
			diff = diff/1000/60/60/24;
		
		if(diff>90){
			alert("90일 이상 조회하실수 없습니다._d")
			$("#sDate").val(moment($("#eDate").val()).subtract('days',90).format("YYYY-MM-DD"))
			return;
		}
		
		var url = "${ctxPath}/chart/getKovisApiData.do";
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&dvcId="+ $("#device").val();
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){

				$("#simpleBtn").css({
					"background" : "rgb(58 134 218)"
				});
				
				$("#detailBtn").css({
					"background" : "rgb(155, 155, 155)"
				});
				
				var json = data.dataList;
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.INCYCLE = Number((data.INCYCLE/60).toFixed(1));
					data.CUT = Number((data.CUT/60).toFixed(1));
					data.WAIT = Number((data.WAIT/60).toFixed(1));
					data.ALARM = Number((data.ALARM/60).toFixed(1));
				})
				
				$("#grid").empty();
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
	                autoSync: true,
	                group: [{
	                	field:"name",dir:"asc"
	                },{
	                	field:"partCount",dir:"asc"
                		,aggregates : [{
	                		field : "prgmStart" , aggregate: "min"
	                	},{
	                		field : "prgmEnd" , aggregate: "max"
	                	},{
	                		field : "INCYCLE" , aggregate: "sum"
	                	},{
	                		field : "CUT" , aggregate: "sum"
	                	},{
	                		field : "WAIT" , aggregate: "sum"
	                	},{
	                		field : "ALARM" , aggregate: "sum"
	                	}]
	                }],
	                sort: [{
                    	field: "name" , dir:"asc" 
                    },{
                    	field: "partCount" , dir:""
                    },{
                    	field: "prgmStart" , dir:"asc"
                    }],
// 					aggregate: [
// 						{ field: "prgmStart", aggregate: "min" },
// 						{ field: "prgmEnd", aggregate: "max" },
// 					],	
	                schema: {
	                    model: {
							id: "a",	 
							fields: {
								checkerOp :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	oprNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	INCYCLE: { type:"number" },
		                    	CUT: { type:"number" },
		                    	WAIT: { type:"number" },
		                    	ALARM: { type:"number" },
		                    	prgmStart: { type:"date", format:"{0: yyyy-MM-dd HH:mm:ss}" },
// 		                    	prgmEnd: { type:"date", format:"{0: yyyy-MM-dd HH:mm:ss}" },
// 		                    	 workIdx: { type: "number" },
		                    }
	                    }
	                }
	            });
				
				kendotableOp = $("#grid").kendoGrid({
					dataSource : dataSource
					,editable : false
					,height : getElSize(1600)
					,dataBound : function(e) {
						gridOp = this;
						gridCss()
						$(e.sender.element).find('[data-role="autocomplete"]').each(function(){
							$(this).data("kendoAutoComplete").setOptions({autoWidth:true})
							test = $(this)
							console.log($(this))
						})
					}
					,toolbar: [{
						name:"excel"
					}]
					,excel:{
			            fileName:"가동분석" +  ".xlsx"
			        }
					,filterable: {
					      mode: "row"
					}
					,columns : [/* {
						field:"checkerOp",
						title:"<input type='checkbox' id='checkallOp' onclick='checkOpAll(this)'>"
						,width:originWidth * 0.055,
						template: "<input type='checkbox' onclick='checkRowOp(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" 
					}, */{
						title : "장비"
						,field : "name"
						,width : getElSize(440)
						,
					},{
						title : "생산수량"
						,field : "cnt"
						,width : getElSize(190)
						,filterable: {
		                    cell: {
		                        enabled: false
		                    }
			             }
					},{
						title : "프로그램"
						,field : "mainPrgmName"
						,width : getElSize(380)
					},{
						title : "프로그램 명"
						,field : "prgmHead"
						,width : getElSize(390)
//		 				,groupFooterTemplate : function(e){
//		 					if(e.prgmStart!=undefined){
//		 						return "소계 시간 : " + kendo.toString(e.prgmStart.min,"yyyy-MM-dd HH:mm:ss") + " ~ " + e.prgmEnd.max
			
//		 					}else{
//		 						return "nodisplay"
//		 					}
//		 				}
					},{
						title : "Time ( 분 ) "
						,columns :[{
			    			field: "INCYCLE",
							title: "IN-CYCLE",
							width : getElSize(220),
							/* groupFooterTemplate: function(e){
								if(e.INCYCLE!=undefined){
									return "합계 : " + e.INCYCLE.sum.toFixed(1)
								}
							}, */headerAttributes: {
						    	style: "background: " + incycleColor
						    }
							,filterable: {
			                    cell: {
			                        enabled: false
			                    }
				             }
						},{
			    			field: "CUT",
							title: "CUT",
							width : getElSize(220),
							/* groupFooterTemplate: function(e){
								if(e.CUT!=undefined){
									return e.CUT.sum.toFixed(1)
								}
							}, */headerAttributes: {
						    	style: "background: " + cutColor
						    }
							,filterable: {
			                    cell: {
			                        enabled: false
			                    }
				             }
						},{
			    			field: "WAIT",
							title: "WAIT",
							width : getElSize(220), 
							/* groupFooterTemplate: function(e){
								if(e.WAIT!=undefined){
									return e.WAIT.sum.toFixed(1)
								}
							}, */headerAttributes: {
						    	style: "background: " + waitColor
						    }
							,filterable: {
			                    cell: {
			                        enabled: false
			                    }
				             }
						},{
			    			field: "ALARM",
							title: "ALARM",
							width : getElSize(220),
							/* groupFooterTemplate: function(e){
								if(e.ALARM!=undefined){
									return e.ALARM.sum.toFixed(1)
								}
							}, */headerAttributes: {
						    	style: "background: " + alarmColor
						    }
							,filterable: {
			                    cell: {
			                        enabled: false
			                    }
				             }
						}]
					}]
				}).data("kendoGrid")
				
				//액셀 다운시 설정
				excelSetting();
				
				$.hideLoading();
				
			}
		})
	}

	</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				장비 : <select id="device"></select>
				　날짜 <input type="text" id="sDate" class="date" readonly="readonly">
				 ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button id="simpleBtn" onclick="getSimpleTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 단순 화면 조회
				</button>
				<button id="detailBtn" onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 상세 화면 조회
				</button>
<!-- 				<button onclick="saveRow()"> -->
<!-- 					저장 -->
<!-- 				</button> -->
			</div>

		</div>
		
		<div id="div2">
			<div id="grid">
			
			</div>
		</div>
	</div>	
</body>
</html>	