<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<title>Multi Vision.</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);

	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};

	var sideMargin;
	$(function(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2)
		});
		
		$("#svg").css({
			"top" : $("#container").offset().top,
			width : contentWidth/2,
			height : contentHeight/2,
			left : $("#container").offset().left
		});
		
		$("#part2").css({
			width : contentWidth/2,
			height : contentHeight/2,
			"top" : $("#container").offset().top,
			right : $("#container").offset().left
		});
		
		$("#part3").css({
			width : contentWidth/2,
			height : contentHeight/2,
			top : contentHeight/2 + $("#container").offset().top,
			left : $("#container").offset().left
		});
		
		$("#part4").css({
			width : contentWidth/2,
			height : contentHeight/2,
			top : contentHeight/2 + $("#container").offset().top,
			right : $("#container").offset().left
		});
		
		sideMargin = $("#container").offset().left;
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : -getElSize(100),
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
	});
</script>

<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/solid-gauge.js"></script>
<script src="${ctxPath }/js/chart/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/part1.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/part2.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/part3.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/part4.js"></script> 

<link rel="stylesheet" href="${ctxPath }/css/part2.css"/>
<link rel="stylesheet" href="${ctxPath }/css/part3.css"/>
<link rel="stylesheet" href="${ctxPath }/css/part4.css"/>  

<style>
/* #loader{
	position: absolute;
	width: 250px;
	-webkit-filter: brightness(10);
	z-index: 99999;
}
 */
#time{
	position: absolute;
	top: 85px;
	font-size : 15px;
	color: white;
} 
 
 .unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}

#date{
	position: absolute;
	top: 85px;
	font-size : 15px;
	color: white;
} 
.chartTitle{
	position : absolute;
	top:-100;
	left:20%;
	right:0;
	margin:auto;
	font-size : 50;
}


#container{
	background-size : 100% 100%;
	background-color: white;
}

body{
	-moz-text-size-adjust: none;
   	text-size-adjust: none;
	-webkit-text-size-adjust: none;
	min-font-size:5; 
	width: 100%;
	height: 100%;
	background-color: black;
	padding: 0px;
	margin: 0px;
	font-family:'Helvetica';
	background-size : 100% 100%;
}

#chart{
	overflow: hidden;
}

#svg{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	position: absolute;
}

#part1, #part2, #part3, #part4{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	position: absolute;
}
.title{
	position: absolute;
	z-index: 99;
}

#title_main{
	width: 500px;
}

#title_left{
	left : 25px;
	width: 150px;
}
#pieChart1{
	position: absolute;
	left : 450px;
	z-index: 99;
	top: 740px;
	height: 350px;
	width: 360px;
}

#pieChart2{
	position: absolute;
	/* left : 200px; */
	z-index: 99;
	/* top: 740px; 
	height: 350px;
	width: 360px; */
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}


#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.tr1{
	font-size: 15px;
}

.td1{
	padding: 10px;
	border: 1px solid white; 
}

#go123{
	position: absolute;
	z-index: 999;
	width: 300;
}
#textBackground{
	background-color: white;
	width: 430px;
	height: 190px;
	z-index: 1;
	position: absolute;
	border-radius : 10px;
	opacity : 1;
	top: 860;
	left: 1260;
}
#mainText{
	position: absolute;
	z-index: 2;
	font-size: 40px;
	font-weight: bolder;
	text-align: right;
	top: 870;
	left: 1280;
}
#tableDiv{
	left: 10px;
	width: 300px; 
	position: absolute; 
	z-index: 999;
	top: 225px;
}

</style> 
<script type="text/javascript">
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
		
	};
	
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : -getElSize(100)
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			url = "${ctxPath}/chart/main.do";
			location.href = url;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else{
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		};
	};
	
	$(function(){
		$(".menu").click(goReport);
		getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		
		
		document.oncontextmenu = function() {stopLoop()};
		setDivPos();
		
		setInterval(time, 1000);
		
		$("#svg").click(function(){
			location.href = "${ctxPath}/chart/main.do";
		});
		
		$("#part2").click(function(){
			location.href = "${ctxPath}/chart/main3.do";
		});
		
		$("#part3").click(function(){
			location.href = "${ctxPath}/chart/main2.do";
		});
		
		$("#part4").click(function(){
			location.href = "${ctxPath}/chart/main4.do";
		});
		
		//getNightlyMachineStatus();
		//startPageLoop();
	});
	
	function getNightlyMachineStatus(){
		var url = "${ctxPath}/device/getNightlyMachineStatus.do";
		var date =  new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var targetDate = year + "-" + month + "-" + day;
		var param = "targetDate=" + targetDate;
		
		$.ajax({
			url : url,
			data: param,
			dataType : "text",
			type : "post",
			success : function(data){
				var json = $.parseJSON(data);
				
				
				var tr = "";
				var status = "";
				$(json).each(function (i, data){
					if(data.status=="WAIT"){
						status = "완료";
					}else{
						status = "중단";
					};
					tr += "<tr>" +
							"<td  style='border:1px solid white; font-Size:14px;'>" + (i+1)  + "</td>" +
							"<td  style='border:1px solid white;font-Size:14px;'> " + data.name + "</td>" +
							"<td style='border:1px solid white;font-Size:14px;'>" + data.stopDate + "</td>" +
							"<td style='border:1px solid white;font-Size:14px;'>" + data.stopTime + "</td>" + 
							"<td style='border:1px solid white;font-Size:15px;'>" + status + "</td>" +
						"</tr>";
				});
				
				if(json.length==0){
					tr = "<tr  style='border:1px solid white; font-Size:12px;'>" + 
							"<td colspan='5'>없음</tb>" + 
						"<tr>";
				};
				
				$("#table").append(tr);
			}
		});
	};
	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			location.href = ctxPath + "/chart/main.do";
		},1000*60*10);
	};
	
	function bodyNeonEffect(color) {
		var lineWidth = 70;
		var toggle = true;
		setInterval(function() {
			$("#svg").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= 2;
				if (lineWidth == 0) {
					toggle = false;
				}
			} else if (!toggle) {
				lineWidth += 2;
				if (lineWidth == 70) {
					toggle = true;
				}
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setDivPos(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		/* 
		$("#loader").css({
			"top" : (height/2) + (height/4) - ($("#loader").height()/2),
			"left" : (width/2) - (width/4) - ($("#loader").width()/2)
 		}); */
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	
	/* window.addEventListener("keydown", function (event) {
		if(event.keyCode=="32"){
			drawRow();
		};	
	}, true); */
	
	var rowNum = 0;
	
	function drawRow(){
		var rows = $(
							"<tr style='color:white; text-align:center' class='tr'>" + 
								"<td width='84px' style='padding:5px' class='td'>" + tableRow[rowNum][0] + "</td>" + 
								"<td width='154px'style='padding:5px' class='td'>" + tableRow[rowNum][1] + "</td>" +
								"<td width='200px'style='padding:5px' class='td'>" + tableRow[rowNum][2] + "</td>" +
								"<td width='110px'style='padding:5px' class='td'>" + tableRow[rowNum][3] + "</td>" +
							"</tr>"); 
		
	
		rows.hide();
	   $('tr:last-child').after(rows);
	  	rows.fadeIn("slow");
		//$("#table").append(rows);
		rowNum++;
	};
	
	var tableRow = [
	                		["1", "5FPM", "04:25", "완료"],
	                		["2", "HF7P", "05:07", "완료"],
	                		["3", "HFMS#1", "06:10", "중단"],
	                		["4", "HFMS#2", "00:01", "중단"],
	                		["5", "D3710", "02:10", "완료"],
	                ];
</script>
</head>


<body oncontextmenu="return false"	>
	
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	
	<!-- PART1 -->
	<div id="Legend" style="position: absolute; font-size: 25px; z-index: 99999999">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> 가동 
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> 대기 
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> 알람 
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> Power off   
	</div>
	
	<!-- <div id="pieChart1"  ></div> -->
	<div id="pieChart2"></div>

	<!-- <div id="tableDiv" >
		<center><font style="font-size: 20px;color: white;font-weight: bold;" id="tableHeader">야간 무인 정지장비</font></center>
		<br>
		<table style="width: 100%; text-align: center; color: white;" id="table">
			<tr style=" font-weight: bold;" class='tr1'>
				<td class='td1'>No</td>
				<td class='td1'>설비명</td>
				<td class='td1'>날짜</td>
				<td class='td1'>정지시간</td>
				<td class='td1'>상태</td>
			</tr>
		</table>
	</div> -->
	
	<img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title" onclick="stopLoop();">
	<span id="title_right" >두산 공작기계</span>		

	<div id="svg"></div>

	<font id="date"></font>
	<font id="time"></font>
	
	
	
	<!-- PART2 -->
	<div id="part2">
		<img src="${ctxPath }/images/DashBoard/title3.svg" id="title_main2" class="title">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left2">
	<span id="title_right2">두산 공작기계</span>			
	
	<hr>

	<font id="date2"></font>
	<font id="time2"></font>
	
	<Center>
		<table id="mainTable">
			<tr>
				<td class="leftTd">
					<div id="status2_0" class="status"></div>
				</td>
				<td>
					<div id="status2_1" class="status"></div>			
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_2" class="status"></div>
				</td>
				<td>
					<div id="status2_3" class="status"></div>
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_4" class="status"></div>
				</td>
				<td>
					<div id="status2_5" class="status"></div>
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_6" class="status"></div>
				</td>
				<td>
					<div id="status2_7" class="status"></div>
				</td>
			</tr>	
			<tr>		
				<td class="leftTd">
					<div id="status2_8" class="status"></div>
				</td>
				<td>
					<div id="status2_9" class="status"></div>
				</td>
			<tr>	
				<td class="leftTd">
					<div id="status2_10" class="status"></div>
				</td>
				<td>
					<div id="status2_11" class="status"></div>
				</td>
			</tr>	
			<tr>
				<td class="leftTd">
					<div id="status2_12" class="status"></div>
				</td>
				<Td>
					<div id="status2_13" class="status"></div>
				</Td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_14" class="status"></div>
				</td>
				<td>
					<div id="status2_15" class="status"></div>
				</td>	
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_16" class="status"></div>
				</td>
				<td>
					<div id="status2_17" class="status"></div>
				</td>
			</tr>	
			<tr>		
				<Td class="leftTd">
					<div id="status2_18" class="status"></div>
				</Td>	
				<td>
					<div id="status2_19" class="status"></div>
				</td>
			</tr>	
			<tr>
				<td class="leftTd">
					<div id="status2_20" class="status"></div>
				</td>		
				<td>
					<div id="status2_21" class="status"></div>
				</td>
			</tr>
		</table>
	</Center>
	</div>
	
	
	<!-- PART3 -->
	<div id="part3">
		<div id="popup" style="background-color:white; position: absolute; display: none;border-radius : 20px;">
		<center><div id="hour" style="font-size: 60px; font-weight: bold;"></div></center>
		<div id='popupChart' style="width: 100%" ></div>
	</div>
	
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left3" >
	<span id="title_right3" >두산 공작기계</span>	
	<center><img src="${ctxPath }/images/DashBoard/title2.svg" id="title_main3" ></center>
	
	
	<hr id="hr3">

	<font id="date3"></font>
	<font id="time3"></font>
	
	<table id="mainTable">
		<tr>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 25 0 25" class="td3">
				<font style="font-weight: bold; color: white; font-size: 25px;" id="3_dvcName1" class="tableText3"></font><br>
				<span style="font-weight: bold; color: white; font-size: 25px" id="progName1" class="tableText3"></span> 
				<span style="font-weight: bold; color: white; font-size: 25px" id="progHeader1" class="tableText3"></span>

				<div id="status1" class="status3"></div>
				
				<div style="float: right; font-weight: bold; font-size: 20px; color: white; margin-right: 10px" class="mode">Mode : Automatic</div>
				<table class="subTable" style="width:100%"> 
					<tr style="color: black; background-color: #fff8dc; font-weight: bold;" >
						<td width="50%" align="center" style="padding: 10px;" class="td_title3">
							일 누적 가동현황
						</td>
						<td width="50%" align="center" class="td_title3">
							현 가동 상태
						</td>
					</tr>
					<tr class="tr">
						<td width="50%" class="pie_tr">
							<div class="pie" id="pie1"></div>
						</td>
						<td valign="top">
							<div >
								<div id="opTime1" style="float: left; width: 45%; height: 200px;margin-top: 50px;" class="gauge_tr"></div>
								<div id="cuttingTime1" style="float: right; width: 45%; height: 200px;margin-top: 50px;" class="gauge_tr"></div>
							</div>
							<div >
								<div id="spdLoad1" style="float: left; width: 45%; height: 200px" class="gauge_tr"></div>
								<div id="feedOverride1" style="float: right; width: 45%; height: 200px" class="gauge_tr"></div>
							</div>
							<div style="margin-left:25px;margin-right:25px;margin-bottom:15px;text-align: left; line-height: 25px" class="alarmDiv">
								<font style="font-size: 15px; font-weight: bold; color: red;" class="alarmText">Alarm</font><br>
								<font style="font-size: 15px; font-weight: bold; color: black;" id="alarmMsg1_0" class="alarmText"></font><br>
								<font style="font-size: 15px; font-weight: bold; color: black;" id="alarmMsg1_1" class="alarmText"></font><br>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 25 0 25" class="td3">
				<font style="font-weight: bold; color: white; font-size: 25px" id="3_dvcName2" class="tableText3"></font><br>
				<span style="font-weight: bold; color: white; font-size: 25px" id="progName2" class="tableText3"></span> 
				<span style="font-weight: bold; color: white; font-size: 25px" id="progHeader2" class="tableText3"></span>
				
				<div id="status2" class="status3"></div>
				
				<div style="float: right; font-weight: bold; font-size: 20px; color: white; margin-right: 10px" class="mode">Mode : Automatic</div>
				<table class="subTable" style="width:100%"> 
					<tr style="color: black;  background-color: #fff8dc; font-weight: bold;" >
						<td width="50%" align="center" style="padding: 10px; font-size: 20px;" class="td_title3">
							일 누적 가동현황
						</td>
						<td width="50%" align="center" style="font-size: 20px;" class="td_title3">
							현 가동 상태
						</td>
					</tr>
					<tr class="tr">
						<td width="50%"class="pie_tr">
							<div class="pie" id="pie2"></div>
						</td>
						<td valign="top">
							<div >
								<div id="opTime2" style="float: left; width: 45%; height: 200px;margin-top: 50px;" class="gauge_tr"></div>
								<div id="cuttingTime2" style="float: right; width: 45%; height: 200px;margin-top: 50px;" class="gauge_tr"></div>
							</div>
							<div >
								<div id="spdLoad2" style="float: left; width: 45%; height: 200px;" class="gauge_tr"></div>
								<div id="feedOverride2" style="float: right; width: 45%; height: 200px" class="gauge_tr"></div>
							</div>
								<div style="margin-left:25px;margin-right:25px;margin-bottom:15px;text-align: left; line-height: 25px" class="alarmDiv">
									<font style="font-size: 15px; font-weight: bold; color: red;" class="alarmText">Alarm</font><br>
									<font style="font-size: 15px; font-weight: bold; color: black;" id="alarmMsg2_0" class="alarmText"></font><br>
									<font style="font-size: 15px; font-weight: bold; color: black;" id="alarmMsg2_1" class="alarmText"></font><br>
								</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div> 
	
	<div id="part4">
		<span id="title_right4" >두산 공작기계</span>
		<!-- <iframe src="http://docs.google.com/gview?url=http://106.240.234.114:8080/securobot/sample.pptx&embedded=true" style="width:100%; height:100%;" frameborder="0"></iframe> -->
		<%-- <video src="${ctxPath }/video/video_4.mp4" width="100%" autoplay="autoplay" loop="loop" ></video> --%>
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left4" >
	<center><font id="title4">가공 결품 예상 현황</font></center>
		
	<hr id="hr4">

	<font id="date4"></font>
	<font id="time4"></font>
	
	<font id="listCnt" style="color: white;font-weight: bolder;font-size: 25; position: absolute; ;"></font>	
	
	<center>
		<table id="mainTable4" border="2px" > 

		</table>
	</center>
	</div>
	
	</div>
</body>
</html>