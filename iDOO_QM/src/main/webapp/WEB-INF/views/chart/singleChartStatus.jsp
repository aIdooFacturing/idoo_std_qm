<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function drawBorder(el){
		$(el).css({
			"box-shadow" : "0px 0px 10px red inset"
		});
	};
	
	$(function(){
		/* console.log("------")
		console.log(moment().format("YYYY-MM-DD"))
		var curTime = new Date
		
		curTime.getHours()
		curTime.getMinutes()
		 
		console.log(curTime)
		if(curTime.getHours()<=8 && curTime.getMinutes() <=30){
			moment().format("YYYY-MM-DD")
		}
		$("#sDate").val(moment().format("YYYY-MM-DD"));
		 */
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var time =addZero(String(date.getHours()))+":"+addZero(String(date.getMinutes()));
		var Yday=new Date(date.setDate(date.getDate()-1));
		
		if(time<="08:30"){	//8시 30분이전일때 전날의 데이터 보여주기
			month = addZero(String(Yday.getMonth()+1));
			day = addZero(String(Yday.getDate()));
			todayChk="true";
		}
		$("#sDate").val(year + "-" + month + "-" + day);
	})
	
	var first = true;
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + $("#sDate").val() + 
					"&eDate=" +  $("#sDate").val() + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var list = "<option value='-1'>Auto</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvcId").html(list).change(function(){
					window.localStorage.setItem("dvcId", $("#dvcId").val());
					window.localStorage.setItem("name", $("#dvcId option:selected").text());
				});
				
				$("#dvcId").css({
					"font-size" : getElSize(70),
					//"margin-bottom" : getElSize(10),
					"width" : getElSize(500),
					"-webkit-appearance": "none",
				    "-moz-appearance": "none",
			    	"appearance": "none",
			    	//"margin-top" : getElSize(100)
				});
				
				if(first){
					//localstorage에 저장 된 dvcid get
					for(var i = 0; i < dvcArray.length; i++){
						if(selectedDvcId == dvcArray[i]){
							dvcIdx = i;
						}
					}
					
					first = false;
				}
				
				/* try{
					dvcId = dvcArray[dvcIdx];
				} catch(err){
					dvcId = dvcArray[0];
				} */
				dvcId = dvcArray[dvcIdx];
				getDvcData()
				$("#dvcId").val(dvcId);
				
				dvcIdx++;
				if(dvcIdx>=dvcArray.length) dvcIdx=0;
				
				//getDvcId();
				getDetailData();
				getCurrentDvcStatus(dvcId)
			}
		});
	};
	
	/*  
	가동율, 정분율, 타임차트(원형), 스핀들로드
	*/
	function getCurrentDvcStatus(dvcId){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" +  $("#sDate").val();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var cuttingTime = data.cuttingTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				
				if(data.chartStatus=="null"){
					name = window.sessionStorage.getItem("name");
				}
				
				
				//spd, feed
				$("#opratio_gauge").empty();
				$("#opratio_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(opRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
				
				$("#cutting_gauge").empty();
				$("#cutting_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(cuttingRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
						
				
				$("#opratio_span").html("${op_ratio}").css({
					"top" : getElSize(1780),
					"left" :$("#opratio_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#opratio_span").width()/2)- marginWidth
				});
				
				$("#cuttingratio_span").html("${cuttingratio}").css({
					"top" : getElSize(1780),
					"left" :$("#cutting_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#cuttingratio_span").width()/2)- marginWidth
				});
				
				//$("#progName" + idx).html(progName);
				//$("#progHeader" + idx).html(progHeader);
				
				if(String(type).indexOf("IO")!=-1){
					//$("#progHeader" + idx).append("I/O Logik");
				};
				
				//var pieChart = $("#pie" + idx).highcharts();
				
				//console.log(idx)
				//var a = (24*60*60) - (inCycleTime + waitTime+ alarmTime + noConTime);
				
				//drawPieData(chartStatus, inCycleTime - cuttingTime, cuttingTime, waitTime, alarmTime, noConTime ,a, pieChart);

				
				drawPieChart()
				
				/* var opTimeChart = $("#opTime" + idx).highcharts().series[0].points[0];
				opTimeChart.update(Number(Number(opRatio).toFixed(1)));
			
				var cuttingTime = $("#cuttingTime" + idx).highcharts().series[0].points[0];
				cuttingTime.update(Number(Number(cuttingRatio).toFixed(1))); */
				
				$("#inCycleSpan").html((inCycleTime/60/60).toFixed(1));
				$("#waitSpan").html((waitTime/60/60).toFixed(1));
				$("#alarmSpan").html((alarmTime/60/60).toFixed(1));
				$("#noConnSpan").html((noConTime/60/60).toFixed(1));
				$("#CutSpan").html((cuttingTime/60/60).toFixed(1));
				
				
				pieChart.series[0].data[0].update(Number((inCycleTime/60/60).toFixed(1)))
				pieChart.series[0].data[1].update(Number((cuttingTime/60/60).toFixed(1)))
				pieChart.series[0].data[2].update(Number((waitTime/60/60).toFixed(1)))
				pieChart.series[0].data[3].update(Number(alarmTime/60/60))
				pieChart.series[0].data[4].update(Number((noConTime/60/60).toFixed(1)))
				
				if(inCycleTime==0 && waitTime==0 && alarmTime==0 && noConTime==0 ){
					pieChart.series[0].data[4].update(1)
				}
				
				setInterval(function(){
					getCurrentDvcStatus(dvcId)					
				}, 1000*60*10)
			}
		});
	};
	
	function getDvcData(){
		//getMachineName();
		/* getDetailData();
		getAlarmList();
		getRapairDataList(); */
		getStartTime();
		
		//setTimeout(getDvcData, 5000);
	};
	
	var handle = 0;
	var selectedDvcId;
	$(function(){
		selectedDvcId = window.localStorage.getItem("dvcId");
		if(selectedDvcId==null) selectedDvcId =96; 
		
		createNav("monitor_nav", 2)
		getDvcList();
		time();
		
		$("#dvcId").change(function(){
			dvcId = $("#dvcId").val(); 
			if(this.value==-1){
				rotate_flag = true;	
			}else{
				rotate_flag = false;
				getDvcData();
				getDetailData();
				getCurrentDvcStatus(dvcId);
			}
		});
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
	
		setDivPos();
		
		/* window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10); */
		
		
		chkBanner();
	});
	
	var startTimeLabel = new Array();
	var startHour, startMinute;
	
	function getStartTime(){
		var url = ctxPath + "/chart/getStartTime.do";
		var param = "shopId=" + shopId;;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split("-")[0]
				startMinute = data.split("-")[1]
				startHour = "08"
				drawBarChart("timeChart");
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});	
	};
	
	function drawBarChart(id, idx) {
		var m0 = "",
			m02 = "",
			m04 = "",
			m06 = "",
			m08 = "",
			
			m1 = "";
			m12 = "",
			m14 = "",
			m16 = "",
			m18 = "",
			
			m2 = "";
			m22 = "",
			m24 = "",
			m26 = "",
			m28 = "",
			
			m3 = "";
			m32 = "",
			m34 = "",
			m36 = "",
			m38 = "",
			
			m4 = "";
			m42 = "",
			m44 = "",
			m46 = "",
			m48 = "",
			
			m5 = "";
			m52 = "",
			m54 = "",
			m56 = "",
			m58 = "";
		
		var n = Number(startHour);
		if(startMinute!=0) n+=1;
		
		for(var i = 0, j = n ; i < 24; i++, j++){
			eval("m" + startMinute + "=" + j);
			
			startTimeLabel.push(m0);
			startTimeLabel.push(m02);
			startTimeLabel.push(m04);
			startTimeLabel.push(m06);
			startTimeLabel.push(m08);
			
			startTimeLabel.push(m1);
			startTimeLabel.push(m12);
			startTimeLabel.push(m14);
			startTimeLabel.push(m16);
			startTimeLabel.push(m18);
			
			startTimeLabel.push(m2);
			startTimeLabel.push(m22);
			startTimeLabel.push(m24);
			startTimeLabel.push(m26);
			startTimeLabel.push(m28);
			
			startTimeLabel.push(m3);
			startTimeLabel.push(m32);
			startTimeLabel.push(m34);
			startTimeLabel.push(m36);
			startTimeLabel.push(m38);
			
			startTimeLabel.push(m4);
			startTimeLabel.push(m42);
			startTimeLabel.push(m44);
			startTimeLabel.push(m46);
			startTimeLabel.push(m48);
			
			startTimeLabel.push(m5);
			startTimeLabel.push(m52);
			startTimeLabel.push(m54);
			startTimeLabel.push(m56);
			startTimeLabel.push(m58);
			
			if(j==24){ j = 0}
		};

		var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
		}, ]

		var height = window.innerHeight;

		
		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(0,0,0,0)',
				height : getElSize(200),
				marginTop : -getElSize(300),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50)
			},
			credits : false,
			title : false,
			xAxis : {
				categories : startTimeLabel,
				labels : {
					step: 1,
					formatter : function() {
						var val = this.value

						return val;
					},
					style : {
						color : "white",
						fontSize : getElSize(30),
						fontWeight : "bold"
					},
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				}
			},
			legend : {
				enabled : false
			},
			series : []
		};

		$("#" + id).highcharts(options);

		var status = $("#" + id).highcharts();
		var options = status.options;

		options.series = [];
		options.title = null;
		options.exporting = false;
		getTimeData(options);
		
		///////////////////////// demo data
		
		
//		options.series.push({
//			data : [ {
//				y : Number(20),
//				segmentColor : "red"
//			} ],
//		});
	//	
	//	
//		for(var i = 0; i < 719; i++){
//			var color = "";
//			var n = Math.random() * 10;
//			if(n<=5){
//				color = "green";
//			}else if(n<=8){
//				color = "yellow";
//			}else {
//				color = "red";
//			}
//			options.series[0].data.push({
//				y : Number(20),
//				segmentColor : color
//			});
//		};
	//	
//		status = new Highcharts.Chart(options);
		
		
		
		////////////////////////////////
		
		setInterval(function(){
			var cMinute = String(addZero(new Date().getMinutes())).substr(0,1);
			if(targetMinute!=cMinute){
				targetMinute = cMinute;
				drawBarChart(id, idx);
			};
		},5000)
	};
	
	var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	
	
	/* 
	타임차트(바)
	*/
	function getTimeData(options){
		var url = ctxPath + "/chart/getTimeData.do";
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() + 1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = addZero(String(date.getMinutes())).substr(0,1);
		
		
		if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
			day = addZero(String(new Date().getDate()+1));
		};
		
		var today = year + "-" + month + "-" + day;
		var param = "workDate=" +  $("#sDate").val() + 
					"&dvcId=" + dvcId;
		
		
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				var json = data.statusList;
				console.log(json)
				var color = "";
				
				var status = json[0].status;
				if(status=="IN-CYCLE"){
					color = "#A3D800"
				}else if(status=="WAIT"){
					color = "#FF9100";
				}else if(status=="ALARM"){
					color = "#EC1C24";
				}else if(status=="NO-CONNECTION"){
					color = "#A0A0A0";
				};
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startN = 0;
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=20){
						startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						console.log(f_Hour, f_Minute)
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					console.log(startN)
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : "#A0A0A0"
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : "#A0A0A0"
						});
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				};
				
				
				$(json).each(function(idx, data){
					spdLoadPoint.push(Number(data.spdLoad));
					spdOverridePoint.push(Number(data.spdOverride));
					
					if(data.status=="IN-CYCLE"){
						if(Number(data.spdLoad)==0){
							console.log("연")
							color = "#A3D800"
						}else{		//순절삭
							console.log("찐")
							color = "#009744"
						}
					}else if(data.status=="WAIT"){
						color = "#FF9100";
					}else if(data.status=="ALARM"){
						color = "#EC1C24";
					}else if(data.status=="NO-CONNECTION"){
						color = "#A0A0A0";
					};
					
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				for(var i = 0; i < 719-(json.length+startN); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
					spdLoadPoint.push(Number(-10));
					spdOverridePoint.push(Number(-10));
				};
				
				status = new Highcharts.Chart(options);
				getSpdFeed();
				
				//drawLabelPoint();
			},error : function(e1,e2,e3){
			}
		});
	};
	
	var spdLoadPoint = [];
	var spdOverridePoint = [];
	
	function getSpdFeed(){
		$('#spdFeed').highcharts({
	        chart: {
	            type: 'line',
	            backgroundColor : "rgba(0,0,0,0)",
	            height : getElSize(400),
	        },
	        exporting : false,
	        credits : false,
	        title: {
	            text: null
	        },
	        subtitle: {
	            text: null
	        },
	        legend : {
	        	enabled : false,
	        	itemStyle : {
	        			color : "white",
	        			fontWeight: 'bold'
	        		}
	        },
	        xAxis: {
	            //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	        	labels : {
	        		enabled : false
	        	}
	        },
	        yAxis: {
	        	min : 0,
	        	gridLineWidth: 0,
				minorGridLineWidth: 0,
	            title: {
	                text: null
	            },
	            labels : {
	            	style : {
	            		color : "white"
	            	}
	            }
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {	
	                    enabled: false,
	                    color: 'white',
	                    style: {
	                        textShadow: false 
	                    }
	                },
	                enableMouseTracking: false
	            }
	        },
	        series: [{
	            name: 'Spindle Load',
	            color : 'yellow',
	            //data: spdLoadPoint
	            data : spdLoadPoint,
	            lineWidth : getElSize(10),
	            marker : {
                    enabled : false,
                },
	        }, {
	            name: 'Spindle Override',
	            color : 'red',
	            data: spdOverridePoint,
	            lineWidth : getElSize(10),
	            marker : {
                    enabled : false,
                },
	        }]
	    });	
		
		spdChart = $('#spdFeed').highcharts();
		$("#legend").css("display", "inline");
	};
	
	function drawPrdctChart(ratio){
		$("#target_ratio").html(Math.round(ratio) + "%").css({
			"color" : "#0080FF",
			"font-size" : getElSize(100),
			"right" : getElSize(30),
			"top" : getElSize(850)
		});
		
		$('#prdctChart').highcharts({
		    chart: {
		    	backgroundColor : "rgba(0,0,0,0)",
		    	height : getElSize(230),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50),
		        type: 'bar'
		    },
		    credits : false,
		    exporting : false,
		    title: {
		        text: null
		    },
		    xAxis: {
		    	/* gridLineWidth:0,
		    	lineWidth: 0,*/
		    	tickWidth: 0, 
				labels: {
		       		enabled: false
		    	},    
		    },
		    yAxis: {
			    gridLineWidth: 0,
				minorGridLineWidth: 0,
		        min: 0,
		        max :100,
				labels : {
					style : {
						color : "#7A7B7C",
						fontSize :getElSize(30)
					}
		        },
		        title: {
		            text: null
		        }
		    },
		    tooltip : {
				enabled : false
			},
		    legend: {
		        reversed: true,
		        enabled : false
		    },
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            groupPadding: 0,
		            pointPadding: 0,
		            borderWidth: 0
		        }
		    },
		    /* series: [{
		    		color : "#BBBDBF",
		        name: 'target',
		        data: [10]
		    },{
		    		color : "#A3D800",
		        name: 'goal',
		        data: [10]
		    }] */
		    series: [{
	    		color : "#A3D800",
	        name: 'goal',
	        data: [Math.round(ratio)]
	    }]
		});			
	};
	
	function drawPieChart(){
		/* incycleColor=10
		cuttingColor=7
		waitColor=5
		alarmColor=2
		noconnColor=1 */

		// Build the chart
	    $('#pieChart').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            width : getElSize(350),
	            height : getElSize(350),
	            plotBorderWidth: null,
	            backgroundColor : "rgba(255,255,255,0)",
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: null
	        },
	        tooltip: {
				enabled :false,
	        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        legend : {
	        	enabled : false
	        },
	        exporting : false,
	        credits : false,
	        plotOptions: {
	            pie: {
	            	size: getElSize(350),
	            	//borderWidth: 0,
	                //allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'In-Cycle',
	                y: 0,
	                color : "#A3D800"
	            },{
	                name: 'Cut',
	                y: 0,
	                color : "#009744"
	            }, {
	                name: 'Wait',
	                y: 0,
	                color : "#FF9100"
	            }, {
	                name: 'Alarm',
	                y: 0,
	                color : "#EC1C24"
	            }, {
	                name: 'Off',
	                y: 0,
	                color : "#A0A0A0"
	            }]
	        }]
	    });
		  
	    pieChart = $('#pieChart').highcharts()
	    
	    $('#pieChart').css({
	    	"margin-top" : "18.82%",
        	"margin-left" : "0.28%",
   			"z-index" : 1,
		//	"opacity" : 0.8
	    })
	    

	};
	
	function getDetailData(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		
		var today = year + "-" + month + "-" + date_;
		
		var url = ctxPath + "/chart/getDetailBlockData.do";
		var param = "dvcId=" + dvcId + 
					"&sDate=" +  $("#sDate").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var alarm = "";
				$(json).each(function(idx, data){
					var hour = new Date().getHours()-8;
					if(hour==0) hour=1;
					 
					var sign = "";
					
					$("#cylCnt").html(Math.round(data.lastFnPrdctNum/data.prdctPerCyl));
					$("#prdctPerCyl").html(data.prdctPerCyl);
					$("#daily_target_cycle").html(data.tgCnt);
					$("#complete_cycle").html(data.lastFnPrdctNum);
					$("#daily_avg_cycle_time").html(Number(data.LastAvrCycleTime/60).toFixed(1));
					$("#daily_length").html(Number(data.prdctPerHour).toFixed(1));
					$("#feedOverride").html(Number(data.feedOverride));
					$("#spdLoad").html(Number(data.spdLoad));
					
					//var remainCnt = data.remainCnt;
					var remainCnt = data.tgCnt - (data.lastFnPrdctNum);
					var color = "";
					if(Number(remainCnt)>0){
						sign = "-";
						color = "red"
					}else{
						sign = "+";
						remainCnt = Math.abs(remainCnt);
						color = "blue";
					};
					
					$("#downValue").html(sign + remainCnt).css({
						"font-size" : getElSize(70),
						"color" : color
					})
					
					
					//일 생산 현황
					if(isNaN(data.lastFnPrdctNum / data.tgCnt * 100)){
						drawPrdctChart(0);
					}else if((data.lastFnPrdctNum / data.tgCnt * 100)==Infinity){
						drawPrdctChart(0);
					}else{

						drawPrdctChart(data.lastFnPrdctNum / data.tgCnt * 100);
					}
					//data.lastAlarmCode + " - " + data.lastAlarmMsg;
					var alarm1, alarm2, alarm3;
					if(data.ncAlarmNum1==""){
						alarm1 = "";
					}else{
						alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
					};
					if(data.ncAlarmNum2==""){
						alarm2 = "";
					}else{
						alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
					};
					if(data.ncAlarmNum3==""){
						alarm3 = "";
					}else{
						alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
					};
					
					
					alarm += alarm1 +
							alarm2 +
							alarm3; 
							
					if(data.status=="ALARM" && alarm == ""){
						//console.log("get")
						alarm = "[장비 확인 필요]";
					}
					
					$("#alarm").html(alarm);	
					
					var n = (data.lastFnPrdctNum)/data.tgCnt*100;
					//barChart.series[0].data[0].update(Number(Number(n).toFixed(1)));
				});
				
				
				$("#downValue").css({
					"position" : "absolute",
					"top" : $("#main_table td:nth(9)").offset().top - getElSize(100)
				});

				$("#downValue").css({
					"right" : getElSize(2530),
				});
				
				//setTimeout(getDetailData, 5000)
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(35),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$(".content").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
			"color" : "white",
			"text-align" : "center",
			"font-size" : getElSize(130),
		});
		
		$("#prdctChart_td").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
		});
		
		$("#timeChart_td").css({
			"height" : getElSize(700),
			"background-color" : "#191919"
		});
	
		$("#timeChart").css({
			"margin-top" : getElSize(50)	
		});
		
		$("#dvcId").css({
			"position" : "absolute",
			"font-size" : getElSize(70),
			"margin-top" : getElSize(25),
			"margin-left" : getElSize(30)
		});
		
		$("#statusChart").css({
			"width" : getElSize(750),
			"margin-top" : getElSize(150),
			"z-index" : 2,
		});
		
		$("#legend").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"margin-left" : getElSize(1600),
			"display" : "none"
		});
		
		$("#spdLoadLine").css({
			"background-color" : "#F9FF00",			
			"width" : getElSize(90),
			"height" : getElSize(20)
		});
		
		$("#spdOvrrdLine").css({
			"background-color" : "#FF0000",
			"width" : getElSize(90),
			"height" : getElSize(20),
			"margin-left" : getElSize(50)
		});
		
		$("#inCycleTime, #waitTime, #alarmTime, #noConnTime, #cuttingTime").css({
			"color" : "black",
			"font-size" : getElSize(35)
		});
		
		//1043
		$("#inCycleTime").css({
			"top" : getElSize(1010),
			"left" : getElSize(960)
		}); 
	
		$("#cuttingTime").css({
			"top" : getElSize(1070),
			"left" : getElSize(960)
		}); 

		$("#waitTime").css({
			"top" : getElSize(1130),
			"left" : getElSize(960)
		});
		
		$("#alarmTime").css({
			"top" : getElSize(1190),
			"left" : getElSize(960)
		});
		
		$("#noConnTime").css({
			"top" :  getElSize(1250),
			"left" : getElSize(960)
		});
		
		$("#inCycleSpan, #waitSpan, #alarmSpan, #noConnSpan, #CutSpan").css({
			"color" : "black",
			"font-size" : getElSize(47),
			"position" : "absolute"
		});
		
		$("#inCycleSpan").css({
			"top" :  getElSize(1005),
			"left" : getElSize(1110)
		});

		$("#CutSpan").css({
			"top" :  getElSize(1065),
			"left" : getElSize(1110)
		});
		
		$("#waitSpan").css({
			"top" : getElSize(1125),
/* 			"top" : $("#main_table td:nth(9)").offset().top + getElSize(845), */
			"left" : getElSize(1110)
		});
		
		$("#alarmSpan").css({
			"top" : getElSize(1185),
/* 			"top" : $("#main_table td:nth(9)").offset().top + getElSize(910), */
			"left" : getElSize(1110)
		});
		
		$("#noConnSpan").css({
			"top" : getElSize(1245),
/* 			"top" : $("#main_table td:nth(9)").offset().top + getElSize(985), */
			"left" : getElSize(1110)
		});
		
		$("#opratio_gauge").css({
			"margin-left" : getElSize(100),
			"margin-top" : getElSize(650)
		});
		
		$("#cutting_gauge").css({
			"margin-right" : getElSize(100),
			"margin-top" : getElSize(650)
		});
		$("img").css({
			"display" : "inline"
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#datePicker").css({
			"position" : "absolute",
			"top" : getElSize(155),
			"left" : getElSize(1100)
		})
		
		$("#sDate").css({
			"width" : getElSize(300),
			"font-size" : getElSize(50)+"px"
		})
		$(".imgText").css({
			"z-index" : 10
		}); 
		$("#pieIndex").css({
			"z-index" : 5
		}); 
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	
	$( function() {
	    $( ".date" ).datepicker({
	    	maxDate: 0,
		    onSelect : function(e){
		    	console.log($(".date").val())
		    	//e == 날짜 ? 1 , ''
		    	if(!moment().isAfter(e)){
		    		alert("오늘 이후의 날자는 선택 하실 수 없습니다.")
		    	}else{
		    		$(".date").val(e);
					getDvcData();
					getDetailData();
					getCurrentDvcStatus(dvcId);
		    	}
		    }
	    })
   });
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td style="display:inline">
					<select id="dvcId"></select>
					<div id="datePicker"><label for='sDate'><spring:message code="date_"></spring:message> : </label><input type='text' readonly="readonly" id='sDate' class="date"></div>
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table  style="width: 100%" id="main_table">
						<tr>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="dailyprdcttarget"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="dailyprdctcnt"></spring:message>	
								<span id="downValue"></span>
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap"; class="title_span";t>
								<spring:message code="prdct_per_cycle"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_cycle_cnt"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_avrg_cycle_time"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_cnt_per_hour"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								Feed Override	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								Spindle Load	
							</td>
						</tr>
						<tr>
							<td class='content' id="daily_target_cycle"></td>
							<td class='content' id="complete_cycle"></td>
							<td class='content' id="prdctPerCyl"></td>
							<td class='content' id="cylCnt"></td>
							<td class='content' id="daily_avg_cycle_time" style="white-space: nowrap;"></td>
							<td class='content' id="daily_length"></td>
							<td class='content' id="feedOverride"></td>
							<td class='content' id="spdLoad"></td>
						</tr>
						<tr>
							<TD class="title_span" colspan="2">
								<spring:message code="dailystackedstatus"></spring:message>	
							</TD>
							<TD class="title_span" colspan="6" >
								<spring:message code="prdct_status"></spring:message>	
							</TD>
						</tr>
						<Tr>
							<div id="pieChart" style="position: absolute;"></div>
							<td rowspan="3" colspan="2" style="text-align: center; vertical-align: top;"  >

								<span id="inCycleTime" class='imgText'>In-Cycle</span> <span id="inCycleSpan" class='imgText'>0</span>
								<span id=cuttingTime class='imgText'>Cut</span> <span id="CutSpan" class='imgText'>0</span>
								<span id="waitTime" class='imgText'>Wait</span> <span id="waitSpan" class='imgText'>0</span>
								<span id="alarmTime" class='imgText'>Alarm</span> <span id="alarmSpan" class='imgText'>0</span>
								<span id="noConnTime" class='imgText'>Off</span> <span id="noConnSpan" class='imgText'>0</span>
								
								<div style="position: absolute;" id="pieIndex">
									<img alt="" src="${ctxPath }/images/statusChart_copy.png" id="statusChart" style="float: left;">
								</div>
								
								<div id="opratio_gauge" style="float: left;"></div>
								<div id="cutting_gauge" style="float: right;"></div>
								<span id="opratio_span"></span>
								<span id="cuttingratio_span"></span>
								
							</td>
							<td colspan="6" style="text-align: center; vertical-align: bottom;" id="prdctChart_td">
								<span id="target_ratio" ></span>
								<div id="prdctChart" ></div>
							</td>
						</Tr>
						<tr>
							<TD class="title_span" colspan="6">
								<spring:message code="operation_status"></spring:message>	
							</TD>
						</tr>
						<tr>
							<td colspan="6" style="text-align: center; vertical-align: top;" id="timeChart_td">
								<div id="timeChart"></div>
								<div id="spdFeed"></div>
								<table id="legend">
									<Tr>
										<td><div id="spdLoadLine"></div></td>
										<Td>Spindle Load</Td>
										<td ><div id="spdOvrrdLine"></div></td>
										<Td>Spindle Override</Td>
									</Tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_blue.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	