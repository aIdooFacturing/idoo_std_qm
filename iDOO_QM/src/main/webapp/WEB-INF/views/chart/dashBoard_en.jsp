<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highchart_en.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller_en.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<style>
/* .chartTitle{
	position : absolute;
	top:-100;
	left:20%;
	right:0;
	margin:auto;
	font-size : 50;
} */

#chart{
	overflow: hidden;
}

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}

#title_left{
	left : 50px;
	width: 300px;
}


#pieChart1{
	position: absolute;
	left : 800px;
	z-index: 99;
	top: 1480px;
	height: 700px;
	width: 750px;
}

#pieChart2{
	position: absolute;
	left : 300px;
	z-index: 99;
	top: 1480px; 
	height: 700px;
	width: 750px;
}

#tableDiv{
	left: 20px;
	width: 600px; 
	position: absolute; 
	z-index: 999;
	top: 450px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}

#go123{
	display:none;
	position: absolute;
	z-index: 999;
	width: 600;
	bottom : 50px;
	right: 0px;
}

#time{
	position: absolute;
	top: 170px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#logo{
	position: absolute;
	top: 170px;
	right: 60px;	
}
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 
.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style> 
<script type="text/javascript">
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			closePanel();
			panel = false;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else if(type=="menu2"){
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		}else if(type=="menu3"){
			url = "${ctxPath}/chart/DIMM.do";
			location.href = url;
		};
	};
	
	$(function(){
		/* setTimeout(function(){
			location.href = ctxPath + "/chart/main2_slide.do";
		},1000*60*1); */
		
		$(".menu").click(goReport);
		//getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		document.oncontextmenu = function() {stopLoop()}; 
		setDivPos();
		setInterval(time, 1000);
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
		
		$("#title_right").click(function(){
			location.href = "${ctxPath}/chart/main2.do";
		});
		
		$("#title_left").click(function(){
			location.href = "${ctxPath}/chart/main4.do";
		});
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		getNightlyMachineStatus();
		//startPageLoop();
		
		$("#pieChart2").css({
			"left" : $("#container").offset().left + getElSize(300),
			"top": $("#container").offset().top + contentHeight/(targetHeight/1480),
			"height": contentHeight/(targetHeight/700),
			"width": getElSize(750)
		});
		
		$(".menu").click(goReport);
		$("#menu0").addClass("selected_menu");
		$("#menu0").removeClass("unSelected_menu");
	});
	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			location.href = ctxPath + "/chart/multiVision.do";
		},1000*60*10);
	};
	
	function getNightlyMachineStatus(){
		var url = "${ctxPath}/device/getNightlyMachineStatus.do";
		var date =  new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var targetDate = year + "-" + month + "-" + day;
		var param = "targetDate=" + targetDate;
		
		$.ajax({
			url : url,
			data: param,
			dataType : "text",
			type : "post",
			success : function(data){
				var json = $.parseJSON(data);
				
				var tr = "";
				var status = "";
				var fontSize = getElSize(25);
				$(json).each(function (i, data){
					if(data.status=="WAIT"){
						status = "Completion";
					}else{
						status = "Stop";
					};
					tr += "<tr>" +
							"<td  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + (i+1)  + "</td>" +
							"<td  style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + status + "</td>" +
						"</tr>";
				});
				
					tr = "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
							"<td  style='border:1px solid white; font-Size:" + fontSize + "px;'>1</td>" +
							"<td  style='border:1px solid white;font-Size:" + fontSize + "px;'>HFMS#3</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>01-20</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>00:37:50</td>" + 
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>Stop</td>" + 
						"</tr>" + 
						"<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
						"<td  style='border:1px solid white; font-Size:" + fontSize + "px;'>2</td>" +
						"<td  style='border:1px solid white;font-Size:" + fontSize + "px;'>5FPM#2</td>" +
						"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>01-20</td>" +
						"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>10:37:50</td>" + 
						"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>Running</td>" + 
					"</tr>";

				$("#table").append(tr);
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
			"margin-top" : height/2 - ($("#container").height()/2)
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		
		$("#Legend").css({
			"bottom" : 50,
			"left" : width/2 - $("#Legend").width()/2	
		});
		
		$("#title_main").css({
			"font-size" : getElSize(100),
			"top" : $("#container").offset().top + (getElSize(50))
		});

		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"font-size" : getElSize(45),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_left").css({
			"left" : $("#container").offset().left + getElSize(50)
		});
		
		$("#title_right").css({
			"right" : $("#container").offset().left + getElSize(50)
		});
		
		$("#tableDiv").css({
			"left" : $("#container").offset().left + getElSize(20),
			"width" : getElSize(600),
			"top" : $("#container").offset().top + getElSize(450)
		});
		
		$("#table").css("margin-top", contentHeight/(targetHeight/50));
		$("#tableTitle").css("font-size", getElSize(40));
		$(".tr").css("font-size", getElSize(30));
		$(".td").css("padding", getElSize(10));
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("#go123").css({ 
			"width": getElSize(600),
			"top" : $("#container").offset().top + contentHeight/(targetHeight/1650),
			"right": $("#container").offset().left
		});
		
		$("#Legend").css({
			"font-size" : getElSize(50),
			"bottom" : $("#container").offset().top + getElSize(50),
			"left" : originWidth/2 - ($("#Legend").width()/2)
		});
		
		$("#Legend").css("left" , originWidth/2 - ($("#Legend").width()/2));
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : 0,
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : -1,
			"background-color" : "black",
			"opacity" : 0.4
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
	};
	
	var border_interval = null;
	function bodyNeonEffect(color) {
		clearInterval(border_interval);
		var lineWidth = getElSize(100);
		var toggle = true;
		border_interval = setInterval(function() {
			$("#container").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= getElSize(3);
				if (lineWidth <= 0) {
					toggle = false;
				};
			} else if (!toggle) {
				lineWidth += getElSize(3);
				if (lineWidth >= getElSize(100)) {
					toggle = true;
				};
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
</script>
</head>
<body oncontextmenu="return false">
	<div id="corver"></div>	
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">Shop Layout</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">Performance Analysis</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">Alarm History</td>
				</tr>
				<tr>
					<td id="menu3" class="menu">Nighttime Machine Status</td>
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<div id="Legend" style="position: absolute; font-size: 50px;">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> In-Cycle 
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> Wait 
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> Alarm 
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> Power Off  
	</div>

	<!-- <div id="pieChart1"  ></div> -->
	<div id="pieChart2" ></div>
	
	<img src="${ctxPath }/images/DashBoard/go123.gif" id="go123">

	<div id="tableDiv" >
		<center><font style="font-size: 40px;color: white;font-weight: bold;" id="tableTitle">Stopped Machine<br>On Night Time</font></center>
		<table style="width: 100%; text-align: center; color: white;" id="table">
			<tr style="font-size: 30px; font-weight: bold;" class='tr' >
				<td class='td'>No</td>
				<td class='td'>Name</td>
				<td class='td'>Date</td>
				<td class='td'>Stop Time</td>
				<td class='td'>Status</td>
			</tr>
		</table>
	</div>

	<%-- <img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title"> --%>
	<div id="title_main" style="color: white; font-weight: bolder;" class="title">Shop Layout</div>
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>	
	<div id="title_right" style="color: white;" class="title">Doosan Infracore<br>Machine Tools</div>
	
	<font id="date"></font>
	<font id="time"></font>
</body>
</html>