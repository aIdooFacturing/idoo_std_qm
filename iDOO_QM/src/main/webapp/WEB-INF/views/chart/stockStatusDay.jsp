<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
		<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0 response.setDateHeader("expires", -1); // Stop proxy caching %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
	<link rel="stylesheet" href="${ctxPath }/css/style.css">

	<title>Dash Board</title>
	<script type="text/javascript">
		var ctxPath = "${ctxPath}";
		var fromDashboard = "${fromDashBoard}";
		var targetWidth = 3840;
		var targetHeight = 2160;

		var originWidth = window.innerWidth;
		var originHeight = window.innerHeight;

		var contentWidth = originWidth;
		var contentHeight = targetHeight / (targetWidth / originWidth);

		var screen_ratio = getElSize(240);

		if (originHeight / screen_ratio < 9) {
			contentWidth = targetWidth / (targetHeight / originHeight)
			contentHeight = originHeight;
		};

		function getElSize(n) {
			return contentWidth / (targetWidth / n);
		};

		function setElSize(n) {
			return Math.floor(targetWidth / (contentWidth / n));
		};

		var marginWidth = (originWidth - contentWidth) / 2;
		var marginHeight = (originHeight - contentHeight) / 2;
	</script>
	<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
	<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
	<script src="http://kendo.cdn.telerik.com/2018.1.221/js/jszip.min.js"></script>
	
	<style>
		* {
			margin: 0px;
			padding: 0px;
		}

		body {
			margin: 0px;
			padding: 0px;
			width: 100%;
			height: 100%;
			overflow: hidden;
			background-color: black;
			font-family: 'Helvetica';
		}

		#wrapper thead {
			font-size: 12;
		}

		#wrapper tbody {
			font-size: 12;
			font-weight : bolder;
			background-color: white;
		}

		.k-grid-header th.k-header {
			background: linear-gradient( to top, black, gray);
			color: white;
		}

		.k-grouping-row td {
			background: linear-gradient( to top, #5D5D5D, #8C8C8C);
			color: white;
		}

		#wrapper tbody tr td.k-group-cell {
			background: #8C8C8C;
		}

		.k-header.k-grid-toolbar {
			background: linear-gradient( to top, black, gray);
			color: white;
		}

		a.k-button.k-button-icontext.k-grid-cancel-changes {
			background: linear-gradient( to top, red, #ff6363, #ff6300);
			transition-duration: 0.5s;
		}

		a.k-button.k-button-icontext.k-grid-cancel-changes:hover {
			background: red;
			color: white;
			transition-duration: 0.5s;
		}

		a.k-button.k-button-icontext.k-grid-finish {
			float: right;
			background: greenyellow;
			transition-duration: 0.5s;
		}

		.k-button.k-button-icontext.k-grid-print {
			float: right;
			background: greenyellow;
			transition-duration: 0.5s;
		}

		a.k-button.k-button-icontext.k-grid-finish:hover {
			background: #486b13;
			transition-duration: 0.5s;
			color: white; 
		}

		.k-button.k-button-icontext.k-grid-print:hover {
			background: #486b13;
			transition-duration: 0.5s;
			color: white; 
		}
		
		th, td {
		    border: 1px solid #444444;
		  }
		.k-icon.k-i-collapse{
			display:none;
		} 
		#wrapper tbody tr.k-grouping-row td{
			font-size:0px;
			padding :5px;
		}
		
		@font-face {
			font-family: "Arial Unicode";
			unicode-range: utf-8;
		}
		.k-widget {
		    font-family: 'Arial Unicode MS';
		}
		
		#printT tr th{
			font-size:  6px;
		}
		#printT tr td{
			font-size:  6px;
		}



	</style>
	<script type="text/javascript">
		function replaceHash(str) {
			return str.replace(/#/gi, "-");
		};

		function replaceHyphen(str) {
			return str.replace(/#/gi, "-");
		};

		var today = "";
		function setDate() {
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth() + 1));
			var day = addZero(String(date.getDate()));
			
			var minusDay=0;
			if(date.getHours()<=8 && date.getMinutes()<30){
				console.log("이전")
				minusDay=2
			}else{
				console.log("이후")
				minusDay=1
			}
			
			var Yday=new Date(date.setDate(date.getDate()-minusDay));
			year = Yday.getFullYear();
			month = addZero(String(Yday.getMonth() +1));
			day = addZero(String(Yday.getDate()));
			
			today = year + "-" + month + "-" + day;
			$(".date").val(year + "-" + month + "-" + day);

		};


		var handle = 0;

		$(function () {
			getPrdNo();
			createNav("inven_nav", 9);

			setEl();
			setDate();
			time();

			setEvt();

			$("#home").click(function () { location.href = "${ctxPath}/chart/index.do" });

			window.setInterval(function () {
				var width = window.innerWidth;
				var height = window.innerHeight;

				if (width != originWidth || height != originHeight) {
					location.reload();
				};
			}, 1000 * 10);


			chkBanner();
		});

		function setEvt() {

		};


		function getTime() {
			var date = new Date();
			var hour = addZero(String(date.getHours()));
			var minute = addZero(String(date.getMinutes()));
			var second = addZero(String(date.getSeconds()));

			return hour + ":" + minute;
		};

		function addZero(n) {
			if (n.length == "1") {
				n = "0" + n;
			};
			return n;
		};

		function setEl() {
			var neonColor = "#0096FF";

			var width = window.innerWidth;
			var height = window.innerHeight;

			$(".right").css({
				"height": getElSize(120)
			});

			$(".left, .menu_left").css({
				"width": getElSize(495)
			})

			$("#container").css({
				"width": contentWidth,
				"height": contentHeight,
			});

			$("#container").css({
				"margin-left": (originWidth / 2) - ($("#container").width() / 2),
				"margin-top": (originHeight / 2) - ($("#container").height() / 2)
			})

			$("#intro").css({
				"position": "absolute",
				"bottom": 0 + marginHeight,
				"font-size": getElSize(140),
				"font-weight": "bolder",
				"z-index": 9999
			});

			$("#intro_back").css({
				"width": originWidth,
				"display": "none",
				"height": getElSize(180),
				"opacity": 0.5,
				"position": "absolute",
				"background-color": "black",
				"bottom": 0 + marginHeight,
				"z-index": 9999
			})

			$("#time").css({
				"color": "white",
				"position": "absolute",
				"font-size": getElSize(40),
				"top": getElSize(25) + marginHeight,
				"right": getElSize(30) + marginWidth
			});

			$("#table").css({
				"position": "absolute",
				"width": $("#container").width(),
				"top": getElSize(100) + marginHeight
			});

			$(".right").css({
				"width": contentWidth - $(".left").width()
			});

			$(".menu_right").css({
				"width": $(".right").width()
			})

			$("#home").css({
				"cursor": "pointer"
			})

			$("#title_right").css({
				"position": "absolute",
				"z-index": 2,
				"color": "white",
				"font-size": getElSize(40),
				"top": getElSize(130) + marginHeight,
				"right": getElSize(30) + marginWidth
			});

			$("span").css({
				"color": "#8D8D8D",
				"position": "absolute",
				"font-size": getElSize(45),
				"margin-top": getElSize(20),
				"margin-left": getElSize(20)
			});

			$("#selected").css({
				"color": "white",
			});

			$("span").parent("td").css({
				"cursor": "pointer"
			});

			$(".title_span").css({
				"color": "white",
				"font-size": getElSize(40),
				"background-color": "#353535",
				"padding": getElSize(15)
			});


			$("select, button, input").css({
				"font-size": getElSize(40),
				"margin-left": getElSize(20),
				"margin-right": getElSize(20)
			});

			$("button").css({
				"padding": getElSize(15),
			})

			$("#search").css({
				"cursor": "pointer",
				"width": getElSize(80),
			});

			$("#content_table td").css({
				"color": "##BFBFBF",
				"font-size": getElSize(50)
			});

			$(".tmpTable, .tmpTable tr, .tmpTable td").css({
				"border": getElSize(5) + "px solid rgb(50,50,50)"
			});

			$(".tmpTable td").css({
				"padding": getElSize(10),
				"height": getElSize(100)
			});

			$(".contentTr").css({
				"font-size": getElSize(60)
			});

			$("#delDiv").css({
				"position": "absolute",
				"width": getElSize(700),
				"height": getElSize(200),
				"background-color": "#444444",
				"color": "white"
			});

			$("#delDiv").css({
				"top": (window.innerHeight / 2) - ($("#delDiv").height() / 2),
				"left": (window.innerWidth / 2) - ($("#delDiv").width() / 2),
				"display": "none",
				"border-radius": getElSize(10),
				"padding": getElSize(20)
			});

			$("#intro").css({
				"font-size": getElSize(100)
			});

			$("#wrapper").css({
				"width": getElSize(3350)
			})
		};

		function time() {
			$("#time").html(getToday());
			handle = requestAnimationFrame(time)
		};

		function getPrdNo() {
			var url = "${ctxPath}/common/getPrdNo.do";
			var param = "shopId=" + shopId;

			$.ajax({
				url: url,
				data: param,
				type: "post",
				dataType: "json",
				success: function (data) {
					json = data.dataList;

					var option = "<option value='ALL'>${total}</option>";

					$(json).each(function (idx, data) {
						option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>";
					});

					$("#group").html(option);

					getStockStatus();
				}
			});
		};

		var className = "";
		var classFlag = true;

		var kendotable;
		var confirm;
		var nowDateInsert;
		
		function LineCnt(){
			console.log(kendotable);
			console.log($("#wrapper").data("kendoGrid"))
			console.log($("wrapper"))
			return "메"
		}
		
		
		function dropdownlist(container, options){
			$('<input required name="' + options.field + '" />')
			 .appendTo(container)
			 .kendoDropDownList({
				 autoWidth: true,
				 autoBind: false,
				 dataTextField: "name",
				 height: 3300,
				 dataValueField: "id",
				 dataSource: companylist
			});
			console.log(container);
			console.log(options);
		}
		
		function printGrid(){
			var diagram = $("#wrapper").getKendoDiagram();
            diagram.exportImage().done(function(data) {
                kendo.saveAs({
                    dataURI: data,
                    fileName: "diagram.png",
                    proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
                });
            });
			
			
			/* var gridElement = $('#wrapper'),
            printableContent = '',
            win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
            doc = win.document.open();

	        var htmlStart =
	                '<!DOCTYPE html>' +
	                '<html>' +
	                '<head>' +
	                '<meta charset="utf-8" />' +
	                '<title>Kendo UI Grid</title>' +
	                '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
	                '<style>' +
	                'html { font: 11pt sans-serif; }' +
	                '.k-grid { border-top-width: 0; }' +
	                '.k-grid, .k-grid-content { height: auto !important; }' +
	                '.k-grid-content { overflow: visible !important; }' +
	                'div.k-grid table { table-layout: auto; width: 100% !important; }' +
	                '.k-grid .k-grid-header th { border-top: 1px solid; }' +
	                '.k-grouping-header, .k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
	                // '.k-grid-pager { display: none; }' + // optional: hide the whole pager
	                '</style>' +
	                '</head>' +
	                '<body>';
	
	        var htmlEnd =
	                '</body>' +
	                '</html>';
	
	        var gridHeader = gridElement.children('.k-grid-header');
	        if (gridHeader[0]) {
	            var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
	            printableContent = gridElement
	                .clone()
	                    .children('.k-grid-header').remove()
	                .end()
	                    .children('.k-grid-content')
	                        .find('table')
	                            .first()
	                                .children('tbody').before(thead)
	                            .end()
	                        .end()
	                    .end()
	                .end()[0].outerHTML;
	        } else {
	            printableContent = gridElement.clone()[0].outerHTML;
	        }
	
	        doc.write(htmlStart + printableContent + htmlEnd);
	        doc.close();
	        win.print(); */
		}
		
		var currentValue;
		$(document).ready(function () {
			 
			confirm = $("#confirm").kendoDialog({
				visible: false,
				title: false,
				closable: false,
				actions: [{
					text: "네",
					action: function (e) {
						$.showLoading();
						var kendolist = kendotable.dataSource.data();
						console.log("=====저장할 리스트 목록=====")
						console.log(kendolist)
						/* $(kendolist).each(function (idx, data) {
							kendolist[idx].chk = 1
							kendolist[idx].ohdCnt = kendolist[idx].iniohdCnt+kendolist[idx].rcvCnt-kendolist[idx].notiCnt-kendolist[idx].issCnt
							kendolist[idx].ohdCntR =  kendolist[idx].iniohdCntR + kendolist[idx].rcvCntR
							kendolist[idx].ohdCntM = kendolist[idx].iniohdCntM+kendolist[idx].rcvCntM-kendolist[idx].notiCntM-kendolist[idx].issCntM
							kendolist[idx].ohdCntC = kendolist[idx].iniohdCntC+kendolist[idx].rcvCntC-kendolist[idx].notiCntC-kendolist[idx].issCntC
							kendolist[idx].ohdCntP = kendolist[idx].iniohdCntP+kendolist[idx].rcvCntP-kendolist[idx].notiCntP-kendolist[idx].issCntP
						}) */
						
						var url = "${ctxPath}/chart/finishStckMaster.do";
						var obj = new Object();
						obj.val = kendolist;

						var param = "val=" + JSON.stringify(obj)
							+ "&date=" + $(".hasDatepicker").val();
						$.ajax({
							url: url,
							data: param,
							type: "post",
							success: function (data) {
								$.hideLoading();
								getStockStatus();
							},error:function(error){
								$.hideLoading();
							}
						})
						
						return true;
					},
					primary: true
				}, {
					text: "아니오"
				}]
			}).data("kendoDialog")

			var nowDate;
			var minusDate;
			if(moment().format("HH:mm")<"08:30"){
				nowDate=moment().subtract(1, 'day').format("YYYY-MM-DD")
				minusDate=moment().subtract(2, 'day').format("YYYY-MM-DD")
			}else{
				nowDate=moment().format("YYYY-MM-DD")
				minusDate=moment().subtract(1, 'day').format("YYYY-MM-DD")
			}
			
			kendotable = $("#wrapper").kendoGrid({
				dataSource: {
				},
				height: getElSize(1745),
				editable: true,
				groupable: false,
				/* dataBound:function(){
			          grid = this;
			            grid.tbody.find('tr').each(function(){            
			             var item = grid.dataItem(this);
			             kendo.bind(this,item);
			         })
			    }, */
			    dataBound:function(){
					/* kendo.pdf.defineFont({
					    "Verdana"             : "/fonts/Verdana.ttf", // this is a URL
					    "Verdana|Bold"        : "/fonts/Verdana_Bold.ttf",
					    "Verdana|Bold|Italic" : "/fonts/Verdana_Bold_Italic.ttf",
					    "Verdana|Italic"      : "/fonts/Verdana_Italic.ttf"
					}); */
					
			    },
				columns: [{
					field: "prdNo",
					title: "차종",
					encoded: true,
					locked: true,
					lockable: false,
					groupHeaderTemplate: ' ',
					width: getElSize(410)
				}, {
					field: "item",
					title: "부품명",
					locked: true,
					lockable: false,
					width: getElSize(390)
				}, {
					title: "소재",
					columns: [{
						field: "iniohdCnt"
						, title: "전일 재고"
						, width: getElSize(170)
					}, {
						field: "rcvCnt"
						, title: "입고"
						, width: getElSize(170)
//						, template : '<input type="text" class="k-input k-textbox" name="rcvCnt" value="#=rcvCnt#" data-bind="value:rcvCnt">'
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						field: "notiCnt"
						, title: "불량"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "누적입고",
						width: getElSize(170)
					}, {
						field: "issCnt"
						, title: "출고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						field: "ohdCnt"
						, template: "<div class='#=ohdCnt#'>#=iniohdCnt+rcvCnt-notiCnt-issCnt#"
						, title: "창고 재고"
						, width: getElSize(170)
						, editor: readOnly
					}]
				}, {
					width: getElSize(137)
					, attributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}, headerAttributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}
				}, {
					title: "라인소재재고",
					columns: [{
						field: "iniohdCntL"
						, title: "전일재고"
						, width: getElSize(170)
						, editor: readOnly
					}, {
						field: "rcvCntL"
						, title: "입고"
						, width: getElSize(170)
						, editor: readOnly
					}, {
						field: "ohdCntL"
//						,template:  "#=Number(iniohdCntL)+Number(rcvCntL)-Number(rcvCntM)#"
						, title: '현재고'
						, width: getElSize(170)
						, editor: readOnly
					}]
				}, {
					width: getElSize(137)
					, attributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}, headerAttributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}
				},{
					title: "R삭",
					columns: [{
						field: "iniohdCntR"
						, title: "전일재고"
						, width: getElSize(170)
					}, {
						field: "rcvCntR"
						, title: "입고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "누적<br>생산수량"
						, width: getElSize(170)
					}, {
						field: "issCntR"
						, title: "출고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						field: "notiCntR"
						, title: "불량"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "불량 누계"
						, width: getElSize(170)
					}, {
						field: "ohdCntR",
						template: "#=Number(iniohdCntR)+Number(rcvCntR)-Number(issCntR)-Number(notiCntR)#"
						, title: "재고"
						, width: getElSize(170)
						, editor: readOnly
					}]
				}, {
					width: getElSize(137)
					, attributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}, headerAttributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}
				},{
					title: "1차 가공(MCT)",
					columns: [{
						field: "iniohdCntM"
						, title: "전일재고"
						, width: getElSize(170)
					}, {
						field: "rcvCntM"
						, title: "입고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "누적<br>생산수량"
						, width: getElSize(170)
					}, {
						field: "issCntM"
						, title: "출고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						field: "notiCntM"
						, title: "불량"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "불량 누계"
						, width: getElSize(170)
					}, {
						field: "ohdCntM",
						template: "#=Number(iniohdCntM)+Number(rcvCntM)-Number(issCntM)-Number(notiCntM)#"
						, title: "재고"
						, width: getElSize(170)
						, editor: readOnly
					}]
				}, {
					width: getElSize(137)
					, attributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}, headerAttributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}
				}, {
					title: "2차 가공(CNC)",
					columns: [{
						field: "iniohdCntC"
						, title: "전일재고"
						, width: getElSize(170)
					}, {
						field: "rcvCntC"
						, title: "입고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "누적입고"
						, width: getElSize(170)
					}, {
						field: "issCntC"
						, title: "출고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "누적출고"
						, width: getElSize(170)
					}, {
						field: "notiCntC"
						, title: "불량"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "불량 누계"
						, width: getElSize(170)
					}, {
						field: "ohdCntC",
						template: "#=Number(iniohdCntC)+Number(rcvCntC)-Number(issCntC)-Number(notiCntC)#"
						, title: "재고"
						, width: getElSize(170)
						, editor: readOnly
					}]
				}, {
					width: getElSize(137)
					, attributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}, headerAttributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}
				}, {
					title: "완제품",
					columns: [{
						field: "iniohdCntP"
						, title: "전일재고"
						, width: getElSize(170)
					}, {
						field: "rcvCntP"
						, title: "입고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "입고<br>(외주실적)"
						, width: getElSize(170)
						, attributes: {
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						field: "issCntP"
						, title: "출고"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						field: "notiCntP"
						, title: "불량"
						, width: getElSize(170)
						, attributes: {
							class: "editable-cell" ,
							style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
						}
					}, {
						title: "불량 누계"
						, width: getElSize(170)
					}, {
						field: "ohdCntP",
						template: "#=Number(iniohdCntP)+Number(rcvCntP)-Number(issCntP)-Number(notiCntP)#"
						, title: "재고"
						, width: getElSize(170)
						, editor: readOnly
					}]
				}, {
					width: getElSize(137)
					, attributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}, headerAttributes: {
						style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
					}
				}, {
					title: "통합 재고",width:getElSize(500)
					, columns: [{
						title: "총 재고"
						, template: "#=Number(ohdCnt)+Number(ohdCntL)+Number(ohdCntR)+Number(ohdCntM)+Number(ohdCntC)+Number(ohdCntP)#"
						, width: getElSize(170)
						, editor: readOnly
					}, {
						field: "totalday"
						, title: "재고 일수"
						, template: "-"
						, width: getElSize(170)
						, editor: readOnly
					}]
				}],
			//	navigatable: true,
        							
				toolbar: [{ name: "날짜", template: '#=date()#' }, { name: "custom", text: "저장", iconClass: "k-icon k-i-save" },
				{ name: "cancel", text: "취소" }, { name: "finish", text: "마감 처리", iconClass: "k-icon k-i-check" },/*  {name:"pdf"} , */ {name:"print" , text: "인쇄" ,iconClass: "k-icon k-i-pinterest"} /* ,  {name:"excel"} */],
				/* excel : {
					fileName : minusDate+" 재고.xlsx"
				},
				pdf :{
					margin : '0cm'
				}, */
				edit: function (e) {
					var input = e.container.find(".k-input");
					var value = input.val();
					//기존 저장된 값 들고오기
					currentValue = Number(value);
					
//					currentValue = Number(e.model.rcvCnt); 
					//edit 들어가서 키입력시
					//같은 아이디값 복사하기
					
					input.keyup(function (e) {
						
						/* console.log("-----key update-----")
						console.log(e.keyCode)
						if(e.keyCode ==52){
							$(".editable-cell")[1].click()
						}else if(e.keyCode == 13){
							alert("enter")
						}else if(e.keyCode == 50){
						alert("2222")
						} */
						
						value = input.val();

						var kendolist = kendotable.dataSource.data();
						var grid = $("#wrapper").data("kendoGrid");

						var row = $(this).closest("tr");
						var rowIdx = $("tr", grid.tbody).index(row);
						var col = $(this).closest("td");
						dataItem = kendotable.dataItem(row);
						var reset = JSON.stringify(dataItem[col.context.name]);
						var clone = $.extend({}, dataItem);

						if (isNaN(value) == false) {
						} else {
							
							dataItem.set(col.context.name,currentValue)
							alert("숫자만 입력하세요")
							return false;
						}

						
						$(grid.tbody).off("change").on("change", "td", function (e) {
							var row = $(this).closest("tr");
							dataItem = kendotable.dataItem(row);
							var clone = $.extend({}, dataItem);
							var rowIdx = $("tr", grid.tbody).index(row);
							var colIdx = $("td", row).index(this);
							var colName = $('#wrapper').find('th').eq(colIdx).text();
							var returnChk = 0;	// returnChk == 0일때만 경고창발생 (한번하기위함)
							//창고
							
							$(kendolist).each(function (idx, data) {
								if (clone.prdNo == kendolist[idx].prdNo && (col.context.name == "rcvCnt" || col.context.name == "issCnt" || col.context.name == "notiCnt")) {
									kendolist[idx].set(col.context.name, Number(clone.get(col.context.name)));
									console.log("입력한 값 : " + clone.get(col.context.name))
									console.log("비교 값 : " + (clone.get(col.context.name) - currentValue));
									if (col.context.name == "rcvCnt") {
										data.set("ohdCnt", Number(clone.ohdCnt) + Number(clone.get(col.context.name) - currentValue))
									} else if (col.context.name == "issCnt") {
										//자재 출고 수정시 라인소재 입고 수정
										data.set("rcvCntL",Number(clone.get(col.context.name)))
										data.set("ohdCntL", Number(clone.ohdCntL) + Number(clone.get(col.context.name) - currentValue))

										data.set("ohdCnt", Number(clone.ohdCnt) - Number(clone.get(col.context.name) - currentValue))
									} else if (col.context.name == "notiCnt") {
										
										data.set("ohdCnt", Number(clone.ohdCnt) - Number(clone.get(col.context.name) - currentValue))
									}
								}else if (clone.prdNo == kendolist[idx].prdNo && clone.idM == kendolist[idx].idM && (col.context.name == "rcvCntR" || col.context.name == "issCntR" || col.context.name == "notiCntR")) {
									console.log("R Modify");
									
									if (col.context.name == "rcvCntR") {
										if(clone.minOpr=="0010"){
											//R입고 수정시 L출고
											data.set("ohdCntL", Number(clone.ohdCntL) - Number(clone.get(col.context.name) - currentValue))
											data.set("issCntL", Number(clone.get(col.context.name)));
										}else{
											if(returnChk==0){
												console.log("----------------")
												console.log(data)
												console.log(data.rcvCntR)
												data.set("rcvCntR",0);
												console.log(data)
												console.log(data.rcvCntR)
												kendo.alert("R작업이 등록되어있지않습니다.")
												returnChk=1;
											}
											data.set("rcvCntR",0);
										}
										
										if(clone.item == kendolist[idx].item && returnChk!=1){
											data.set("ohdCntR", Number(clone.ohdCntR) + Number(clone.get(col.context.name) - currentValue))
										}
									} else if (col.context.name == "issCntR") {
										
										if(clone.item == kendolist[idx].item){
											console.log(clone.minOpr);
											
											if(clone.minOpr=="0010"){
												//R 출고 수정시 M입고 수정
												data.set("rcvCntM", Number(clone.get(col.context.name)))
												data.set("ohdCntM", Number(clone.ohdCntM) + Number(clone.get(col.context.name) - currentValue))

												data.set("ohdCntR", Number(clone.ohdCntR) - Number(clone.get(col.context.name) - currentValue))
											}else{
												if(returnChk==0){
													kendo.alert("R작업이 등록되어있지않습니다.")
													returnChk=1;
												}
												data.set("issCntR",0);
											}
										}
									} else if (col.context.name == "notiCntR") {
										if(clone.item == kendolist[idx].item){
											console.log("---자재출고시 라인소재 재고구하기")
											console.log(clone.ohdCntM)
											console.log( Number(clone.get(col.context.name)))
											console.log(currentValue)
											if(clone.minOpr=="0010"){
												data.set("ohdCntR", Number(clone.ohdCntR) - Number(clone.get(col.context.name) - currentValue))
											}else{
												if(returnChk==0){
													kendo.alert("R작업이 등록되어있지않습니다.")
													returnChk=1;
												}
												data.set("notiCntR",0);
											}
										}
									}
								}
								else if (clone.prdNo == kendolist[idx].prdNo && clone.idM == kendolist[idx].idM && (col.context.name == "rcvCntM" || col.context.name == "issCntM" || col.context.name == "notiCntM")) {
									console.log("MCT Modify")
									if (col.context.name == "rcvCntM") {
										console.log(clone.minOpr)
										if(clone.minOpr=="0020"){
											//M입고 수정시 L출고
											data.set("ohdCntL", Number(clone.ohdCntL) - Number(clone.get(col.context.name) - currentValue))
										}else if(clone.minOpr=="0"){
											if(returnChk==0){
												kendo.alert("라우팅이 등록되어있지않습니다.")
												returnChk=1;
											}
			
											if(clone.item == kendolist[idx].item){
												data.set("rcvCntM",0);
											}
										}else{
											if(clone.item == kendolist[idx].item && returnChk!=1){
												//M입고 수정시 R출고
												data.set("ohdCntR", Number(clone.ohdCntR) - Number(clone.get(col.context.name) - currentValue))
												data.set("issCntR", Number(clone.get(col.context.name)));
											}
										}
										
										if(clone.item == kendolist[idx].item && returnChk!=1){
											data.set("ohdCntM", Number(clone.ohdCntM) + Number(clone.get(col.context.name) - currentValue))
										}
									} else if (col.context.name == "issCntM") {
										
										
										if(clone.item == kendolist[idx].item){
											//M 출고 수정시 C입고 수정
											data.set("rcvCntC", Number(clone.get(col.context.name)))
											data.set("ohdCntC", Number(clone.ohdCntC) + Number(clone.get(col.context.name) - currentValue))

											console.log("")
											data.set("ohdCntM", Number(clone.ohdCntM) - Number(clone.get(col.context.name) - currentValue))
										}
									} else if (col.context.name == "notiCntM") {
										if(clone.item == kendolist[idx].item){
											console.log("---자재출고시 라인소재 재고구하기")
											console.log(clone.ohdCntM)
											console.log( Number(clone.get(col.context.name)))
											console.log(currentValue)
											
											data.set("ohdCntM", Number(clone.ohdCntM) - Number(clone.get(col.context.name) - currentValue))
										}
									}
								} else if (clone.item == kendolist[idx].item && clone.idC == kendolist[idx].idC && (col.context.name == "rcvCntC" || col.context.name == "issCntC" || col.context.name == "notiCntC")) {
									console.log("CNC Modify")
									if (col.context.name == "rcvCntC") {
										//C 입고 수정시 M출고 수정
										data.set("issCntM", Number(clone.get(col.context.name)));
										data.set("ohdCntM", Number(clone.ohdCntM) - Number(clone.get(col.context.name) - currentValue));

										data.set("ohdCntC", Number(clone.ohdCntC) + Number(clone.get(col.context.name) - currentValue));
										console.log(data.ohdCntC);
									} else if (col.context.name == "issCntC") {
										//C 출고 수정시 P입고 수정
										data.set("rcvCntP", Number(clone.get(col.context.name)))
										data.set("ohdCntP", Number(clone.ohdCntP) + Number(clone.get(col.context.name) - currentValue))
										
										data.set("ohdCntC", Number(clone.ohdCntC) - Number(clone.get(col.context.name) - currentValue))
									} else if (col.context.name == "notiCntC") {
										data.set("ohdCntC", Number(clone.ohdCntC) - Number(clone.get(col.context.name) - currentValue))
									}
								} else if (clone.item == kendolist[idx].item && clone.idP == kendolist[idx].idP && (col.context.name == "rcvCntP" || col.context.name == "issCntP" || col.context.name == "notiCntP")) {
									console.log("Product Modify")
									if (col.context.name == "rcvCntP") {
										console.log("CNC Modify")
										
										//P 입고 수정시 C출고 수정
										data.set("issCntC", Number(clone.get(col.context.name)))
										data.set("ohdCntC", Number(clone.ohdCntC) - Number(clone.get(col.context.name) - currentValue))
										
										data.set("ohdCntP", Number(clone.ohdCntP) + Number(clone.get(col.context.name) - currentValue))
									} else if (col.context.name == "issCntP") {
										data.set("ohdCntP", Number(clone.ohdCntP) - Number(clone.get(col.context.name) - currentValue))
									} else if (col.context.name == "notiCntP") {
										data.set("ohdCntP", Number(clone.ohdCntP) - Number(clone.get(col.context.name) - currentValue))
									}
								} else {
									console.log("속하지 않음.");
								}
							})

							var scroll = $("div.k-grid-content").scrollTop();
							
							$("#wrapper").data("kendoGrid").dataSource.fetch();
							
							$("div.k-grid-content").scrollTop(scroll);
							
							
						});
						
					});
				}


			}).data("kendoGrid");
			
			setDate();
			
			$(".date").datepicker({
				onSelect: function (e) {
					if (e > moment().format("YYYY-MM-DD")) {
						$(".date").val(nowDateInsert);
						alert("오늘 이후의 날짜를 선택할수 없습니다.")

						return false;
					} else {
						//e == 날짜 
						$(".date").val(e);
						getStockStatus();
					}
				}
			})
			
			$(".k-grid-custom").click(function () {
				
				var kendolist = kendotable.dataSource.data();
				console.log("=====저장할 리스트 목록=====")
//				console.log(kendolist)
				console.log("자재창고 출고 : "+kendolist[17].issCnt)
				console.log("라인소재 입고 : "+kendolist[17].rcvCntL)
				console.log("라인소재 출고 : "+kendolist[17].issCntL)
				console.log("R삭 입고 : "+kendolist[17].rcvCntR)
				/* $(kendolist).each(function (idx, data) {
					kendolist[idx].chk = 0
					kendolist[idx].ohdCnt = kendolist[idx].iniohdCnt+kendolist[idx].rcvCnt-kendolist[idx].notiCnt-kendolist[idx].issCnt
					kendolist[idx].ohdCntL = kendolist[idx].iniohdCntL+kendolist[idx].rcvCntL
					kendolist[idx].ohdCntR = kendolist[idx].iniohdCntR+kendolist[idx].rcvCntR
					kendolist[idx].ohdCntM = kendolist[idx].iniohdCntM+kendolist[idx].rcvCntM-kendolist[idx].notiCntM-kendolist[idx].issCntM
					kendolist[idx].ohdCntC = kendolist[idx].iniohdCntC+kendolist[idx].rcvCntC-kendolist[idx].notiCntC-kendolist[idx].issCntC
					kendolist[idx].ohdCntP = kendolist[idx].iniohdCntP+kendolist[idx].rcvCntP-kendolist[idx].notiCntP-kendolist[idx].issCntP
				}) */
				
				var saveChk="false"
/* 				$(kendolist).each(function (idx, data) {
					if(data.ohdCnt < 0 || data.ohdCntL < 0 || data.ohdCntR < 0 || data.ohdCntM < 0 || data.ohdCntC < 0 || data.ohdCntP < 0){
						alert(data.prdNo+" 차종의 재고수량이 0보다 작습니다.")
						saveChk="true";
						return false;
					}
				}) */
				if(saveChk=="true")
				return false;
				
				console.log("0-------------0씨")
				$(kendolist).each(function (idx, data) {
					console.log(data.ohdCntM)
					if(Number(data.ohdCnt)>2000){
						console.log("C " + idx + " :: " + data.ohdCnt)
					}
					if(data.ohdCnt.length>=5){
						console.log("C 글자 : " + idx + " :: " + data.ohdCnt)
					}
					if(Number(data.ohdCntM)>2000){
						console.log("M " + idx + " :: " + data.ohdCntM)
					}
					if(data.ohdCntM.length>=5){
						console.log("M 글자 : " + idx + " :: " + data.ohdCntM)
					}
				 })
				 console.log("----CnC합")
				 $(kendolist).each(function (idx, data) {
					console.log(data.ohdCntC)
				 })
				
				$.showLoading();
				var url = "${ctxPath}/chart/BertStckMasterSave.do";
				var obj = new Object();
				obj.val = kendolist;

				var param = "val=" + JSON.stringify(obj)
					+ "&date=" + $(".hasDatepicker").val();
				console.log(param);
				$.ajax({
					url: url,
					data: param,
					type: "post",
					success: function (data) {
						$.hideLoading();
						getStockStatus();
						alert("저장완료 되었습니다.")
					}
				})
			});

			$(".k-grid-finish").click(function () {
				confirm.open();
			})
			
			$(".k-grid-print").click(function () {
				printChkWin=true;
				win = window.open("","","height=" + screen.height + ",width=" + screen.width + "fullscreen=yes");
		        self.focus();
		        win.document.open();
		        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
		        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
		        win.document.write('#adb { background-color:red	}');
		        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
		        win.document.write(document.getElementById('printD').innerHTML);
		        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
		        win.document.close();
		        
		        if(printChkWin==true){
					$(window).click(function() {
						//Hide the menus if visible
						alert('프린터 창이 열려 있습니다')
					});
				}
		        win.print();
		       
		        printChkWin=false;
				$(window).off("click");
		        
				win.close();
			})
			
		})
		var beforedata = [];
		var test12 = [];

		function readOnly(container, options) {
			container.removeClass("k-edit-cell");
			container.text(options.model.get(options.field));
		}

		function date() {
			return '날짜  : <input type="text" class="date" readonly="readonly">'
		}
		var kendodata;
		//태이블 table


		function getStockStatus() {
			$(".k-button").css({
			    "display":"none"})
			classFlag = true;
			var url;
			var nowDate;
			var minusDate;
			if(moment().format("HH:mm")<"08:30"){
				nowDate=moment().subtract(1, 'day').format("YYYY-MM-DD")
				minusDate=moment().subtract(2, 'day').format("YYYY-MM-DD")
			}else{
				nowDate=moment().format("YYYY-MM-DD")
				minusDate=moment().subtract(1, 'day').format("YYYY-MM-DD")
			}
			
			if($(".date").val()==minusDate){
				console.log("날자가 어제입니다.")
				url = "${ctxPath}/chart/getfinishedStockStatus.do";
				$(".k-button").css({
				    "display":"inline"})
			}else if( $(".date").val()==nowDate ){
				console.log("날자가 오늘입니다.")
				url = "${ctxPath}/chart/getStockStatusDay.do";
				$(".k-button").css({
				    "display":"none"})
			}else{
				console.log("옛날꺼다")
				url = "${ctxPath}/chart/getfinishedStockStatus.do";
				$(".k-button").css({
				    "display":"none"})
				    
				$(".k-button.k-button-icontext.k-grid-print").css({
				    "display":"inline"})
			}
				
			$.showLoading()
			
			var param = "date=" + $(".date").val();
			nowDateInsert = $(".date").val();
			$.ajax({
				url: url,
				data: param,
				dataType: "json",
				type: "post",
				success: function (data) {
					var json = data.dataList;
					console.log(json)
					if(json.length==0){
						
					}else if(json.length!=0){
						if(json[0].chk==1){
							$(".k-button").css({
							    "display":"none"})
						}
					}
					
					$(json).each(function (idx, data) {
						var Lcnt=0;
						var Rcnt=0;
						data.ohdCntP = data.iniohdCntP+data.rcvCntP-data.issCntP-data.notiCntP;
				
						$(json).each(function (idx1, data1) {
							if(data.prdNo==data1.prdNo){
								Lcnt=Lcnt+Number(data1.rcvCntM)
							}
							if(data.item==data1.item){
								Rcnt=Rcnt+Number(data1.rcvCntR)
							}
						})
						if(data.minOpr=="0020"){
	 						data.ohdCntL = data.iniohdCntL+data.rcvCntL-Lcnt;
	 						
						}else{
							data.ohdCntL = data.iniohdCntL+data.rcvCntR-Rcnt;
							
						}

						data.ohdCntC = data.iniohdCntC+data.rcvCntC-data.issCntC-data.notiCntC;
						data.ohdCntM = data.iniohdCntM+data.rcvCntM-data.issCntM-data.notiCntM;
						data.ohdCnt = data.iniohdCnt+data.rcvCnt-data.issCnt-data.notiCnt;
						
					});
					
					
					
					 if (today == $(".date").val() && json[0].chk!=1) { //오늘 날짜일때 수정
						kendodata = new kendo.data.DataSource({
							data: json,
							batch: true,
							editable: false,
							group: { field: "prdNo" },
							sort: [{
		                    	field: "prdNo" , dir:"asc" 
		                    },{
		                    	field: "item" , dir:"asc"
		                    }],
							height: 500,
							schema: {
								model: {
									id: "id",
									fields: {
										prdNo: { editable: false },
										item: { editable: false },
										iniohdCnt: { editable: false },
										iniohdCntR: { editable: false },
										iniohdCntM: { editable: false },
										iniohdCntC: { editable: false },
										iniohdCntP: { editable: false },

										iniohdCntL: { editable: true },
										rcvCntL: { editable: true },
										ohdCntL: { editable: true },

										ohdCntR: { editable: true },
										ohdCntM: { editable: true },
										ohdCntC: { editable: true },
										ohdCntP: { editable: true },
										rcvCntM: { editable: true, type: "Number"}
									}
								}
							}
						});
					} else {//오늘이전날짜일때 수정불가능
						kendodata = new kendo.data.DataSource({
							data: json,
							batch: true,
							group: { field: "prdNo" },
							sort: [{
		                    	field: "prdNo" , dir:"asc" 
		                    },{
		                    	field: "item" , dir:"asc"
		                    }],
							height: 500,
							schema: {
								model: {
									id: "id",
									fields: { 
										prdNo: { editable: false },
										item: { editable: false },
										iniohdCnt: { editable: false },
										iniohdCntR: { editable: false },
										rcvCntR: { editable: false },
										iniohdCntM: { editable: false },
										iniohdCntC: { editable: false },
										iniohdCntP: { editable: false },
										
										iniohdCntL: { editable: true },
										rcvCntL: { editable: true },
										ohdCntL: { editable: true },

										rcvCnt: { editable: false },
										rcvCntM: { editable: false },
										rcvCntC: { editable: false },
										rcvCntP: { editable: false },
										
										issCnt: { editable: false },
										issCntR: { editable: false },
										issCntM: { editable: false },
										issCntC: { editable: false },
										issCntP: { editable: false },
										
										notiCnt: { editable: false },
										notiCntR: { editable: false },
										notiCntM: { editable: false },
										notiCntC: { editable: false },
										notiCntP: { editable: false },

										ohdCntR: { editable: false },
										ohdCntM: { editable: false },
										ohdCntC: { editable: false },
										ohdCntP: { editable: false },

										copyR: { editable: false },
										copyM: { editable: false },
										copyC: { editable: false },
										copyP: { editable: false },
										
									}
								}
							}
						}); 
					}
					//$("#wrapper").find("table").on("keydown", onGridKeydown);

					kendotable.setDataSource(kendodata);

					$("button, input[type='time']").css("font-size", getElSize(40));

					$(".alarmTable td").css({
						"padding": getElSize(20),
						"font-size": getElSize(40),
						"border": getElSize(5) + "px solid black"
					});

					scrolify($('#table2'), getElSize(1450));


					$("body, html").css({
						"overflow": "hidden"
					})
					
					var scroll = $("div.k-grid-content").scrollTop();
					kendotable.dataSource.fetch();
					$("div.k-grid-content").scrollTop(scroll);
					
					$(".date").css("font-size",getElSize(45))
					
				
					console.log("--print--")
					console.log(json)
					
					var color="white";
					var FlagC=1;
					var days=new Date(nowDateInsert)
					var week= new Array('일요일','월요일','화요일','수요일','목요일','금요일','토요일')
					days=days.getDay();
					days=week[days]
					
					
					var table="<table id='printT' align='center' border='4' width='95%'>";
					table +="<thead><tr style='background-color:#D5D5D5;'><th colspan='38' rowspan='3' class='Thead'> 수불장 ( "+ nowDateInsert +"  " + days + " )</th><th colspan='9' class='sign' colspan='2'>결제</th></tr><tr><th class='sign' colspan='2'>담당</th><th class='sign' colspan='2'>과장</th><th class='sign' colspan='2'>상무이사</th><th class='sign' colspan='3'>부사장</th></tr><tr><th class='sign' colspan='2'>　</th><th class='sign' colspan='2'>　</th><th class='sign' colspan='2'>　</th><th class='sign' colspan='3'>　</th></tr>"
					table+= "<tr style='background-color:#D5D5D5;'>" +
									"<th class='Ptitle'>차종(_RW)</th>" +
									"<th class='Pitem'>부품명</th>" +
									"<th class='Pbox'>전일재고</th>" +
									"<th class='Pbox'>입고</th>" +
									"<th class='Pbox'>불량</th>" +
									"<th class='Pbox'>누적입고</th>" +
									"<th class='Pbox'>출고</th>" +
									"<th class='Pbox'>창고재고</th>" +
									"<th class='Pspace'>　</th>" +
									"<th class='Pbox'>전일재고</th>" +
									"<th class='Pbox'>입고</th>" +
									"<th class='Pbox'>현재고</th>" +
									"<th class='Pspace'>　</th>" +
									"<th class='Pbox'>전일재고</th>" +
									"<th class='Pbox'>입고</th>" +
									"<th class='Pbox'>누적생산수량</th>" +
									"<th class='Pbox'>출고</th>" +
									"<th class='Pbox'>불량</th>" +
									"<th class='Pbox'>불량 누계</th>" +
									"<th class='Pbox'>재고</th>" +
									"<th class='Pspace'>　</th>" +
									"<th class='Pbox'>전일재고</th>" +
									"<th class='Pbox'>입고</th>" +
									"<th class='Pbox'>누적생산수량</th>" +
									"<th class='Pbox'>출고</th>" +
									"<th class='Pbox'>불량</th>" +
									"<th class='Pbox'>불량 누계</th>" +
									"<th class='Pbox'>재고</th>" +
									"<th class='Pspace'>　</th>" +
									"<th class='Pbox'>전일재고</th>" +
									"<th class='Pbox'>입고</th>" +
									"<th class='Pbox'>누적생산수량</th>" +
									"<th class='Pbox'>출고</th>" +
									"<th class='Pbox'>불량</th>" +
									"<th class='Pbox'>불량 누계</th>" +
									"<th class='Pbox'>재고</th>" +
									"<th class='Pspace'>　</th>" +
									"<th class='Pbox'>전일재고</th>" +
									"<th class='Pbox'>입고</th>" +
									"<th class='Pbox'>누적생산수량</th>" +
									"<th class='Pbox'>출고</th>" +
									"<th class='Pbox'>불량</th>" +
									"<th class='Pbox'>불량 누계</th>" +
									"<th class='Pbox'>재고</th>" +
									"<th class='Pspace'>　</th>" +
									"<th class='Pbox'>총 재고</th>" +
									"<th class='Pbox'>재고일수</th>" +
								"</tr>"  +
							"</thead><tbody>";

					$(json).each(function(idx, data){
							
						if(idx!=0){
							if(json[idx].prdNo==json[idx-1].prdNo){
								color=color
							}else{
								if(FlagC==1){
									color="white"
									FlagC=2
								}else{
									color="#D5D5D5"
									FlagC=1
								}
							}
						}
						//40개 빈공간 빼면
	//						var arr=new Object();
	//						var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
							//예외 리스트
							if(data.prdNo=="FS_FRT_RH_RW" || data.prdNo=="FS_FRT_LH_RW" || data.prdNo=="PM_FRT_17_KR_LH_RW" || data.prdNo=="PM_FRT_17_KR_RH_RW" || data.prdNo=="TQ_FRT_4WD_RW" || data.prdNo=="VQ_RR_LH_RW" || data.prdNo=="VQ_RR_RH_RW"){
							}else{

								table+="<tr style='background-color:"+color+";'><td align='center' >"
								table+=data.prdNo.substr(0,data.prdNo.lastIndexOf("_"))+"</td><td align='center'>";
								table+=data.item.substr(data.item.lastIndexOf("_")+1,2)+"</td><td align='center'>";
								table+=data.iniohdCnt + "</td><td align='center'>";
								table+=data.rcvCnt+"</td><td align='center'>";
								table+=data.notiCnt+"</td><td align='center'>";
								table+=data.issCnt+"</td><td align='center'>";
								table+=data.issCnt+"</td><td align='center'>";
								table+=data.ohdCnt+"</td><td align='center'>";
								
								table+="　"+"</td><td align='center'>";
								table+=data.iniohdCntL+"</td><td align='center'>";
								table+=data.rcvCntL+"</td><td align='center'>";
								table+=data.ohdCntL+"</td><td align='center'>";
								
								table+="　"+"</td><td align='center'>";
								table+=data.iniohdCntR + "</td><td align='center'>";
								table+=data.rcvCntR+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.issCntR+"</td><td align='center'>";
								table+=data.notiCntR+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.ohdCntR+"</td><td align='center'>";
								
								table+="　"+"</td><td align='center'>";
								table+=data.iniohdCntM + "</td><td align='center'>";
								table+=data.rcvCntM+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.issCntM+"</td><td align='center'>";
								table+=data.notiCntM+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.ohdCntM+"</td><td align='center'>";
								
								table+="　"+"</td><td align='center'>";
								table+=data.iniohdCntC + "</td><td align='center'>";
								table+=data.rcvCntC+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.issCntC+"</td><td align='center'>";
								table+=data.notiCntC+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.ohdCntC+"</td><td align='center'>";
								
								table+="　"+"</td><td align='center'>";
								table+=data.iniohdCntP + "</td><td align='center'>";
								table+=data.rcvCntP+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.issCntP+"</td><td align='center'>";
								table+=data.notiCntP+"</td><td align='center'>";
								table+='-'+"</td><td align='center'>";
								table+=data.ohdCntP+"</td><td align='center'>";
								
								table+="　"+"</td><td align='center'>";
								table+=9999+"</td><td align='center'>";
								table+='-'+"</td>";
								
								table+="</tr>"
							}

					});
					
					table+="</tbody></table>"
							
					
					$("#printD").empty();
					
					$("#printD").append(table);
					
					$("#printT tr th.Ptitle").css("width","5%");
					$("#printT tr th.Pitem").css("width","3%");
					$("#printT tr th.Pbox").css("width","2.3%");
					$("#printT tr th.sign").css("height","2.3%");
					$("#printT tr th.sign").css("height",30);
					$("#printT tr th").css("font-size",1)
					$("#printT tr td").css("font-size",1)
					$("#printT tr th.Thead").css("font-size",getElSize(74))
					$("#printT tr th.sign").css("font-size",12);

					$("#printT tr th").css("padding",0)
					$("#printT tr td").css("padding",0)
					
					
			        //////////	
					$.hideLoading();
				}, error: function (request, status, error) {
					alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
				}
			});
		};
		
		 function onGridKeydown(e) {
	            if (e.keyCode === kendo.keys.TAB) {
	                var grid = $(this).closest("[data-role=grid]").data("kendoGrid");
	                var current = grid.current();
	                
	                console.log("-----grid, current-----")
	                console.log(grid);
	                console.log(current)
	                console.log(current.hasClass("editable-cell"))
	                
	                if (!current.hasClass("editable-cell")) {
	                  console.log(1)
	                    var nextCell;
	                    if (e.shiftKey) {
	                        nextCell = current.prevAll(".editable-cell");
	                        if (!nextCell[0]) {
	                            //search the next row
	                            var prevRow = current.parent().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev();
	                            var nextCell = prevRow.children(".editable-cell:last");
	                          console.log(2)
	                        }
	                      console.log(3)
	                    } else {
	                      console.log(4)
	                        nextCell = current.nextAll(".editable-cell");
	                        if (!nextCell[0]) {
	                          console.log(5)
	                            //search the next row
	                            var nextRow = current.parent().next();
	                            var nextCell = nextRow.children(".editable-cell:first");
	                        }
	                    }
	                    
	                    console.log(nextCell)
	                    console.log(nextCell[0])
	                    grid.current(nextCell);
	                    grid.editCell(nextCell[0]);
	                }

	            }
	        };  
        
		var dialog;
		$(function(){
				dialog = $("#dialog").kendoDialog({
				  visible: false,
			      actions: [{
			          text: "OK",
			          action: function(e){
			              // e.sender is a reference to the dialog widget object
			              // OK action was clicked
			              // Returning false will prevent the closing of the dialog
			              return false;
			          },
			          primary: true
			      },{
			          text: "Cancel"
			      }],
			      closable: false,
			      height: 400,
			      width: 800,
			      title: "불량 등록 메뉴",
			      close: function(e) {
			    	  kendotable.dataSource.fetch();
			      },
			      content: "<div style='height:100%; overflow:scroll;'>" +
			    	    "<button style='float: right;' id='addNotiRow' onclick='addNotiRow()'>추가</button>"+
		    	  		"<table id='notiTable' style='width:100%; color:black; border: 1px solid #444444; border-collapse: collapse;'>"+
						"<tr><th><input type='checkbox'></th><th>LOT NO.</th><th>수량</th><th>사유</th><th>삭제</th></tr>"+
					    "</table>"+
					    "<center><button>닫기</button><center>"+
			      		"</div>"
						   
			    }).data("kendoDialog");
		})
		
		function dialog_open(container, options){
			console.log("이곳은  open 이벤트 발생 지역 입니다.")
			console.log(options)
			dialog.open()
			options.model.notiCnt=5
		}
		
		function addNotiRow(){
			console.log('addNotiRow')
			var table = document.getElementById("notiTable");
		    var row = table.insertRow(-1);
		    var cell0 = row.insertCell(0);
		    var cell1 = row.insertCell(1);
		    var cell2 = row.insertCell(2);
		    var cell3 = row.insertCell(3);
		    var cell4 = row.insertCell(4);
		    
		    cell0.innerHTML = "<input type='checkbox'>";
		    
		    cell1.innerHTML = "<select>"+
							  "<option>LOT00</option>"+
							  "<option>LOT01</option>"+
							  "<option>LOT02</option>"+
							  "</select>";
							  
		    cell2.innerHTML = "<input type='number'>";
		    
		    cell3.innerHTML = "<select>"+
		    				  "<option>재고조정</option>"+
		    				  "<option>생산 중 파손</option>"+
		    				  "<option>소재불량</option>"+
		    				  "</select>";
		    				  
		    cell4.innerHTML = "<button onclick='deleteRow(this)'>삭제</button>";
		}
		
		function deleteRow(e){
			var row = e.parentNode.parentNode;
			console.log(row)
			 row.parentNode.removeChild(row);
		}
		
	</script>
</head>

<body>
	<div id="delDiv">
		<Center>
			<font>
				<spring:message code="chk_del"></spring:message>
			</font>
			<Br>
			<br>
			<button id="resetOk" onclick="okDel();">
				<spring:message code="del"></spring:message>
			</button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();" style="background:red;">
				<spring:message code="cancel"></spring:message>
			</button>
		</Center>
	</div>
	<div id="confirm">마감 처리를 하시 겠습니까?</div>

	<div id="time"></div>
	<div id="title_right"></div>
	<div id="dialog"></div>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td>
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home">
				</td>
				<td>
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td>
					<img alt="" src="${ctxPath }/images/inven_left.png" class='menu_left'>
				</td>
				<td>
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%">
						<Tr>
							<td>
							</td>
						</Tr>
						<tr>
							<td>
								<div id="wrapper"></div>
								
							</td>
						</tr>
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	</div>
	
	<div id="printD" style="display: none;">
	</div>

	<div id="intro_back"></div>
	<span id="intro"></span>
</body>

</html>