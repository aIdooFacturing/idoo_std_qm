<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highchart.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller_IE.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/canvas.css">
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<%-- <script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script> --%>
<%-- <script type="text/javascript" src="${ctxPath }/js/canvas.js"></script> --%>
<style>
.chartTitle{
	position : absolute;
	top:-100;
	left:20%;
	right:0;
	margin:auto;
	font-size : 50;
}

#chart{
	overflow: hidden;
}

body{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
}

#canvas{
	background: url("../images/DashBoard/Road.png");
	background-size : 100% 100%;
	z-index: 99999;
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}

#title_main{
	left: 1300px;
	width: 1000px;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 800px;
	z-index: 99;
	top: 1480px;
	height: 700px;
	width: 750px;
}

#pieChart2{
	position: absolute;
	left : 300px;
	z-index: 99;
	top: 1480px; 
	height: 700px;
	width: 750px;
}

#tableDiv{
	left: 20px;
	width: 600px; 
	position: absolute; 
	z-index: 999;
	top: 450px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}

#go123{
	position: absolute;
	z-index: 999;
	width: 600;
	bottom : 50px;
	right: 0px;
}
#textBackground{
	background-color: white;
	width: 780px;
	height: 320px;
	z-index: 1;
	position: absolute;
	border-radius : 10px;
	opacity : 1;
	top: 1720;
	left: 2570;
}
#mainText{
	position: absolute;
	z-index: 2;
	font-size: 70px;
	font-weight: bolder;
	text-align: right;
	top: 1730;
	left: 2610;
}
</style> 
<script type="text/javascript">
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var loopFlag = null;
	var flag = true;
	function stopLoop(){
		if(flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		flag = !flag;
		alert("페이지 자동 이동 : " + flag);
		
	};
	
	$(function(){
		document.oncontextmenu = function() {stopLoop()};
		setDivPos();
		setInterval(time, 1000);
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		getNightlyMachineStatus();
		
		startPageLoop();
	});
	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			location.href = ctxPath + "/chart/multiVision.do";
		},1000*60*10);
	};
	
	function getNightlyMachineStatus(){
		var url = "${ctxPath}/device/getNightlyMachineStatus.do";
		var date =  new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var targetDate = year + "-" + month + "-" + day;
		var param = "targetDate=" + targetDate;
		
		$.ajax({
			url : url,
			data: param,
			dataType : "text",
			type : "post",
			success : function(data){
				var json = $.parseJSON(data);
				
				
				var tr = "";
				var status = "";
				$(json).each(function (i, data){
					if(data.status=="WAIT"){
						status = "완료";
					}else{
						status = "중단";
					};
					tr += "<tr>" +
							"<td  style='border:1px solid white; font-Size:25px;'>" + (i+1)  + "</td>" +
							"<td  style='border:1px solid white;font-Size:25px;'> " + data.wc + "</td>" +
							"<td style='border:1px solid white;font-Size:25px;'>" + data.stopDate + "</td>" +
							"<td style='border:1px solid white;font-Size:25px;'>" + data.stopTime + "</td>" + 
							"<td style='border:1px solid white;font-Size:25px;'>" + status + "</td>" +
						"</tr>";
				});
				
				if(json.length==0){
					tr = "<tr  style='border:1px solid white; font-Size:25px;'>" + 
							"<td colspan='5'>없음</tb>" + 
						"<tr>";
				};
				
				$("#table").append(tr);
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#Legend").css({
			"bottom" : 50,
			"left" : width/2 - $("#Legend").width()/2	
		});
	};
	
	function bodyNeonEffect(color) {
		var lineWidth = 70;
		var toggle = true;
		setInterval(function() {
			$("body").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= 2;
				if (lineWidth == 0) {
					toggle = false;
				}
			} else if (!toggle) {
				lineWidth += 2;
				if (lineWidth == 70) {
					toggle = true;
				}
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
</script>
</head>


<body oncontextmenu="return false">
	<div id="Legend" style="position: absolute; font-size: 50px;">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> 가동 
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> 대기 
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> 알람 
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> Power Off  
	</div>

	<!-- <div id="pieChart1"  ></div> -->
	<div id="pieChart2" ></div>
	
	<img src="${ctxPath }/images/DashBoard/go123.gif" id="go123">

	<div id="tableDiv" >
		<center><font style="font-size: 40px;color: white;font-weight: bold;">야간 무인 정지장비</font></center>
		<br><br>
		<table style="width: 100%; text-align: center; color: white;" id="table">
			<tr style="font-size: 30px; font-weight: bold;" class='tr'>
				<td class='td'>No</td>
				<td class='td'>설비명</td>
				<td class='td'>날짜</td>
				<td class='td'>정지시간</td>
				<td class='td'>상태</td>
			</tr>
		</table>
	</div>

	<img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title">	

	<!-- <div id="svg"></div> -->

	<font id="date"></font>
	<font id="time"></font>
	
	<!-- <canvas id="canvas" > </canvas>  -->
</body>
</html>