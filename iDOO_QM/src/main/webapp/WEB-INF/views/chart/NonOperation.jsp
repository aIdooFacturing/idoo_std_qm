<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("maintenance", "NonOperation")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => getTable()
		
		//getMene();
		getMenu();

		setEl();
	})
	
	//비가동 사유 리스트 담기 위한 변수
	var nonOpTy = [];

	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))

		$( "#sDate" ).datepicker({
			onSelect : function(e){
				var diff = new Date($("#eDate").val()).getTime()-new Date(e).getTime();
				diff = diff/1000/60/60/24;
				console.log(e)
				if(diff>186){
					alert("6개월 이상 조회하실수 없습니다._s")
// 					$("#sDate").val(moment($("#eDate").val()).subtract('days',90).format("YYYY-MM-DD"))
					$("#sDate").val(moment($("#eDate").val()).subtract('months',6).format("YYYY-MM-DD"))
					getTable();
				}else{
			    	//e == 날짜 
			    	getTable();
			    	$("#sDate").val(e);
				}
		    }
	    })
	    
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	var diff = new Date(e).getTime()-new Date($("#sDate").val()).getTime();
				diff = diff/1000/60/60/24;
				
				if(diff>186){
					alert("6개월 이상 조회하실수 없습니다._d")
// 					$("#eDate").val(moment($("#sDate").val()).add('days',90).format("YYYY-MM-DD"))
					$("#eDate").val(moment($("#sDate").val()).add('months',6).format("YYYY-MM-DD"))
					getTable();
				}else{
			    	//e == 날짜 
			    	getTable();
			    	$("#eDate").val(e);
				}
				
		    }
	    })
// 	    $( "#sDate" ).datepicker({
// 		    onSelect : function(e){
// 		    	//e == 날짜 
// 		    	getTable();
// 		    	$("#sDate").val(e);
// 		    }
// 	    })
// 	    $( "#eDate" ).datepicker({
// 		    onSelect : function(e){
// 		    	//e == 날짜 
// 		    	getTable();
// 		    	$("#eDate").val(e);
// 		    }
// 	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
		
		$("#worker").css({
			"width" : getElSize(270)
		})
		
		$("#historyPage").css({
			"background" : "blue"
			,"padding-top" : getElSize(40)
			,"padding-bottom" : getElSize(40)
			,"padding-left" : getElSize(75)
			,"padding-right" : getElSize(75)
			,"border-radius" : getElSize(15)
			,"cursor" : "pointer"
		})
	}
	
	function getMenu(){
		nonOpTyDataSource = new kendo.data.DataSource({});
		
		//이유 선택박스 가지고오기
		var url = ctxPath + "/chart/getNonOpTy.do"
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var options = ""
				$(json).each(function(idx, data){
					var obj = {
							"nonOpTy" : data.id,
							"nonOpName" : decode(data.nonOpTy)
					}
					
					if(data.use==0){
						nonOpTyDataSource.add(obj)
					}
					nonOpTy.push(obj)
				});
				
				getGrid();
			}	
		})
	}
	
	//비가동 전체 선택기능
	function checkOpAll(e){
		console.log($("#checkallOp").is(":checked"))
		if($("#checkallOp").is(":checked")){
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		kendotableOp.dataSource.fetch();
	}
	
	//체크박스 tbody 선택시 (비가동)
	function checkRowOp(e,idx){
		console.log("event click")
		console.log(idx)
		
		var gridList=kendotableOp.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		
		console.log(dataItem)
		
		 var row = $("#grid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = gridOp.dataItem(row)
		 console.log(initData)
		 
		 if(initData.checkSelect){
			 initData.checkSelect = false;
		 }else{
			 initData.checkSelect = true;
		 }
		 kendotableOp.dataSource.fetch();
	}
	
	function getGrid(){
		
		kendotableOp = $("#grid").kendoGrid({
			editable : true
			,height : getElSize(1650)
			,dataBound : function(e) {
				gridOp = this;
				gridCss()
			}
			,columns : [{
				field:"checkerOp",
				title:"<input type='checkbox' id='checkallOp' onclick='checkOpAll(this)'>"
				,width:originWidth * 0.055,
// 				template: "<input type='checkbox' onclick='checkRowOp(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" 
				template: "#if(rawIdx==0){#<input type='checkbox' onclick='checkRowOp(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/> #} else{# <input type='checkbox' disabled='disabled' class='checkbox'> #}#"
// 				template : "#if(divide=='0'){# xxx #}else{#ddd #}#"


			},{
				title : "장비"
				,field : "oprNo"
			},{
				title : "비가동 유형"
				,field : "chartStatus"
			},{
				title : "시작"
				,field : "sDate"
			},{
				title : "종료"
				,field : "eDate"
				,template : "#=nonIngView(eDate,rawIdx)#"
			},{
				title : "비가동<br>시간(분)"
				,field : "waitTime"
				,template : "#=nonWaitView(waitTime,rawIdx)#"
			},{
    			field: "reason",
				title: "비가동사유",
				template : "#=changeNonOpty(reason,rawIdx)# ",
				editor: categoryDropDownEditorByNonOpty,
				editable : false,
    			attributes: {
        			style: "text-align: center; font-size: " + getElSize(35) + "px"
      			}
			}]
		}).data("kendoGrid")
		
		getTable();
	}
	// 집계중 표시
	function nonIngView(date,idx){
		//집계중일때
		if(idx==1){
			return "비가동 집계중"
		}else if(idx==0){
			return date
		}
	}
	//
	function nonWaitView(time,idx){
		//집계중일때
		if(idx==1){
			return " - "
		}else if(idx==0){
			return time
		}
	}
	function categoryDropDownEditorByNonOpty(container, options) {
		if(options.model.rawIdx==1){
			return;
		}
		$('<input name="' + options.field + '"/>')
           .appendTo(container)
           .kendoDropDownList({
           	valuePrimitive: true,
               dataTextField: "nonOpName",
               dataValueField: "nonOpTy",
//                optionLabel: "==선택==",
//                value : nonOpTyDataSource[0].nonOpTy,
               dataSource: nonOpTyDataSource,
               change:function(e){
            	   gridCss();
               }
         });
    }
	
	
	function gridCss(){
		
		$(".kendo thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$(".kendo tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px"
		})
	
		$("#nonOpList thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$("#nonOpList tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px",
			"text-align" : "center"
		})
		// check box 사이즈
		$("#checkallOp").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})

		// check box 사이즈
		$("#checkall").css({
			"width" : originWidth * 0.035,
			"height" : originWidth * 0.035
		})
		
		// check box 사이즈
		$(".checkbox").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})
		
		$(".k-grouping-row td").css({
			"color" : "white",
			"text-align" : "left"
		})
		
		$(".k-i-collapse").remove()
		
	}
	
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getTable();
				
			}
		});
	}
	
	function changeNonOpty(num,idx){
		if(idx==1){
			return "-"
		}
		
		var length = nonOpTy.length;
		for(var i=0;i<length;i++){
			if(nonOpTy[i].nonOpTy==num){
				return nonOpTy[i].nonOpName
			}
		}
		return "사유를 선택해주세요"
	}
	
	function getTable(){
		
// 		var param = "sDate=" + $("#sDate").val() +
// 					"&eDate=" + $("#eDate").val();
		
		var url = "${ctxPath}/chart/getWorkerWatingTime.do";
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val();
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading();

				var json = data.dataList;
				console.log(json)
				
				$(json).each(function(idx,data){
					data.name = decode(data.name)
					data.oprNo = decode(data.oprNo);
					data.reason = 0;
					data.time = data.waitTime;
					data.checkSelect = false;
				})
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
	                autoSync: true,
	                group: [{field:"oprNo",dir:"asc"}],
	                sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "sDate" , dir:"asc"
                    }],
					aggregate: [
						{ field: "time", aggregate: "sum" },
					],	
	                schema: {
	                    model: {
							id: "a",	 
							fields: {
								checkerOp :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	oprNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	chartStatus: { editable: false,validation: { required: false },nullable:true },
		                    	sDate: { editable: false,validation: { required: false },nullable:true },
		                    	eDate: { editable: false,validation: { required: false },nullable:true },
		                    	waitTime: { editable: false,validation: { required: false },nullable:true },
		                    	reason: { editable: true,validation: { required: false },nullable:true },
		                    }
	                    }
	                }
	            });
				
				kendotableOp.setDataSource(dataSource)
			}
		})
	}
	
	function saveRow(){
		var savelist = [];
		var json = kendotableOp.dataSource.data();
		// 선택되어 있는 리스트 가져오기
		$(json).each(function(idx,data){
			if(data.checkSelect==true){
				savelist.push(data)
			}
		})
		
		if(savelist.length==0){
			alert("비가동 보고하실 항목을 선택해 주세요.");
			return ;
		}
		
		for(i=0 , len = savelist.length; i < len; i++) {
			if(savelist[i].reason==0){
				alert("비가동 사유를 선택해 주세요.");
				return;
			}
		}

		
		var obj = new Object();
		obj.val = savelist;

		var url = "${ctxPath}/chart/addNonOpHst_One.do";
		var param = "val=" + JSON.stringify(obj);

		//비가동 사유 입력 
// 		console.log("--저장리스트--")
// 		console.log(param)
		
// 		return;
		$.ajax({
				url : url,
				data : param,
				type : "post",
				success : function(data){
					if(data=="success"){
						getTable();
						alert("저장되었습니다;");
						return;
					}else if(data=="fail"){
						alert("저장에 실패하셨습니다. 관리자에게 문의해 주세요_0930");
						return;
					}
					console.log(data);
				}
		})
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				작업자 : <select id="worker"></select>
				　날짜 <input type="text" id="sDate" class="date" readonly="readonly">
				 ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
				<button onclick="saveRow()">
					저장
				</button>
			</div>

			<div style="display: table-cell; vertical-align: middle;">
				<span id="historyPage" onclick="location.href='${ctxPath}/chart/NonOperationHistory.do'">비가동 내역 조회</span>
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
			
			</div>
		</div>
	</div>	
</body>
</html>	