<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/x-kendo-template" id="windowTemplate">
	<center>
    	<button class="k-button" id="yesButton">${check}</button>
    	<button class="k-button" id="noButton">${cancel}</button>
	</center>
</script>
<script type="text/javascript">
	var chk_del = "${chk_del}";
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/jquery.min.js"></script> 
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
select{
	padding: .1em .5em;
    width: 12.4em;
}

#table22{
	border-bottom : 1px solid #1E1E23  !important;
	border-right : 1px solid #1E1E23 !important;
	border-left : 1px solid #1E1E23  !important;
	border-top : 1px solid #1E1E23 !important;
}
.k-grid-header-wrap{
	border-right : 1px solid #1E1E23 !important;
}

.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
}
.k-grid-header thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid-content tbody > tr
{
 background : #DCDCDC ;
 text-align: center;
}
.k-grid-content tbody > .k-alt
{
 background : #F0F0F0 ;
 text-align: center;
}

.k-grid-content tbody > tr:hover, .k-grid-content tbody > .k-alt:hover 
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}

button:hover{
	cursor: pointer;
	background: #6699F0 !important;
	border-color : #6699F0 !important;
}
</style> 
<script type="text/javascript">

const loadPage = () =>{
	createMenuTree("maintenance","workerMaanger")
}

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};

	function closeForm(){
		$("#insertForm").css({
			"z-index" : -999,
			"display" : "none"
		});
		closeCorver()
	}
	
	function addWorker(){
		var url = "${ctxPath}/chart/addWorker.do";
		var param = "id=" + $("#workerId").html() + 
					"&name=" + $("#name").val() + 
					"&pwd=" + $("#pwd").val() + 
					"&part=" + $("#part").val() + 
					"&email=" + $("#email").val()+
					"&LV=" + $("#LV").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success"){
					closeForm();
					//getWorkerList();
					location.reload()
					
				}
			}
		});
	};
	
	$(function(){
		windowTemplate = kendo.template($("#windowTemplate").html());
		delMsg = $("#window").kendoWindow({
		    title: chk_del,
		    visible: false, //the window will not appear before its .open method is called
		    width: getElSize(1000) + "px",
		    height: getElSize(250) + "px",
		}).data("kendoWindow");
		
		getWorkerList();
		bindEvt2();
		$(".excel").click(csvSend);
		setEl();
		setDate();
		
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#startDate_form").val() + "," + $("#startTime_form").val());
		var endTime = new Date($("#endDate_form").val() + "," + $("#endTime_form").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return timeDiff;
	}
	
	
	function bindEvt2(){

	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	var csvOutput;
	function csvSend(){
		var sDate, eDate;
		
		sDate = $("#sDate").val();
		eDate = $("#eDate").val();
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};

	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(40),
			"width" : $("#container").width() - getElSize(80),
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(25),
			"padding" : getElSize(15),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : -1,
			"background-color" : "rgba(0,0,0,0.7)",
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8),
			"color" : "black",
		})
		
		$("select").css({
			"font-size" : getElSize(48),
			"height" : getElSize(80),
			"border": "1px #F0F0F0 #999",
           "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "#F0F0F0",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "black",
            "border" : "none",
			"padding-top": getElSize(10),
			"padding-bottom": getElSize(10),
			"width" : getElSize(450)
		});
		
		
		$("input").css({
			 "border": "1px #F0F0F0 #999",
	            "width" : getElSize(450),
	            "height" : getElSize(80),
	            "font-family": "inherit",
	            "background-color" : "F0F0F0",
	            "z-index" : "999",
	            "border-radius": "0px",
	            "appearance":"none",
	            "background-size" : getElSize(60),
	            "color" : "black",
	            "border" : "none",
	            "font-size" : getElSize(48),
				"border-color" : "#222327"
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		$(".alarmTable td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(35),
			"border": getElSize(1) + "px solid black"
		});
		
		$("#wrapper").css({
			"height" :getElSize(1550),
			"width" : $(".right").width(),
			"overflow" : "auto"
		});
		
		$("#insertForm").css({
			"width" : getElSize(1500),
			"position" : "absolute",
			"z-index" : -999,
			"display" : "none",
		    "border": getElSize(5) + "px solid #00C6FF"
		});
		
		
		$("#insert table td, #update table td").css({
			"color" : "black",
			"font-size" : getElSize(36),
			"padding" : getElSize(30),
			"text-align" : "Center",
			"background-color" : "#DCDCDC"
		});
		
		$("#insertForm").css({
			"left" : (originWidth/2) - ($("#insertForm").width()/2),
			"top" : (originHeight/2) - ($("#insertForm").height()/2)
		});
		
		$(".table_title").css({
			"background-color" : "#353542",
			"color" : "white"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};

	
	var className = "";
	var classFlag = true;

	function getWorkerList(){
		classFlag = true;
		var url = "${ctxPath}/common/getAllWorkerList.do";
		var list=[];

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					if(data.seq!="."){
						var arr=new Object();
						
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						arr.a=data.id;
						arr.b=decodeURIComponent(data.name).replace(/\+/gi, " ");
						arr.c=decodeURIComponent(data.part).replace(/\+/gi, " ");
						arr.d=data.email;
						arr.LV=data.LV;
						list.push(arr);

					}
				});

				$("#table22").kendoGrid({
					height:getElSize(1580),
					dataSource:list,
					columns:[{
						field:"a",title:"${worker_num}",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"b",title:"${name}",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"c",title:"${depart}",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"d",title:"Email",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"LV",
						title:"권한",
						template: "#=getLv(LV)#",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},
					{
        				command : [{
        					name : "퇴사",
        					click : function(e){
        						del_btn_evt(e,this,"delWorker()")
        						//delRouting(e,this)
        					}
        				}],
        				attributes: {
            				style: "text-align: center; font-size: " + getElSize(36) + "px;"
          				}
        			}
					/* ,
					{
						command : [{
							name : "퇴사"
							,click : function(e){
								var tr = $(e.target).closest("tr"); // get the current table row (tr)
					            var data = this.dataItem(tr);
								console.log(data)
								del_workerId=data.a
								var url = "${ctxPath}/chart/delWorker.do";
								var param = "workerId=" + del_workerId;
								
								
								$.ajax({
									url : url,
									data : param,
									type : "post",
									dataType : "text",
									success :function(data){
										if(data=="success"){
											getWorkerList();
										}
									}
								});
							}
						}]
					} */]
				});
				
			    var xgrid = $("#table22").data("kendoGrid");

			    $(xgrid.tbody).on("click", "td", function (e) {
			    		if(typeof($(this).children()[0])!="undefined"){
			    			return
			    		};
			    		
			            var row = $(this).closest("tr");
			            var curRowIdx = $("tr", xgrid.tbody).index(row);
			            var colIdx = $("td", row).index(this);
			            var item = xgrid.dataItem(row);
			            showEditForm(item.a);
			    });
			
				$(".k-button").css({
					"height" : getElSize(60),
					"width" : getElSize(200),
					"border-color" : "#9B9B9B",
					"background" : "#9B9B9B",
					"border-radius" : getElSize(8),
					"color" : "black",
				});
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(1) + "px solid black"
				});
				

			}
		});
	};
	
	function getLv(LV){
		if(LV==1){
			return '작업자';
		}else if(LV==2){
			return '관리자';
		}else if(LV==3){
			return 'ADMIN';
		}
	}
	
	var windowTemplate, delMsg;
	function del_btn_evt(e, el, cd){
		e.preventDefault();
		var tr = $(e.target).closest("tr");
		var data = el.dataItem(tr); //get the row data so it can be referred later
		
		
		delMsg.content(windowTemplate(data)); //send the row data object to the template and render it
		delMsg.center().open();
		
		
		var grid = $("#table22").data("kendoGrid");
	    var dataItem = grid.dataItem(tr);
		var id = dataItem.id 
		 
		del_workerId = dataItem.a;
	    $("#yesButton").click(function(){
	    	eval(cd)
	        grid.dataSource.remove(data)
	        delMsg.close();
	    })
	    $("#noButton").click(function(){
	    	delMsg.close();
	    }) 
	}

	var del_workerId;
	function delWorker(){
		var url = "${ctxPath}/chart/delWorker.do";
		var param = "workerId=" + del_workerId;
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success :function(data){
				if(data=="success"){
					getWorkerList();
				}
			}
		});
	};
	
	function showEditForm(id){
		var url = "${ctxPath}/chart/getWorkerInfoo.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$("#workerId").html(data.id);
				$("#name").val(data.name);
				$("#pwd").val(data.pwd);
				$("#part").val(data.part);
				$("#email").val(data.email);
				
				$("#insertForm").css({
					"z-index" : 999,
					"display" : "inline"
				});
				
				showCorver();
			}
		});
	};

	function showInsertForm(){
		$("input").val("")
		getNextWorkerId();
	};

	function getNextWorkerId(){
		var url = "${ctxPath}/chart/getNextWorkerId.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				$("#workerId").html(data);
				
				$("#insertForm").css({
					"z-index" : 999,
					"display" : "inline"
				});
				
				showCorver();
			}
		});
	};
	function showCorver(){
		$("#corver").css("z-index", 999);	
	};

	function hideCorver(){
		$("#corver").css("z-index", -999);
	};

	
	function closeInsertForm(){
		hideCorver();
		$("#insertForm, #updateForm").css("z-index",-9999);
		$("#updateForm #rcvCnt").css("color", " white");
		return false;
	};
	
	</script>
</head>
<body>
	<div id="window"></div>
	<div id="corver"></div>
	
	<div id="insertForm">
		<form action="" id="insert">
			<Table style="width: 100%">
				<tr >
					<td class='table_title'>
						<spring:message code="worker_num"></spring:message>
					</td>
					<td id="workerId">
						
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						<spring:message code="name"></spring:message>
					</td>
					<td>
						<input type="text" id="name">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						<spring:message code="pwd"></spring:message>
					</td>
					<td>
						<input type="password" id="pwd">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						<spring:message code="depart"></spring:message>
					</td>
					<td>
						<input type="text" id="part">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						Email
					</td>
					<td>
						<input type="text" id="email">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						권한
					</td>
					<td>
						<select id="LV">
							<option value='1'>작업자</option>
							<option value='2'>관리자</option>
							<option value='3'>ADMIN</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;"><button onclick="addWorker(); return false;"><spring:message code="confirm"></spring:message></button> <button onclick="closeForm(); return false;"><spring:message code="cancel"></spring:message></button> </td>
				</tr>
			</Table>
		</form>
	</div>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table style="width: 100%">
						<Tr>
							<!-- <Td align="right"><button onclick="getIncomStock()">조회</button></Td> --><Td align="right"><button onclick="showInsertForm()"><spring:message code="add"></spring:message> </button></Td>
						</Tr>
					</table>
					<div id="table22"></div>
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	