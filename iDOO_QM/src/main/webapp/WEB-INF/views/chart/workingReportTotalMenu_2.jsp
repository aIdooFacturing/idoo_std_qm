<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
</style> 
<script type="text/javascript">
	var windowTemplate, delMsg;

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	var handle = 0;

	function getWorkerList(){
		classFlag = true;
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "";
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
					
					var object = {
							"workerCd" : data.id
							,"name" : decode(data.name)
					} 
					
					workerArray.push(object)
				});
				
				$(".worker").html(options)
			}
		});
	};
	
	$(function(){
		$("#popup_nonOp #date").attr("disabled",true)
		$("#popup_nonOp .worker").attr("disabled",true)
		$("#popup_nonOp #nonOpTime").attr("disabled",true)
		
		windowTemplate = kendo.template($("#windowTemplate").html());
		delMsg = $("#window").kendoWindow({
		    title: "삭제하시겠습니까?",
		    visible: false, //the window will not appear before its .open method is called
		    width: "300px",
		    height: "100px",
		}).data("kendoWindow");
		
		getWorkerList();
		createNav("order_nav", 1);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#_table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$(".nav_span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$(".sub_menu").css({
			"color" : "rgb(68, 11, 246)",
			"float" : "left",
			"cursor" : "pointer",
			"text-align" : "center",
			"font-size" : getElSize(100) + "px",
			"margin" : getElSize(50) + "px",
			"display" : "table",
			"width" : getElSize(1000) + "px",
			"height" : getElSize(500) + "px",
			"border-radius" : getElSize(30) + "px",
			"background-color" : "rgb(121, 218, 76)"
		}).click(function (){showPopup(this)});
		
		$(".sub_menu span").css({
			"display" : "table-cell",
			"vertical-align" : "middle",
		});
		
		$("#popup").css({
			"background-color" : "rgb(50,50,50)",
			"padding" : getElSize(50) + "px",
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$("#popup table td").css({
			"color" : "white",
			"font-size" : getElSize(80) + "px"
		})
		
		$("#popup table input, #popup table select, #popup button").css({
			"margin" : getElSize(20) + "px",
			"font-size" : getElSize(80) + "px",
			"height" : getElSize(160) + "px"
		});
		
		$("#popup2 button").css({
			"margin" : getElSize(20) + "px",
			"font-size" : getElSize(80) + "px"
		});
		
		$("#popup").css({
			"left" : (originWidth/2) - ($("#popup").width()/2),
			"top" : (originHeight/2) - ($("#popup").height()/2),
			"border" : getElSize(10) + "px solid yellow"
		});
		
		$("#popup2 table td").css({
			"color" : "white",
			"font-size" : getElSize(70) + "px"
		});
		
		$("#popup2").css({
			"background-color" : "rgb(50,50,50)",
			"padding" : getElSize(50) + "px",
			"left" : (originWidth/2) - ($("#popup2").width()/2),
			"top" : (originHeight/2) - ($("#popup2").height()/2)
		});
		
		$("#popup_nonOp, #popup_attendance, #popup2").css({
			"display" : "none",
			"width" : getElSize(3500) + "px", 
			"height" : getElSize(1800) + "px",
			"border" : getElSize(10) + "px solid yellow",
			"z-index" : 999,
			"position" : "absolute"
		});
		
		$("#popup_nonOp table td, #popup_attendance table td").css({
			"color" : "white",
			"font-size" : getElSize(70) + "px"
		});
		
		$("#popup_nonOp, #popup_attendance, #popup2").css({
			"background-color" : "rgb(50,50,50)",
			"padding" : getElSize(50) + "px",
			"left" : (originWidth/2) - ($("#popup_nonOp").width()/2),
			"top" : (originHeight/2) - ($("#popup_nonOp").height()/2)
		});
		
		$("#popup_nonOp button, #popup_attendance button").css({
			"margin" : getElSize(20) + "px",
			"font-size" : getElSize(80) + "px"
		});
		
		$("#popup2 #tableDiv").css({
			"height" : getElSize(1300)
		});
		
		$("#popup_nonOp #tableDiv, #popup_attendance #tableDiv").css({
			"height" : getElSize(1600)
		});
		
		$("#popup_attendance select, #popup_attendance input, #popup_nonOp select, #popup_nonOp input, #popup2 select, #popup2 input").css({
			"height" : getElSize(140) + "px",
			"font-size" : getElSize(60)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function addStartWork(){
		if(chkWorkeTimeValid()=="success"){
			var url = "${ctxPath}/chart/addWorkTimeHist.do";
			var param = "worker=" + $("#popup .worker").val() + 
						"&ty=I";
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					if(data=="success"){
						closePopup();
						alert("출근처리 되었습니다.")
					}else if(data=="duple"){
						alert("이미 출근 하셨습니다.")			
					}
				}
			});
		}else{
			alert("출근 시간이 아닙니다.")
		}
		
	}
	
	function addPrdCmpl(){
		$("#popup2 .worker").attr("disabled",true);
		$("#popup2 #date").val(getToday().substr(0,10))
		closePopup();
		$("#popup2").css({
			"display" : "inline"
		});
	}
	
	function init(){
		selectedRow = {}
		
		$("#reason").val("")
		$("#popup_nonOp .worker").val($("#popup .worker").val())
		$("#popup_nonOp #date").val(getToday().substr(0,10));
		$("#popup_nonOp #sTime, #popup_nonOp #eTime").val(getTime());
		$("#popup_nonOp #nonOpTime").val("");
		
		$("#popup_attendance #date").val(getToday().substr(0,10));
		$("#popup_attendance #sTime, #popup_attendance #eTime").val(getTime());
	};
	
	var checkedIds = {};
	function selectRow($grid,el) {
	    var checked = el.checked,
	    row = $(el).closest("tr"),
	    grid = $grid;
	    dataItem = grid.dataItem(row);

	    checkedIds[dataItem.id] = checked;
	    if (checked) {
	        row.addClass("k-state-selected");
	    }else{
	        row.removeClass("k-state-selected");
	    }
	}
	
	var selectedRow = {};
	
	function addAttendance(){
		$("#popup_attendance .worker").attr("disabled",true);
		$("#popup_attendance #date").attr("disabled",true);
		
		closePopup();
		
		$("#popup_attendance").css({
			"display" : "inline"
		});
		
		$("#popup_attendance #date").val(getToday().substr(0,10));
		$("#popup_attendance #sTime, #popup_attendance #eTime").val(getTime())	
		
		var url = "${ctxPath}/chart/getAttendanceHist.do";
		var param = "worker=" + $("#popup_attendance .worker").val() + 
					"&date=" + $("#popup_attendance #date").val();
		
		var dataList = [];
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				console.log(json)
				$(json).each(function(idx, data){
					var object = {
							"id" : data.id
							,"worker" : data.worker
							,"attendanceTy" : data.group1
							,"date" : data.date
							,"attendanceDiv" : data.group2
							,"attendance" : data.group3
							,"sDate" : data.sDate
							,"eDate" : data.eDate
							,"reason" : decode(data.reason)
					}
					
					dataList.push(object)
				});
			}
		});
		
		$("#popup_attendance #tableDiv").kendoGrid({
            dataSource: new kendo.data.DataSource({
                data: dataList,
                batch: true,
                schema: {
                       model: {
                         id: "id",
                         fields: {
                            id: { editable: false},
                            worker: { editable : false },
                            attendanceTy: { editable : true },
                            date : { editable : true },
                            attendanceDiv : { editable : true },
                            attendance : { editable : true},
                            sDate : { editable : true },
                            eDate : { editable : true },
                            reason: { editable : true }, 
                         }
                       }
                   },
            pageSize: 10
        	}),
        	//change: onAttendanceChange,
        	change: function() {
        		  var cell = this.select();
        		  var cellIndex = cell[0].cellIndex;
        		  var column = this.columns[cellIndex];
        		  var dataItem = this.dataItem(cell.closest("tr"));
        		  
        		  selectedRow = {
        				  id : dataItem.id,
        				  worker : dataItem.worker,
        				  attendanceTy : dataItem.attendanceTy,
        				  attendanceDiv : dataItem.attendanceDiv,
        				  attendance : dataItem.attendance,
        				  date : dataItem.date,
        				  sDate : dataItem.sDate,
        				  eDate : dataItem.eDate,
        				  reason : dataItem.reason
        		  };
        		  
        		  $("#popup_attendance #attendance_ty").val(dataItem.attendanceTy);
        		  $("#popup_attendance #attendance_div").val(dataItem.attendanceDiv);
        		  $("#popup_attendance #assiduity").val(dataItem.attendance);
        		  $("#popup_attendance #date").val(dataItem.date);
        		  $("#popup_attendance #sTime").val(dataItem.sDate);
        		  $("#popup_attendance #eTime").val(dataItem.eDate);
        		  $("#popup_attendance #reason").val(dataItem.reason);
        	},
        		  
        	selectable: "row",
            height: getElSize(1150),
            pageable: true,
         //  toolbar: ["create"],
            groupable: false,
            sortable: true,
            
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
						/* {
							field : "id"
							,title : "선택"
							,template: "<input type='checkbox' class='checkbox'  />" 
							,width : getElSize(170) + "px"
							,attributes: {
                    			style: "text-align: center;"
                  			}
						}, */
                      	{
                			field: "worker",
                			title: "${worker}",
                			editor: categoryDropDownWorkerNameEditor,
                			template : "#= workerName(worker) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "attendanceTy",
                			title: "${attendance_ty}",
                			editor : categoryDropDownAttendanceTyNameEditor,
                			template : "#= attendanceTyName(attendanceTy) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "date",
            				title: "${date_}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "attendanceDiv",
                			title: "${attendace_div}",
                			editor : categoryDropDownAttendanceDivNameEditor,
                			template : "#= attendanceDivName(attendanceDiv) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "attendance",
                			title: "${assiduity}",
                			editor: categoryDropDownAttendanceEditor,
                			template: "#= attendanceName(attendance) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "sDate",
            				title: "${start}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "eDate",
                			title: "${end}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "reason",
                			title: "${reason}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
            				command : [{
            					name : "${del}",
            					click : function(e){
            						grid_attendance = $("#popup_attendance #tableDiv").data("kendoGrid");
            						del_btn_evt(e,this,"delAttendanceHistory(id)", grid_attendance)
            					}
            				}],
            				attributes: {
                				style: "text-align: center; font-size: " + getElSize(35) + "px;"
              				}
            			}
            		],
            		editable: false
        });
		
		$("#popup_attendance #tableDiv th").css({
			"font-size" : getElSize(50) + "px",
			"text-align" : "center"
		})
		
		$(".checkbox").css({
					"width" : getElSize(70) + "px",
					"height" : getElSize(70) + "px"
				})
				
		$(".k-button").css({
			"padding" : getElSize(10)
		})
		grid_attendance = $("#popup_attendance #tableDiv").data("kendoGrid");
		grid_attendance.table.on("click", ".checkbox" , function(){selectRow(grid_attendance,this)});
		
		grid_attendance.table.on("click", ".checkbox" , function(){selectRow(grid_attendance,this)});
		
		
	};
	
	function addNonOp(){
		closePopup();
		$("#popup_nonOp").css({
			"display" : "inline"
		});
		
		$("#popup_nonOp #date").val(getToday().substr(0,10));
		$("#popup_nonOp #sDate, #popup_nonOp #eDate").val(getToday().substr(0,10)).change(function(){
			$("#nonOpTime").val(calcTimeDiff($("#popup_nonOp #sTime").val(), $("#popup_nonOp #eTime").val()))	
		});
		$("#popup_nonOp #sTime, #popup_nonOp #eTime").val(getTime()).change(function(){
			$("#nonOpTime").val(calcTimeDiff($("#popup_nonOp #sTime").val(), $("#popup_nonOp #eTime").val()))	
		});
		
		
		var url = "${ctxPath}/chart/getNonOpHist.do";
		var param = "worker=" + $("#popup_nonOp .worker").val() + 
					"&date=" + $("#popup_nonOp #date").val();
		
		var dataList = [];
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var obj = {
							"id" : data.id,
							"worker" : data.worker,
							"dvcId" : data.dvcId,
							"date" : data.date,
							"nonOpTy" : data.nonOpTy,
							"sDate" : data.sDate,
							"eDate" : data.eDate,
							"nonOpTime" : parseInt(data.nonOpTime/60) + "시간 " + parseInt(data.nonOpTime%60) + "분"
					}	
					dataList.push(obj)
				});
				
			}
		});
		
		$("#popup_nonOp #tableDiv").kendoGrid({
            dataSource: new kendo.data.DataSource({
                data: dataList,
                batch: true,
                schema: {
                       model: {
                         id: "id",
                         fields: {
                            id: { editable: false},
                            worker: { editable : false },
                            dvcId: { editable : true },
                            date : { editable : false },
                            nonOpTy : { editable : true },
                            sDate : { editable : true},
                            eDate : { editable : true },  
                            nonOpTime: { editable : true },
                         }
                       }
                   },
            pageSize: 10
        	}),
        	change: function() {
	      		  var cell = this.select();
	      		  var cellIndex = cell[0].cellIndex;
	      		  var column = this.columns[cellIndex];
	      		  var dataItem = this.dataItem(cell.closest("tr"));
	      		  
	      		selectedRow = {
      				  id : dataItem.id,
      				  worker : dataItem.worker,
      				date : dataItem.date,
      				dvcId : dataItem.dvcId,
      				nonOpTime : dataItem.nonOpTime,
      				nonOpTy : dataItem.nonOpTy,
      				  sDate : dataItem.sDate,
      				  eDate : dataItem.sDate,
      		  };
	      		
		      	$("#popup_nonOp #date").val(dataItem.date)
		      	$("#popup_nonOp #dvcId").val(dataItem.dvcId)
		      	$("#popup_nonOp #nonOpTy").val(dataItem.nonOpTy)
		      	$("#popup_nonOp #nonOpTime").val(dataItem.nonOpTime)
		      	$("#popup_nonOp #sDate").val(dataItem.date)
		      	$("#popup_nonOp #eDate").val(dataItem.date)
		      	$("#popup_nonOp #sTime").val(dataItem.sDate)
		      	$("#popup_nonOp #eTime").val(dataItem.eDate)
	      		
	      	},
	      		  
	      	selectable: "row",
            height: getElSize(1150),
            pageable: true,
         //  toolbar: ["create"],
            groupable: false,
            sortable: true,
            
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
						/* {
							field : "id"
							,title : "선택"
							,template: "<input type='checkbox' class='checkbox'  />" 
							,width : getElSize(170) + "px"
							,attributes: {
                    			style: "text-align: center;"
                  			}
						}, */
                      	{
                			field: "worker",
                			title: "${worker}",
                			editor: categoryDropDownWorkerNameEditor,
                			template : "#= workerName(worker) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			}, 
            			{
                			field: "dvcId",
            				title: "${device}",
            				editor: categoryDropDownDvcNameEditor,
            				template: "#= dvcName(dvcId) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "date",
                			title: "${date_}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "nonOpTy",
                			title: "${non_operation_ty}",
                			editor: categoryDropDownNonOpTyEditor,
                			template: "#= nonOpTyName(nonOpTy) #",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "sDate",
            				title: "${start}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "eDate",
                			title: "${end}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
                			field: "nonOpTime",
                			title: "${time_}",
                			attributes: {
                    			style: "text-align: center; font-size: " + getElSize(35) + "px"
                  			}
            			},
            			{
            				command : [{
            					name : "${del}",
            					click : function(e){
            						grid = $("#popup_nonOp #tableDiv").data("kendoGrid");
            						del_btn_evt(e,this,"delNonOpHistory(id)", grid)
            					}
            				}],
            				attributes: {
                				style: "text-align: center; font-size: " + getElSize(35) + "px;"
              				}
            			}
            		],
            		//editable: true
        });
		
		$("#popup_nonOp #tableDiv th").css({
			"font-size" : getElSize(35) + "px",
			"text-align" : "center"
		})
		
		$(".k-button").css({
			"padding" : getElSize(10)
		})
		grid = $("#popup_nonOp #tableDiv").data("kendoGrid");
		grid.table.on("click", ".checkbox" , function(){selectRow(grid,this)});
		
	};
	
	var prdComplArray = [];
	
	function leave2Work(){
		var url = "${ctxPath}/chart/addWorkTimeHist.do";
		var param = "worker=" + $("#popup2 .worker").val() + 
					"&ty=O";
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					saveRow_prdCompl();
					closePopup2()
				}else if("duple"){
					alert("이미 퇴근 처리 되었습니다.")
					closePopup2()
				}
			}
		});
		
		
	};
	
	function saveRow_prdCompl(){
		prdComplArray = [];
		$(".content").each(function(idx, data){
			var dvcId = data.id
			var tgCnt = $(data).children("td:nth(1)").html()
			var partCyl = $(data).children("td:nth(2)").html()
			var cntCyl = $(data).children("td:nth(3)").html()
			var type = $("#popup2 #nd").val();
			var date = $("#popup2 #date").val();
			var worker = $("#popup2 .worker").val();
			var workerCnt = $(data).children("td:nth(4)").children("input").val()
			var lot1 = $(data).children("td:nth(6)").children("input").val()
			var lot2 = $(data).children("td:nth(7)").children("input").val()
			var lot3 = $(data).children("td:nth(8)").children("input").val()
			var lot4 = $(data).children("td:nth(9)").children("input").val()
			var lot5 = $(data).children("td:nth(10)").children("input").val()
			
			
			var obj = {
				worker : worker,
				prdNo : $("#popup2 #prdNo").val(),	
				dvcId : dvcId,
				tgCnt : tgCnt,
				partCyl : partCyl,
				date : date,
				cntCyl : cntCyl,
				ty : type,
				workerCnt : workerCnt,
				lot1 : lot1,
				lot2 : lot2,
				lot3 : lot3,
				lot4 : lot4,
				lot5 : lot5
			};
			
			prdComplArray.push(obj);
		});
		
		var obj = {
				val : prdComplArray
		}
		
		var url = "${ctxPath}/chart/addPrdComplHist.do";
		var param = "val=" + JSON.stringify(obj);
		
		console.log(obj)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					alert("저장 되었습니다.")
					getPrdCmplData();
				}
			},error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	var deliver_obj = [];
	var update_deliver_obj = [];
	
	function saveRow_attendance(){
		if(typeof(selectedRow.id)=="undefined"){
			addRow_attendance();
			
			deliver_obj = [];
			
			var worker = $("#popup_attendance .worker").val();
			var attendanceTy = $("#popup_attendance #attendance_ty").val()
			var date = $("#popup_attendance #date").val()
			var attendanceDiv = $("#popup_attendance #attendance_div").val()
			var attendance = $("#popup_attendance #assiduity").val()
			var sDate = $("#popup_attendance #sTime").val()
			var eDate = $("#popup_attendance #eTime").val()
			var reason = $("#popup_attendance #reason").val()
			
			var object = {};

			//object.id = id;
			object.worker = worker;
			object.attendanceTy = attendanceTy;
			object.date = date;
			object.attendanceDiv = attendanceDiv;
			object.attendance = attendance;
			object.sDate = sDate;
			object.eDate = eDate;
			object.reason = reason;
				
			deliver_obj.push(object)
			
			/* deliver_obj = [];
			update_deliver_obj = [];
			
			var $checked = $('input[type=checkbox]:checked');
		
			if($checked.length==0){
				alert("항목을 선택하지 않으셨습니다.")
				return
			}
			
			$($checked).each(function(idx, data){
				var checked = data,
		        row = $(data).closest("tr"),
		        grid = $("#popup_attendance #tableDiv").data("kendoGrid"),
		        dataItem = grid.dataItem(row);
				var selectedItem = grid.dataItem(grid.select());
				
				var id = dataItem.id;
				
				var worker = dataItem.worker;
				var attendanceTy = dataItem.attendanceTy;
				var date = dataItem.date;
				var attendanceDiv = dataItem.attendanceDiv;
				var attendance = dataItem.attendance;
				var sDate = dataItem.sDate;
				var eDate = dataItem.eDate;
				var reason = dataItem.reason;
				
				var object = {};

				object.id = id;
				object.worker = worker;
				object.attendanceTy = attendanceTy;
				object.date = date;
				object.attendanceDiv = attendanceDiv;
				object.attendance = attendance;
				object.sDate = sDate;
				object.eDate = eDate;
				object.reason = reason;
				
				
				deliver_obj.push(object)
				
				if(id!=""){
					update_deliver_obj.push(object)
				}
			})
			
			
			
			if(update_deliver_obj.length!=0) {
				updateRow_attendance()
			}
			*/
			
			var obj = new Object();
			obj.val = deliver_obj;
		
			var url = "${ctxPath}/chart/addAttendanceHst.do";
			
			var param = "val=" + JSON.stringify(obj);
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					console.log(data)
					if(data=="success"){
						$("#popup_attendance #tableDiv").empty()
						addAttendance();
						isAddRow = false;
					}
				}
			});	  
		}else{
			var url = "${ctxPath}/chart/updateAttendance.do";
			var param = "id=" + selectedRow.id + 
						"&worker=" + $("#popup_attendance .worker").val() + 
						"&attendanceTy=" + $("#popup_attendance #attendance_ty").val() + 
						"&date=" + $("#popup_attendance #date").val() + 
						"&attendanceDiv=" + $("#popup_attendance #attendance_div").val() +
						"&attendance=" + $("#popup_attendance #assiduity").val() + 
						"&sDate=" + $("#popup_attendance #sTime").val() + 
						"&eDate=" + $("#popup_attendance #eTime").val() +
						"&reason=" + $("#popup_attendance #reason").val();
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					console.log(data)
					if(data=="success"){
						selectedRow = {}
						addAttendance();	
						isAddRow = false;
					}
				}
			});
		}
	};
	
	function saveRow(){
		if(typeof(selectedRow.id)=="undefined"){
			deliver_obj = [];
			//update_deliver_obj = [];
			
			addRow();
			
			var worker = $("#popup_nonOp .worker").val();
			var dvcId = $("#popup_nonOp #dvcId").val()
			var date = $("#popup_nonOp #date").val()
			var nonOpTy = $("#popup_nonOp #nonOpTy").val()
			var sDate = $("#popup_nonOp #sTime").val()
			var eDate = $("#popup_nonOp #eTime").val()
			var nonOpTime = $("#popup_nonOp #nonOpTime").val()
			
			var object = {};
	
			object.worker = worker;
			object.dvcId = dvcId;
			object.date = date;
			object.nonOpTy = nonOpTy;
			object.sDate = sDate;
			object.eDate = eDate;
			object.nonOpTime = Number(nonOpTime.substring(0,nonOpTime.lastIndexOf("시간")) * 60) + Number(nonOpTime.substring(nonOpTime.lastIndexOf(" ")+1, nonOpTime.lastIndexOf("분")));
			
			deliver_obj.push(object)

			/* var $checked = $('input[type=checkbox]:checked');
		
			//납품번호, 공급업체, 납품일자, 품번, 규격, 단위, 수량, 단가, 금액
			
			if($checked.length==0){
				alert("항목을 선택하지 않으셨습니다.")
				return
			}
			
			$($checked).each(function(idx, data){
				var checked = data,
		        row = $(data).closest("tr"),
		        grid = $("#popup_nonOp #tableDiv").data("kendoGrid"),
		        dataItem = grid.dataItem(row);
				var selectedItem = grid.dataItem(grid.select());
				
				var id = dataItem.id;
				
				var worker = dataItem.worker;
				var dvcId = dataItem.dvcId;
				var date = dataItem.date
				var nonOpTy = dataItem.nonOpTy;
				var sDate = dataItem.sDate;
				var eDate = dataItem.eDate;
				var nonOpTime = dataItem.nonOpTime;
				
				var object = {};

				object.id = id;
				object.worker = worker;
				object.dvcId = dvcId;
				object.date = date;
				object.nonOpTy = nonOpTy;
				object.sDate = sDate;
				object.eDate = eDate;
				//문자 파싱
				object.nonOpTime = Number(nonOpTime.substring(0,nonOpTime.lastIndexOf("시간")) * 60) + Number(nonOpTime.substring(nonOpTime.lastIndexOf(" ")+1, nonOpTime.lastIndexOf("분")));
				
				
				deliver_obj.push(object)
				
				if(id!=""){
					update_deliver_obj.push(object)
				}
			})
		
			if(update_deliver_obj.length!=0) {
				updateRow()
			}; */
			
			var obj = new Object();
			obj.val = deliver_obj;
		
			var url = "${ctxPath}/chart/addNonOpHst.do";
			
			var param = "val=" + JSON.stringify(obj);
			
			 $.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					console.log(data)
					if(data=="success"){
						$("#popup_nonOp #tableDiv").empty()
						addNonOp();
						isAddRow = false;
					}
				}
			});	 			
		}else{
			var nonOpTime = $("#popup_nonOp #nonOpTime").val()
			var url = "${ctxPath}/chart/updateNonOpHist.do";
			var param = "id=" + selectedRow.id + 
						"&worker=" + $("#popup_nonOp .worker").val() + 
						"&dvcId=" + $("#popup_nonOp #dvcId").val() + 
						"&date=" + $("#popup_nonOp #date").val() + 
						"&nonOpTy=" + $("#popup_nonOp #nonOpTy").val() +
						"&sDate=" + $("#popup_nonOp #sTime").val()  + 
						"&eDate=" + $("#popup_nonOp #eTime").val() + 
						"&nonOpTime=" + Number(Number(nonOpTime.substring(0,nonOpTime.lastIndexOf("시간")) * 60) + Number(nonOpTime.substring(nonOpTime.lastIndexOf(" ")+1, nonOpTime.lastIndexOf("분"))));
						
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					console.log(data)
					if(data=="success"){
						selectedRow = {}
						addNonOp();	
						isAddRow = false;
					}
				}
			});
		}
		
	};
	
	function updateRow_attendance(){
		var obj = new Object();
		obj.val = update_deliver_obj;
	
		var url = "${ctxPath}/chart/updateAttendance.do";
		
		var param = "val=" + JSON.stringify(obj);
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data
			}
		});
		
		return str;
	};
	
	function updateRow(){
		var obj = new Object();
		obj.val = update_deliver_obj;
	
		var url = "${ctxPath}/chart/updateNonOpHist.do";
		
		var param = "val=" + JSON.stringify(obj);
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data
				console.log(str)
			}
		});
		
		return str;
	};
	
	
	function addRow_attendance(){
		isAddRow = true;
		checkedIds = {}
		grid_attendance.dataSource.add({
			"선택" : "<input type='checkbox' class='checkbox' />",
			"worker" : $("#popup_attendance .worker option:selected").val(),
			"attendanceTy" : $("#popup_attendance #attendance_ty").val(),
			"date" : $("#popup_attendance #date").val(),
			"attendanceDiv" : $("#popup_attendance #attendance_div").val(),
			"attendance" : $("#popup_attendance #assiduity").val(),
			"sDate" : $("#popup_attendance #sTime").val(),
			"eDate" : $("#popup_attendance #eTime").val(),
			"reason" : $("#popup_attendance #reason").val()
		});	
		
		
		$(".k-button").css({
			"padding" : getElSize(10)
		})	
	};
	
	var isAddRow = false;
	function addRow(){
		isAddRow = true;
		checkedIds = {}
		grid.dataSource.add({
			"선택" : "<input type='checkbox' class='checkbox' />",
			"worker" : $("#popup_nonOp .worker option:selected").val(),
			"dvcId" : $("#popup_nonOp #dvcId").val(),
			"date" : $("#popup_nonOp #date").val(),
			"nonOpTy" : $("#popup_nonOp #nonOpTy").val() ,
			"sDate" : $("#popup_nonOp #sDate").val() + " " + $("#popup_nonOp #sTime").val(),
			"eDate" : $("#popup_nonOp #eDate").val() + " " + $("#popup_nonOp #eTime").val(),
			"nonOpTime" : $("#popup_nonOp #nonOpTime").val()
		});	
		
		
		$(".k-button").css({
			"padding" : getElSize(10)
		})
	};
	
	function login_worker(){
		var url = "${ctxPath}/chart/login_worker.do";
		var param = "id=" + $("#popup .worker").val() +
					"&pwd=" + $("#popup #pwd").val();
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data;
			}
		}); 
		
		return str;
	};
	
	function del_btn_evt(e, el, cd, $grid){
		e.preventDefault();
		var tr = $(e.target).closest("tr");
		var data = el.dataItem(tr); //get the row data so it can be referred later
		delMsg.content(windowTemplate(data)); //send the row data object to the template and render it
		delMsg.center().open();

		var grid = $grid;
	    var dataItem = grid.dataItem(tr);
		var id = dataItem.id 
		 
	    $("#yesButton").click(function(){
	    	eval(cd)
	        grid.dataSource.remove(data)  //prepare a "destroy" request
	        grid.dataSource.sync()  //actually send the request (might be ommited if the autoSync option is enabled in the dataSource)
	        delMsg.close();
	        $(".k-button").css({
				"padding" : 10
			})
	    })
	    $("#noButton").click(function(){
	    	delMsg.close();
	    }) 
	}

	
	function delNonOpHistory(id){
		var url = "${ctxPath}/chart/delNonOpHistory.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
			}
		});
	};
	
	function delAttendanceHistory(id){
		var url = "${ctxPath}/chart/delAttendanceHistory.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
			}
		});
	};
	
	function dvcName(dvcId) {
		for (var i = 0; i < dvcArray.length; i++) {
			if (dvcArray[i].dvcId == dvcId) {
				return dvcArray[i].dvcName;
			}
	    }
	}
	
	
	function workerName(workerCd) {
		for (var i = 0; i < workerArray.length; i++) {
			if (workerArray[i].workerCd == workerCd) {
				return workerArray[i].name;
			}
	    }
	}
	
	function nonOpTyName(nonOpTy) {
		for (var i = 0; i < nonOpTyArray.length; i++) {
			if (nonOpTyArray[i].nonOpTy == nonOpTy) {
				return nonOpTyArray[i].nonOpName;
			}
	    }
	}
	
	function attendanceTyName(attendanceTy) {
		for (var i = 0; i < attendanceTyArray.length; i++) {
			if (attendanceTyArray[i].attendanceTy == attendanceTy) {
				return attendanceTyArray[i].attendanceTyName;
			}
	    }
	}
	
	function attendanceDivName(attendanceDiv) {
		for (var i = 0; i < attendanceDivArray.length; i++) {
			if (attendanceDivArray[i].attendanceDiv == attendanceDiv) {
				return attendanceDivArray[i].attendanceDivName;
			}
	    }
	}
	
	function attendanceName(attendance) {
		for (var i = 0; i < attendanceArray.length; i++) {
			if (attendanceArray[i].attendance == attendance) {
				return attendanceArray[i].attendanceName;
			}
	    }
	}
	
	function getDvcList(){
		var url = "${ctxPath}/chart/getDvcId.do";
		var param = "shopId=" + shopId + 
					"&line=ALL";
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
					
					var obj = {
							"dvcId" : data.dvcId,
							"dvcName" : decode(data.name)
					}
					
					dvcArray.push(obj)
				});
				$("#popup_nonOp #dvcId").html(options);
			}
		});
	};
	
	function getNonOpTy(){
		var url = ctxPath + "/chart/getNonOpTy.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = ""
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.nonOpTy).replace(/\+/gi, " ") + "</option>";
					var obj = {
							"nonOpTy" : data.id,
							"nonOpName" : decode(data.nonOpTy)
					}
					
					nonOpTyArray.push(obj)
				});
				
				$("#popup_nonOp #nonOpTy").html(options);
			}	
		})
	}
	
	var workerArray = [];
	var dvcArray = [];
	var nonOpTyArray = [];
	var attendanceTyArray = [];
	var attendanceDivArray = [];
	var attendanceArray = [];
	
	function categoryDropDownAttendanceTyNameEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "attendanceTyName",
			 dataValueField: "attendanceTy",
			 dataSource: attendanceTyArray
		});	
	}
	
	function categoryDropDownAttendanceDivNameEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "attendanceDivName",
			 dataValueField: "attendanceDiv",
			 dataSource: attendanceDivArray
		});	
	}
	
	function categoryDropDownAttendanceEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "attendanceName",
			 dataValueField: "attendance",
			 dataSource: attendanceArray
		});	
	}
	
	
	function categoryDropDownWorkerNameEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "name",
			 dataValueField: "workerCd",
			 dataSource: workerArray
		});
 	}
	
	function categoryDropDownDvcNameEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "dvcName",
			 dataValueField: "dvcId",
			 dataSource: dvcArray
		});
  	}
	
	function categoryDropDownNonOpTyEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "nonOpName",
			 dataValueField: "nonOpTy",
			 dataSource: nonOpTyArray
		});
  }
	
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#popup_nonOp #sDate").val() + "," + $("#popup_nonOp #sTime").val());
		var endTime = new Date($("#popup_nonOp #eDate").val() + "," + $("#popup_nonOp #eTime").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return parseInt(timeDiff/60) + "시간 " + parseInt(timeDiff%60) + "분";
	};
	
	var dayTime = 510; //08:30
	var nightTime = 1230; //20:30

	function chkWorkeTimeValid(){
		var hour = Number(new Date().getHours()) * 60;
		var minute = Number(new Date().getMinutes());
		var current = hour + minute;
		
		var min = 120;
		var max = 360;
		
		var str;
		
		if((dayTime - min) <= current && (dayTime + max) >= current){
			str = "success";	
			console.log("주간")
		}else if((nightTime - min) <= current && (24*60) >= current){
			str = "success";	
			console.log("야간")	
		}else{
			console.log("no check")
			str = "fail";
		}
	
		//02:30
		if(150 >= current){
			console.log("야간")	
			str = "success";	
		}	
		
		return str;
	}
	
	function clickOk(){
		selectedRow = {}
		$(".worker").val($("#popup .worker").val())
		if(login_worker()=="success"){
			if(cMenu == "report2work"){//출근보고
				addStartWork();
			}else if(cMenu == "report2leave_work"){//퇴근보고
				addPrdCmpl();
				$("#popup2 #table td").css({
					"font-size" : getElSize(80) + "px",
					"text-align" : "center",
					"border" : getElSize(10) + "px solid black"
				})
			}else if(cMenu=="report2Attendance"){//근태보고 보고
				addAttendance();
			}else if(cMenu=="report2NonOp"){//비가동 보고
				addNonOp();
			}else if(cMenu=="test"){//자주검사
				location.href = "${ctxPath}/chart/checkPrdct.do?worker=" + $("#popup .worker").val()
			}else if(cMenu="Report2Faulty"){//불량신고
				location.href = "${ctxPath}/chart/addFaulty.do?worker=" + $("#popup .worker").val()
			}	
		}else{
			alert("패스워드가 일치하지 않습니다.")
			$("#pwd").focus();
		}
	};
	
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>";
				});
				
				$("#popup2 #prdNo").html(option).change(getOprNameList);
				getOprNameList();
			}
		});
	};
	
	function getOprNameList(){
		var url = ctxPath + "/chart/getOprNameList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $("#popup2 #prdNo").val(); 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				if(json.length==0) options += "<option>공정 없음</option>";
				
				$(json).each(function(idx, data){
					var oprNo;
					if(data.name=="0010"){
						oprNo = "R삭";
					}else if(data.name=="0020"){
						oprNo = "MCT";
					}else if(data.name=="0030"){
						oprNo = "CNC";
					}
					options += "<option value='" + data.name + "'>" + oprNo + "</option>";
				});
				
				$("#popup2 #operation").html(options).change(getPrdCmplData);
				getPrdCmplData();
			}
		});	
	};
	
	function getPrdCmplData(){
		var url = "${ctxPath}/chart/setPrdCmplData.do";
		var param = "ty=" + $("#popup2 #nd").val() + 
					"&date=" + $("#popup2 #date").val() + 
					"&oprNo=" + $("#popup2 #operation").val() + 
					"&prdNo=" + $("#popup2 #prdNo").val();
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				var tr = "";
				$(".content").remove();
				$(json).each(function(idx, data){
					tr += "<tr class='content' id='" + data.dvcId + "'>" + 
							"<Td>" + decode(data.name) + "</td>" +
							"<Td>" + data.tgCnt + "</td>" +
							"<Td>" + data.partCyl + "</td>" +
							"<Td>" + data.cntCyl + "</td>" +
							"<Td><input type='text' size='4' value='" + data.workerCnt + "'></td>" +
							"<Td>" + Math.round(Number(data.workerCnt) / Number(data.tgCnt) * 100) + "</td>" +
							"<Td><input type='text' size='4' value='" + data.lot1 +"'></td>" +
							"<Td><input type='text' size='4' value='" + data.lot2 +"'></td>" +
							"<Td><input type='text' size='4' value='" + data.lot3 +"'></td>" +
							"<Td><input type='text' size='4' value='" + data.lot4 +"'></td>" +
							"<Td><input type='text' size='4' value='" + data.lot5 +"'></td>" +
						"</tr>";
				});
				
				$("#popup2 #table").append(tr);
				$("#popup2 #table td, #popup2 #table td input").css({
					"font-size" : getElSize(80) + "px",
					"text-align" : "center",
				})				
				$("#popup2 #table td").css({
					"border" : getElSize(10) + "px solid black",
					"color" : "white"
				})				
			}
		});
	};
	
	function showPopup(el){
		$("#pwd").val("");
		var id = el.id;
		cMenu = id;
		if(id=="report2work"){//출근보고
			$("#popup").css({
				"display" : "inline"
			});
		
			$("#okBtn").html("출근")
			
		}else if(id=="report2leave_work"){//퇴근보고
			getPrdNo();
			$("#popup").css({
				"display" : "inline"
			});
		
			$("#okBtn").html("작업보고");
			$("#date").val(getToday().substr(0,10))
			
		}else if(id=="report2Attendance"){//근태보고
			getAttendanceTy();
			
			$("#popup").css({
				"display" : "inline"
			});
		
			$("#okBtn").html("${report2Attendance}");
		}else if(id=="report2NonOp"){//비가동보고
			getDvcList();
			getNonOpTy();
			$("#popup").css({
				"display" : "inline"
			});
			$("#okBtn").html("${confirm}");
			
			
		}else if(id=="test"){//자주검사
			$("#popup").css({
				"display" : "inline"
			});
			$("#okBtn").html("${confirm}");
		}else if(id=="Report2Faulty"){//불량신고
			$("#popup").css({
				"display" : "inline"
			});
			$("#okBtn").html("${confirm}");
		}
	}

	function getAttendanceTy(){
		var url = "${ctxPath}/chart/getCdGroup1.do";
		
		$.ajax({
			url : url,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.attendanceTy + "'>" + decode(data.attendanceTyName) + "</option>";
					var object = {
							"attendanceTy" : data.attendanceTy
							,"attendanceTyName" : decode(data.attendanceTyName)
					}
					
					attendanceTyArray.push(object)
				});
				
				$("#popup_attendance #attendance_ty").html(options).change(getAttendanceDiv);
				getAttendanceDiv();
			}
		});
	};
	
	function getAttendance(){
		var url = "${ctxPath}/chart/getCdGroup3.do";
		var param = "group2=" + $("#popup_attendance #attendance_div").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			async : false,
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.attendance + "'>" + decode(data.attendanceName) + "</option>";
					var object = {
							"attendance" : data.attendance
							,"attendanceName" : decode(data.attendanceName)
					}
					
					attendanceArray.push(object)
				});
				
				$("#popup_attendance #assiduity").html(options);
			}
		});
	};
	
	function getAttendanceDiv(){
		var url = "${ctxPath}/chart/getCdGroup2.do";
		var param = "group1=" + $("#popup_attendance #attendance_ty").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			async : false,
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.attendanceDiv + "'>" + decode(data.attendanceDivName) + "</option>";
					var object = {
							"attendanceDiv" : data.attendanceDiv
							,"attendanceDivName" : decode(data.attendanceDivName)
					}
					
					attendanceDivArray.push(object)
				});
				
				$("#popup_attendance #attendance_div").html(options).change(getAttendance);;
				getAttendance();
			}
		});
	};
	
	function closePopup(){
		$("#popup").css({
			"display" : "none"
		});
	}
	
	function closePopup2(){
		$("#popup2").css({
			"display" : "none"
		});
	}
	
	function closePopup_nonOp(){
		if(isAddRow){
			if(confirm("입력한 내용을 무시하시겠습니까?")){
				$("#popup_nonOp, #popup_attendance").css({
					"display" : "none"
				});
				isAddRow = false;
			}else{
				return;
			}	
		}else{
			$("#popup_nonOp, #popup_attendance").css({
				"display" : "none"
			});
		}
	};
	
	function onKeyEvt(evt){
		if(evt.keyCode==13) clickOk();
	};

</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
<div id="window"></div>
	<div id="popup_attendance">
		<center>
			<table style="width: 95%">
				<tr>
					<td><spring:message code="worker"></spring:message></td>
					<td><select class="worker"></select></td>
					<td><spring:message code="attendance_ty"></spring:message></td>
					<td><select id="attendance_ty"></select></td>
					<td><spring:message code="date_"></spring:message></td>
					<td><input type="date" id="date"></select></td>
				</tr>
				<tr>
					<td><spring:message code="attendace_div"></spring:message></td>
					<td><select id="attendance_div"></select></td>
					<td><spring:message code="assiduity"></spring:message></td>
					<td><select id="assiduity"></select></td>
					<td><spring:message code="start"></spring:message></td>
					<td><input type="time" id="sTime"></td>
				</tr>
				<tr>
					<Td><spring:message code="reason"></spring:message> </Td>
					<Td colspan="3"><input id="reason"></Td>
					<td><spring:message code="end"></spring:message></td>
					<td><input type="time" id="eTime"></td>
				</tr>
			</table>
			
			<div id="tableDiv">
			
			</div>
			
			<%-- <button onclick="addRow_attendance()"><spring:message code="add"></spring:message></button> --%>
			<button onclick="init()"><spring:message code="init"></spring:message></button>
			<button onclick="saveRow_attendance();"><spring:message code="save"></spring:message></button>
			<button onclick="closePopup_nonOp();"><spring:message code="cancel"></spring:message></button>
		</center>
	</div>
	
	<div id="popup_nonOp">
		<center>
			<table style="width: 95%">
				<tr>
					<td><spring:message code="worker"></spring:message></td>
					<td><select class="worker"></select></td>
					<td><spring:message code="date_"></spring:message></td>
					<td><input type="date" id="date"></select></td>
					<td><spring:message code="device"></spring:message></td>
					<td><select id="dvcId"></select></td>
				</tr>
				<tr>
					<td><spring:message code="non_operation_ty"></spring:message></td>
					<td><select id="nonOpTy"></select></td>
					<td><spring:message code="time_"></spring:message></td>
					<td><input id="nonOpTime"></td>
					<td><spring:message code="start"></spring:message></td>
					<td><input type="date" id="sDate" class="date"> <input type="time" id="sTime"></td>
				</tr>
				<tr>
					<Td><spring:message code="bigo"></spring:message> </Td>
					<Td colspan="3"><input id="bigo"></Td>
					<td><spring:message code="end"></spring:message></td>
					<td><input type="date" id="eDate" class="date"> <input type="time" id="eTime"></td>
				</tr>
			</table>
			
			<div id="tableDiv">
			
			</div>
			
			<%-- <button onclick="addRow()"><spring:message code="add"></spring:message></button> --%>
			<button onclick="init()"><spring:message code="init"></spring:message></button>
			<button onclick="saveRow();"><spring:message code="save"></spring:message></button>
			<button onclick="closePopup_nonOp();"><spring:message code="cancel"></spring:message></button>
		</center>
	</div>
	
	<div id="popup2">
		<center>
			<table style="width: 95%">
				<tr>
					<td><spring:message code="prdct_machine_line"></spring:message></td>
					<td><select id="prdNo"></select></td>
					<td><spring:message code="operation"></spring:message></td>
					<td><select id="operation"></select></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><spring:message code="date_"></spring:message></td>
					<td><input type="date" id="date"/></td>
					<td><spring:message code="division"></spring:message></td>
					<td><select id="nd" onchange="getPrdCmplData()">
						<option value="2"><spring:message code="day"></spring:message> </option>
						<option value="1"><spring:message code="night"></spring:message> </option>
					</select></td>
					<td><spring:message code="worker"></spring:message></td>
					<td><select class="worker"></select></td>
				</tr>
			</table>
			
			<div id="tableDiv">
				<table id="table" style="width: 95%; border-collapse: collapse;">
					<Tr>
						<Td rowspan="2">
							<spring:message code="device"></spring:message>
						</Td>
						<Td rowspan="2">
							<spring:message code="plan"></spring:message>
						</Td>
						<Td colspan="3">
							<spring:message code="prdct_performance"></spring:message>
						</Td>
						<Td rowspan="2">
							<spring:message code="target_ratio"></spring:message><br>(%)
						</Td>
						<td colspan="5">
							LOT Number
						</td>
					</Tr>
					<tr>
						<td>
							HMI
						</td>
						<td>
							aIdoo
						</td>
						<Td>
							<spring:message code="worker"></spring:message>
						</Td>
						<td>
							#1
						</td>
						<td>
							#2
						</td>
						<td>
							#3
						</td>
						<td>
							#4
						</td>
						<td>
							#5
						</td>
					</tr>
				</table>
			</div>
			
			<button onclick="leave2Work();"><spring:message code="leave2Work"></spring:message></button>
			<button onclick="saveRow_prdCompl()"><spring:message code="save"></spring:message></button>
			<button onclick="closePopup2();"><spring:message code="cancel"></spring:message></button>
		</center>
	</div>
	<div id="popup">
		<Table>
			<Tr>
				<td>
					<spring:message code="worker"></spring:message>
				</td>
				<td>
					<select class="worker"></select>
				</td>
			</Tr>
			<Tr>
				<td>
					<spring:message code="pwd"></spring:message> 
				</td>
				<td>
					<input type="password" id="pwd" onkeyup="onKeyEvt(event)">
				</td>
			</Tr>
			<tr>
				<Td colspan="2" style="text-align: center;">
					<button id="okBtn" onclick="clickOk()"><spring:message code="go2Work"></spring:message></button>
					<button onclick="closePopup()"><spring:message code="cancel"></spring:message> </button>		
				</Td>
			</tr>
		</Table>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="_table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="report2work" class="sub_menu"><span><spring:message code="report2work"></spring:message></span> </div>
					<div id="report2leave_work" class="sub_menu"><span><spring:message code="report2leave_work"></spring:message></span> </div>
					<div id="report2Attendance" class="sub_menu"><span><spring:message code="report2Attendance"></spring:message></span> </div>
					<div id="report2NonOp" class="sub_menu"><span><spring:message code="report2NonOp"></spring:message></span> </div>
					<div id="test" class="sub_menu"><span><spring:message code="test"></spring:message></span> </div>
					<div id="Report2Faulty" class="sub_menu"><span><spring:message code="Report2Faulty"></spring:message></span> </div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	