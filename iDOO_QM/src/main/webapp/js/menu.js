$(()=>{
    setEl()
    bindEvt()
})

const bindEvt = () =>{

}

const gt_cat = new JqMap()

gt_cat.put("1", "1-1. 출근 보고")
gt_cat.put("2", "1-2. 퇴근 보고")
gt_cat.put("3", "1-3. 조퇴 보고")
gt_cat.put("4", "1-4. 외출 보고")

let is_select_menut = false;
const selectMenu = el =>{
    let id = ""

    if(el.target.tagName.toLowerCase() == "div"){
        id = el.target.id
    }else{
        id = $(el.target).parent("div").attr("id")
    }

    $("#" + id).children("span").hide()

    if(!is_select_menut){   //메뉴 선택
        $(".menu").not("#" + id).hide()

        const $selected_item = $("#" + id)

        $selected_item.css({
            "transition" : "0s",
            "position" : "absolute",
            "left" : el.screenX,
            "top" : 0
        });

        $selected_item.css({
            "transition" : "1s",

            "width" : getElSize(400) + "px",
            "font-size" : getElSize(70) + "px",
            "height" : $("#header").height() * 0.6,
            "padding-top" : "0px",
            "top" : getElSize(20) + "px",
            "left" : (width / 2) - (getElSize(400) / 2),
            "margin" : 0
        })

        $selected_item.children("img").hide()

        if(id == "gt"){ // 근태 관리
            drawGtIcon()
        }else if(id == "operation"){ // 작업 관리
            console.log("작업 관리")

        }else if(id == "history"){ // 이력 관리
        	console.log("이력 조회")
            drawHistoryTable()
        }

    }else{  // 메뉴 초기화
        $(".menu").css("position1", "static");
        $(".gt_sub_menu, #historyWrapper").remove()
        $(".menu_icon").show()
        $(".menu").children("span").show()
        setEl();
    }

    is_select_menut = !is_select_menut
}

const drawHistoryTable = () => {
    const wrapper = document.createElement("div")
    wrapper.setAttribute("id", "historyWrapper")
    wrapper.style.cssText =
        `
            width : ${width * 0.95}px
            ;height : ${height - $("#header").height() - getElSize(100)}px
        `

    // 근태 관리
    const leftSide = document.createElement("div")
    leftSide.setAttribute("id", "leftSide")
    leftSide.style.cssText =
        `
            width : ${$(wrapper).width() * 0.49}px
            ; height : ${$(wrapper).height() * 0.9}px
            ; text-align : center
            ; float : left
            ; border-right : ${getElSize(3)}px solid black
        `

    wrapper.append(leftSide)

    const gt_title = document.createElement("span")
    gt_title.append(document.createTextNode("근태 이력"))

    gt_title.style.cssText =
        `
            font-size : ${getElSize(50)}px
            ; display : inline-block
            ; margin-bottom : ${getElSize(50)}px
        `

    leftSide.append(gt_title)

    const gt_table =
        `
            <table id='gt_table' class='mainTable' style="width: 95%; margin: 0 auto;">
                <thead>
                    <Tr>
                        <Td>날짜</Td>
                        <Td style="width : 20%">출근</Td>
                        <Td style="width : 20%">퇴근</Td>
                        <Td style="width : 20%">조퇴</Td>
                        <Td style="width : 20%">외출</Td>
                    </Tr>
                </thead>
                <tbody>
                       <Tr>
                            <td>2019-03-26</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-25</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-24</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-23</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td>10:23:43</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-22</td> 
                            <td>09:00:12</td>
                            <td>-</td>
                            <td>13:23:42</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-21</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-20</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-19</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td></td>
                       </Tr>
                       <Tr>
                            <td>2019-03-18</td> 
                            <td>09:00:12</td>
                            <td>18:02:32</td>
                            <td>-</td>
                            <td>10:23:43</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-17</td> 
                            <td>09:00:12</td>
                            <td>-</td>
                            <td>13:23:42</td>
                            <td></td>
                       </Tr>
                </tbody>
            </table>
            
            <ul class="pagination" id="pagination1"></ul>
        `
    leftSide.insertAdjacentHTML("beforeend", gt_table)

    // 작업 관리
    const rightSide = document.createElement("div")
    rightSide.setAttribute("id", "rightSide")
    rightSide.style.cssText =
        `
            width : ${$(wrapper).width() * 0.49}px
            ; text-align : center
            ; height : ${$(wrapper).height() * 0.9}px
            ; float : left
        `

    wrapper.append(rightSide)

    const operation_title = document.createElement("span")
    operation_title.append(document.createTextNode("작업 이력"))

    operation_title.style.cssText =
        `
            font-size : ${getElSize(50)}px
            ; display : inline-block
            ; margin-bottom : ${getElSize(50)}px
        `

    rightSide.append(operation_title)

    const operation_table =
        `
            <table id='gt_table' class='mainTable' style="width: 95%; margin: 0 auto;">
                <thead>
                    <Tr>
                        <Td>날짜</Td>
                        <Td style="width : 20%">라인</Td>
                        <Td style="width : 20%">장비</Td>
                        <Td style="width : 20%">목표 수량</Td>
                        <Td style="width : 20%">작업 수량</Td>
                    </Tr>
                </thead>
                <tbody>
                       <Tr>
                            <td>2019-03-26</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #90EE9C">105</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-25</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #FA828A;">80</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-24</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #90EE9C">101</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-23</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #FA828A;">98</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-22</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #FA828A;">90</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-21</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #90EE9C">105</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-20</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #FA828A;">80</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-19</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #90EE9C">101</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-18</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #FA828A;">98</td>
                       </Tr>
                       <Tr>
                            <td>2019-03-17</td> 
                            <td>LFA RR</td>
                            <td>C#1</td>
                            <td>100</td>
                            <td style="background-color: #FA828A;">90</td>
                       </Tr>
                </tbody>
            </table>
            <ul class="pagination" id="pagination2"></ul>
        `
    rightSide.insertAdjacentHTML("beforeend", operation_table)



    $(wrapper).css({
        "margin-left" : (width / 2) - ($(wrapper).width() / 2)
    })

    $("body").append(wrapper)

    //unos table temaplate
    setTableFormat()

    $("#leftSide, #rightSide").css({
        "margin-top" : ($(wrapper).height() / 2) - ($(leftSide).height() / 2)
    })

    //pager
    setPaging("pagination1",1, ()=>{})
    setPaging("pagination2",1, ()=>{})
}
const drawGtIcon = () =>{
    $(gt_cat.keys()).each((idx, data) =>{
        const id = data;
        const name = gt_cat.get(data)

        const div = document.createElement("div")
        div.setAttribute("id", "gt_" + data)
        div.setAttribute("class", "gt_sub_menu")
        div.append(document.createTextNode(name))

        let bgColor = ""
        if(data == 1){
            bgColor = "rgb(102, 117, 206)"
        }else if(data == 2){
            bgColor = "rgb(185, 56, 79)"
        }else if(data == 3){
            bgColor = "#DFD002"
        }else if(data == 4){
            bgColor = "rgb(118, 182, 188)"
        }

        div.style.cssText =
            `
                font-size : ${getElSize(170)}px
                ; border : ${getElSize(7)}px solid white
                ; height : ${getElSize(600)}px
                ; width : ${getElSize(1200)}px
                ; opacity : 0
                ; vertical : middle
                ; cursor : pointer
                ; padding-top : ${getElSize(50)}px
                ; transition : 1s
                ; margin : ${getElSize(170)}px
                ; border-radius : ${getElSize(30)}px
                ; display : inline-block
                ; color : white
                ; background-color : ${bgColor}
            `

        $("table tr td").append(div)

        $(div).click(selectGtSubMenu)
    })

    $(".gt_sub_menu").animate({
        "opacity" : 1
    },100)
}

const selectGtSubMenu = el =>{
    const $box = $("#" + el.target.id)

    if(el.target.id == "gt_1"){ // 출근 보고
        const now = getNow().substr(11)

        const span = document.createElement("span")
        span.append(document.createTextNode(now))
        span.style.cssText =
            `
                text-align : center
                ; color : lightgray
                ; position : fixed
                ; font-size : ${getElSize(80)}px
            `
        $box.append(span)

        $(span).css({
            "top" : ($box.offset().top + $box.height() - getElSize(150)) + "px",
            "left" : ($box.offset().left + $box.width() - ($box.width() / 2) - ($(span).width() / 2))
        })

        $box.css({
            "background-color" : "black"
        })

    }else if(el.target.id == "gt_2"){ // 퇴근 보고
        const now = getNow().substr(11)

        const span = document.createElement("span")
        span.append(document.createTextNode("오늘 하루도 수고 하셨습니다."))
        span.style.cssText =
            `
                text-align : center
                ; color : lightgray
                ; position : absolute
                ; font-size : ${getElSize(80)}px
            `
        $box.append(span)

        $(span).css({
            "top" : ($box.offset().top + $box.height() - getElSize(150)) + "px",
            "left" : ($box.offset().left + $box.width() - ($box.width() / 2))
        })

        $(span).css({
            "top" : ($box.offset().top + $box.height() - getElSize(150)) + "px",
            "left" : ($box.offset().left + $box.width() - ($box.width() / 2) - ($(span).width() / 2))
        })

        $box.css({
            "background-color" : "gray"
        })

    }else if(el.target.id == "gt_3") { // 조퇴 보고
        popup_submenu(el.target.id)

    }else if(el.target.id == "gt_4") { // 외출 보고
        popup_submenu(el.target.id)

    }
    // click event unbind
    $box.off("click")
}

const select_gt_3_button = (clone, origin, ty) =>{
    const reason = $("#gt_3_reason :selected").html()

    clone.css({
        "transition" : "0s",
    })

    //팝업 창 제자리로
    clone.animate({
        "left" : (origin.offset().left - getElSize(170)),
        "top" : (origin.offset().top - getElSize(170))
    }, ()=>{
        clone.remove();
        origin.css({
            "opacity" : 1
        })

        if(ty == "ok"){
            const now = getNow().substr(11)
            const span = document.createElement("span")
            span.append(document.createTextNode(now))
            span.style.cssText =
                `
                text-align : center
                ; color : lightgray
                ; position : fixed
                ; font-size : ${getElSize(80)}px
            `
            origin.append(span)

            const reason_span = document.createElement("span")
            reason_span.append(document.createTextNode(reason))
            reason_span.style.cssText =
                `
                text-align : center
                ; color : lightgray
                ; position : fixed
                ; font-size : ${getElSize(80)}px
            `
            origin.append(reason_span)

            $(reason_span).css({
                "top" : (origin.offset().top + origin.height() - getElSize(250)) + "px",
                "left" : (origin.offset().left + origin.width() - (origin.width() / 2) - ($(reason_span).width() / 2))
            })

            $(span).css({
                "top" : (origin.offset().top + origin.height() - getElSize(150)) + "px",
                "left" : (origin.offset().left + origin.width() - (origin.width() / 2) - ($(span).width() / 2))
            })

            origin.css({
                "background-color" : "white"
            })
        }else{
            // event rebinding
            origin.click(selectGtSubMenu)
        }
    });
}

const popup_submenu = (selected_menu_id) =>{
    showCorver()

    const $selected_item = $("#" + selected_menu_id)

    const clone = $selected_item.clone().appendTo("table tr td")

    $($selected_item).css({
        "transition" : "0s",
        "opacity" : 0
    })

    $selected_item.off("click")

    clone.css({
        "position" : "absolute",
        "z-index" : 10,
        "transition" : "0s",
        "left" : $selected_item.offset().left,
        "top" : $selected_item.offset().top
    });

    clone.css({
        "transition" : "1s",
        "left" : (width / 2) - ($(clone).width() / 2) - getElSize(170),
        "top" : (height / 2) - ($(clone).width() / 2)
    });

    clone.append(document.createElement("br"))

    const selection =
        `
                <select id='gt_3_reason'>
                    <option value>사유</option>
                    <option value>병가</option>
                    <option value>개인 사정</option>
                    <option value>기타</option>
                </select>           
            `

    clone.append(selection)
    setSelectFormat()

    // 컨트롤 wrapper
    const wrapper = document.createElement("div")
    clone.append(wrapper)

    // 컨트롤 버튼
    const ok = document.createElement("div")
    ok.setAttribute("id", "ok")
    ok.append(document.createTextNode("확인"))

    ok.style.cssText =
        `
                width : ${getElSize(300)}px
                ; height : ${getElSize(80)}px
                ; background-color : lightgray
                ; border-radius : ${getElSize(20)}px
                ; cursor : pointer
                ; margin-top : ${getElSize(50)}px
                ; padding-top : ${getElSize(20)}px
                ; margin-left: ${getElSize(250)}px
                ; text-align: center
                ; float : left
                ; font-size : ${getElSize(50)}px 
            `

    wrapper.append(ok)

    const cancel = document.createElement("div")
    cancel.setAttribute("id", "cancel")
    cancel.append(document.createTextNode("취소"))

    cancel.style.cssText =
        `
                width : ${getElSize(300)}px
                ; height : ${getElSize(80)}px
                ; background-color : lightgray
                ; border-radius : ${getElSize(20)}px
                ; cursor : pointer
                ; margin-top : ${getElSize(50)}px
                ; padding-top : ${getElSize(20)}px
                ; margin-right: ${getElSize(250)}px
                ; text-align: center
                ; float : right
                ; font-size : ${getElSize(50)}px 
            `

    wrapper.append(cancel)

    $(ok).hover(function(){
        $(this).css({
            "background-color" : "red"
        })
    }, function(){
        $(this).css({
            "background-color" : "lightgray"
        })
    });

    $(cancel).hover(function(){
        $(this).css({
            "background-color" : "red"
        })
    }, function(){
        $(this).css({
            "background-color" : "lightgray"
        })
    });

    $("#ok, #cancel").click((evt)=>{
        hideCorver()

        select_gt_3_button(clone, $selected_item, evt.target.id)
    })
}
const setEl = () =>{
    $(".menu span").css({
        "font-size" : getElSize(50) + "px"
    })

    $(".menu").css({
        "font-size" : getElSize(150) + "px",
        "border" : getElSize(7) + "px solid white",
        "height" : getElSize(600) + "px",
        "width" : getElSize(1100) + "px",
        "vertical-align" : "middle",
        "cursor" : "pointer",
        "padding-top" : getElSize(50) + "px",
        "transition" : "1s",
        "margin" : getElSize(50) + "px",
        "margin-top" : getElSize(550) + "px",
        "border-radius" : getElSize(30) + "px",
        "display" : "inline-block",
        "color" : "white",
        //"position" : "relative"
    })

    $(".menu").css({
        "opacity" : 1
    }).off().on("click", selectMenu)

    $(".menu span").css({
        "font-size" : getElSize(50) + "px"
    })


    $(".menu_icon").css({
        "width" : getElSize(200) + "px",
    });

    $("#gt").css({
        "background-color" : "rgb(80, 140, 245)"
    })

    $("#operation").css({
        "background-color" : "rgb(230, 111, 45)"
    })

    $("#history").css({
        "background-color" : "rgb(150, 200, 95)"
    })
}