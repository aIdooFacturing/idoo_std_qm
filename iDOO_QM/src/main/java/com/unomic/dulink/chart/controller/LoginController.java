package com.unomic.dulink.chart.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.unomic.dulink.chart.domain.UserVo;
import com.unomic.dulink.chart.service.LoginService;

@RequestMapping(value = "/chart")
@Controller
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Inject
	LoginService loginService;
	
	@RequestMapping(value = "loginCheck", method=RequestMethod.POST)
	public ModelAndView loginCheck(UserVo userVo, HttpSession session) throws Exception {
		ModelAndView mav = new ModelAndView();
		String name;
		try {
			name = loginService.loginCheck(userVo, session);
			if(name!=null) {
				userVo.setName(name);
				mav.addObject("userInfo", userVo);
				mav.setViewName("chart/main");
			}else {
				userVo.setName("000");
				mav.addObject("userInfo", userVo);
				mav.setViewName("chart/main");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping(value = "logOut", method=RequestMethod.POST)
	public ModelAndView logOut(UserVo userVo, HttpSession session) throws Exception {
		ModelAndView mav = new ModelAndView();
		session.invalidate();
		mav.setViewName("chart/main");
		return mav;
	}
	
	
}
