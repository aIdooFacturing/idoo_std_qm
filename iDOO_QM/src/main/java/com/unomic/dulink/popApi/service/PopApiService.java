package com.unomic.dulink.popApi.service;

import java.util.List;

import com.unomic.dulink.popApi.domain.BigoVo;
import com.unomic.dulink.popApi.domain.CommuteVo;
import com.unomic.dulink.popApi.domain.JobVo;
import com.unomic.dulink.popApi.domain.PopApiVo;
import com.unomic.dulink.popApi.domain.PrdVo;

public interface PopApiService {
	
	public String postCommute(CommuteVo cmtVo) throws Exception;
	public String getBigo(BigoVo bigoVo) throws Exception;
		
	public String jsonTest(PopApiVo popVo) throws Exception;
	public String postBigo(BigoVo bigoVo) throws Exception;
	public String putBigo(BigoVo bigoVo) throws Exception;
	public String chkIsOut(CommuteVo cmtVo) throws Exception;
	public String addJobHist(String val);
	public List<PrdVo> getListPrdTg(PrdVo prdVo) throws Exception;
	
	
}
