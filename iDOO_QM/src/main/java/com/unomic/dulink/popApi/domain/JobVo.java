package com.unomic.dulink.popApi.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JobVo {
	String worker;
	String dvc_id;
	String date;
	String start_time;
	String ty_se;
	String pop_id;
	String ty_nd;
	String dvc_cnt;
	String rst_cnt;

	String prd_no;
	String ty;
	Integer ttl_cnt;
}
