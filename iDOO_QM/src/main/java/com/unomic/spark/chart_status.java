//package com.unomic.spark;
//
//
//import java.io.Serializable;
//
//import lombok.Getter;
//import lombok.Setter;
//import lombok.ToString;
//
//import org.apache.spark.SparkConf;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.Function;
//import org.apache.spark.api.java.function.Function2;
//import org.apache.spark.sql.DataFrame;
//import org.apache.spark.sql.SQLContext;
//
//@Getter
//@Setter
//@ToString
//
//public class chart_status implements Serializable{
//	String dvcId;
//	String h;
//	String id;
//	String ieX;
//	String ieY;
//	String name;
//	String pic;
//	String w;
//	String x;
//	String y;
//	String not_use;
//	String x2;
//	String y2;
//	String fontSize;
//	String width;
//	String height;
//	String view_box;
//	String d;
//	String transform;
//	
//	public static void main(String[] args) {
//		System.setProperty("spark.executor.memory", "2g");
//		SparkConf conf = new SparkConf().setAppName("Simple Application").setMaster("spark://192.168.0.92:7077");
//		conf.setJars(new String[]{"target/spark-1.0.0-BUILD-SNAPSHOT.jar"});
//		JavaSparkContext sc = new JavaSparkContext(conf);
//		
//		String data = "/Users/Jeongwan/dev/chart_csv_file/machine_pos.csv";
//		SQLContext sqlContext = new SQLContext(sc);
//
//		
//		JavaRDD<chart_status> status = sc.textFile(data).map(
//				  new Function<String, chart_status>() {
//				    public chart_status call(String line) throws Exception {
//				      String[] parts = line.split(",");
//
//				      chart_status machine = new chart_status();
//				      
//				      machine.setDvcId(parts[0]);
//						machine.setId(parts[1]);
//						machine.setName(parts[2]);
//						machine.setX(parts[3]);
//						machine.setY(parts[4]);
//						machine.setW(parts[5]);
//						machine.setH(parts[6]);
//						machine.setPic(parts[7]);
//						machine.setNot_use(parts[8]);
//						machine.setX2(parts[9]);
//						machine.setY2(parts[10]);
//						machine.setFontSize(parts[10]);
//						machine.setWidth(parts[12]);
//						machine.setHeight(parts[13]);
//						machine.setView_box(parts[14]);
//						machine.setD(parts[15]);
//						machine.setTransform(parts[16]);
//						machine.setIeX(parts[17]);
//						machine.setIeY(parts[18]);
//				      	
//				      return machine;
//				    }
//				  });
//		
//		DataFrame schemaStatus= sqlContext.createDataFrame(status, chart_status.class);
//		schemaStatus.registerTempTable("status");
//		
//		DataFrame result = sqlContext.sql("SELECT * FROM status");
//		
//		result.show();
//	}
//}
//
//class test implements Function2<String , Double, Integer>{
//
//	@Override
//	public Integer call(String s, Double a) throws Exception {
//		
//		
//		return null;
//	}
//	
//}
////class getLength implements Function<String, Integer>{
////	public Integer call(String s) throws Exception {
////		return s.length();
////	}
////};
